# zvoove Job Recruiting

## TYPO3 extension for showing jobs from zvoove and sending applications

This extension will display a list of jobs / vacancies from the zvoove recruiting platform using the Rest API provided
by zvoove. It will also add a form-element using the `tx_form` extension of the TYPO3 core to send applications to 
the API.

The applications will be visible in the INBOX of the zvoove backend at:
`https://yourdomain.europersonal.com/recruiting/inbox/bw`

## Links and further information

| Info          | Link                                            |   
| ------------- | ----------------------------------------------- |
| Repository    | https://bitbucket.org/99grad-team/zvoove/       |
| Documentation | https://docs.typo3.org/p/nng/zvoove/1.0/en-us/  |
| TER           | https://extensions.typo3.org/extension/zvoove   |

## WordPress plugin for zvoove

We are currently working on an adaption of this extension for WordPress. It will have the same functions, but work as
a simple plugin for WordPress, using a custom post-type for storing and syncing the jobs from zvoove and displaying
them in a list- and singleview inside of your WordPress website – without needing iframes.

You will be able to style and customize the templates however you like.
If you are interested and would like to be informed about the current devolopment state and get an email when the 
plugin is ready to be downloaded and installed, please leave us a short message:

Please can contact us at:
zvoove@99grad.de

## Compiling JS and SCSS files to `Resources/Public/Dist`

We have included a small PHP library for compiling and minifying the scss and js files.
(https://packagist.org/packages/itsahappymedium/fec).

If you make changes to the JS and/or SCSS files in the `Public` folder, you can compile them by using
the command line. No need to move to a different build tool or environment.

- on the command line: Switch to the directory `Resources/`
- run `./Libraries/vendor/bin/fec`
- if needed, edit `fec.json` to change settings


## Adding icons

We have added a small set of icons (from Googles "material design") and created a font using
https://icomoon.io

If you need additional icons in the set, follow these steps:

- download the json located at `EXT:zvoove/Resources/Public/Fonts/zvoove/selection.json`
- go to https://icomoon.io/app
- click on "import project" and select the JSON
- add icons to the set, export font
- upload files back to the folder in the extension (better: your own extension)
- compile the JS and SCSS-files like described above
