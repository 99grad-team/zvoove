.. include:: ../Includes.txt

.. _about:

============
Features
============

What does the extension do?
----------------

This extension allows you to display the vacancies you have created on the `zvoove recruiting platform <https://zvoove.com/>`__ 
on your company's website. The integration will be without using iframes which has many advantages:

-  The jobs and content will be indexed by **search engines**
-  There are **no limitations with the design** - style everything in your personal corporate design and anyway you like
-  You can create **custom views** and filtering options

Frontend features
~~~~~~~~~~~~~~~~~

-  Display your vacancies from zoove in a **list- and single-view**
-  Filter and **search for jobs** by title, city, distance, type of contract and department
-  Send applications for a job from an **application form** without leaving your website
-  Bookmark / wishlist function

Development features
~~~~~~~~~~~~~~~~~

-  High performance by complete caching of the jobs in TYPO3
-  Full Fluid integration, NO IFRAMES (!!) - including the media / FAL
-  Scheduler job for automatic synchronising the job-list with zvoove
-  Optional saving of applicant data in the TYPO3 database

Backend features
~~~~~~~~~~~~~~~~~

-  Backend-Module to display all jobs
-  Sync jobs manually or with a scheduler-task


Read on
----------

.. toctree::
   :glob:
   :maxdepth: 1

   About/*