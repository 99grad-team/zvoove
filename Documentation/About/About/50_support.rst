.. include:: ../Includes.txt

.. _support:

============
Free, but...
============

Please note: This is not an official extension from zvoove. The extension was created by `www.99grad.de <www.99grad.de>`__ who were asked to integrate 
zvoove on one of their client's TYPO3 webpages.

During the development we had many problems. They started with a pretty lousy API-documentation from zvoove. It was a draft full of mistakes - many parameters were
not documented – or incorrect. Most of the time we were doing reverse engineering to figure out how to construct the correct request to the zvoove API.

zvoove was not very cooperative during the process of this project. They seemed more interested in making clear to us, that we may not use their logo 
than supporting the development of an extension that would be a great selling argument for their products.

This is the reason we would like to make a difference between "normal" users and the company zvoove:

For everybody except zoove:
--------------

Yes, using the extension is free, but we do ask you to `donate <https://www.paypal.com/paypalme/99grad/1>`__ whatever seems adequate to you. 

| `It's just one click <https://www.paypal.com/paypalme/99grad/1>`__. And it's up to you. But it will keep our developers motivated.


For zvoove:
--------------

If you are planning to promote this extension on your webpage or while talking to potential clients, you will have to contact us and financially 
support the further development! No excuses, sorry.

