.. _start:

=============================================================
zvoove for Typo3 (by 99°)
=============================================================

If you are using `zvoove <https://zvoove.com/>`__ as a recruiting platform and are looking for an
elegant way to display the jobs on your company's website, then: Welcome and keep on reading! 
This extension allows you to: 

- seamlessly integrate zvoove in your TYPO3 webseite without iframes
- display the list of available vacancies from zoove
- send applications from your website to the zvoove inbox
- customize the templates and styling with fluid and css

Looking for the zvoove plugin for WordPress?
------------------------

We are currently working on an adaption of this extension for WordPress. It will have the same functions, but work as
a simple plugin for WordPress, using a custom post-type as storage and syncing the jobs from zvoove. You can display
the jobs from zvoove in a list- and singleview directly inside of your WordPress website – without using iframes.

If you are interested and would like to be informed about the current devolopment state and get an email when the 
plugin is ready to be downloaded and installed, please leave us a short message:

Please can contact us at:
zvoove@99grad.de


.. container:: row m-0 p-0

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h4

            .. rubric:: :ref:`➔ Screenshots <screenshots>`

         .. container:: card-body

            See what you get. Screenshots of the front- and backend.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h4

            .. rubric:: :ref:`★ Features <about>`

         .. container:: card-body

           Read what this extension can do - and can't do.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h4

            .. rubric:: :ref:`⚑ Installation <quickstart>`

         .. container:: card-body

            How to :ref:`install <installation>` the extension and get your website connected to zvoove.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h4

            .. rubric:: :ref:`⨳ Configuration <configuration>`

         .. container:: card-body

            Overview of the :ref:`TypoScript setup <configuration_typoscript>` and 
            configuration you can set in :ref:`yaml <configuration_yaml>`
   
   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h4

            .. rubric:: :ref:`⨳ zvoove for WordPress <wpabout>`

         .. container:: card-body

            Find out, where you can download the adaption of this plugin for your
            WordPress website!

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h4

            .. rubric:: :ref:`⨳ Support us! <support>`

         .. container:: card-body

            Using this extension is free... for everybody but zvoove! Find out,
            how to support the further development. 
            


Is the extension free?
-----------------------

**Yes**, :ref:`but ...! <support>`

License
-----------------------

This extension documentation is published under the
`CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`__ (Creative Commons)
license


Authors
-----------------------

:Version:
   |release|

:Language:
   en

:Authors:
   `www.99grad.de <https://www.99grad.de>`__, David Bascom

:Email:
   info@99grad.de

.. toctree::
   :hidden:
   :maxdepth: 3

   About/Index
   Screenshots/Index
   Installation/Index
   Configuration/Index
   WordPress/Index
   KnownProblems/Index
   Changelog/Index
   Sitemap