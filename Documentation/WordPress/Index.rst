.. include:: ../Includes.txt

.. _wpabout:

============
zvoove plugin for WordPress
============

Display the jobs from zvoove directly on your company's website, 100% stylable and customizable - without the iframes "hack"
------------------------

Your company's website was developed in WordPress and you have been searching for a better solution to integration zvoove
without iframes? Probably you are as annoyed as we were about the cookie-banner being displayed in the iframe and have noticed that
scrolling is a real pain for your visitors. And many clients of zvoove have been wondering, why the vacancies on their
website don't appear in the Google search results (SERPs - Search Engine Result Pages). 

Wouldn't it be great, if you could style the job-list however you like? To be free and flexible in the design and meet the colors, 
fonts and look & feel of your Corporate Design?

**Here is the solution!**

We are currently working on an adaption of the **zvoove plugin** for the world's most used Content Management System. 
It will have the same functionalities and can be installed like any other wp-plugin with a simple click. 
All your jobs and vacancies are stored in the WordPress database as a custom post-type.

The jobs are updated in adjustable intervals using the WordPress cronjob-hook. This keeps your website in sync with 
the data on zvoove. You can display the jobs in a list- and singleview, directly inside of your company's website – 
without using iframes.

If you are interested and would like to be informed about the current devolopment state and get an email when the 
plugin is ready to be downloaded and installed, please leave us a short message:

zvoove@99grad.de


What does the WP-plugin do?
----------------

The zvoove plugin for WordPress allows you to display the vacancies you have created on the `zvoove recruiting platform <https://zvoove.com/>`__ 
on your company's website. The integration will be without using iframes which has many advantages:

-  The SEO (search engine optimization) problem is solved!
-  The jobs and content will be indexed by **search engines**, links on Google will lead to your website instead of the zvoove website
-  There are **no limitations with the design** - style everything in your personal corporate design and anyway you like

Frontend features
~~~~~~~~~~~~~~~~~

-  Display your vacancies from zoove in a **list- and single-view**
-  Filter and **search for jobs** by title, city, distance, type of contract and department
-  Send applications for a job from an **application form** without leaving your website
-  Bookmark / wishlist function

Features for developers
~~~~~~~~~~~~~~~~~

-  Caching of the jobs in WordPress for optimal performance and no latency
-  Full template integration without IFRAMES. External images and media are loaded to your wp-content directory
-  Scheduler job for automatic synchronising the job-list with zvoove
-  Optional saving of applicant data in the WordPress database
