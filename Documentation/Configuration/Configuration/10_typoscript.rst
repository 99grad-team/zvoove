.. include:: ../Includes.txt

.. _configuration_typoscript:

============
TypoScript Setup
============

Configuring with TypoScript Setup
-------------------------------

All of the following settings are configured at 

.. code-block:: typoscript

    plugin.tx_zvoove {
        settings {
            // ... HERE!
        }
    }

detailPid
""""""""""""""
.. container:: table-row

   Property
        detailPid
   Data type
        int
   Description
        Define the page-ID that is used to display the detail view of the jobs.
        This PID can be overriden in the individual flexform.
   Default
        none

listPid
""""""""""""""
.. container:: table-row

   Property
        listPid
   Data type
        int
   Description
        Define the page-ID that is used to display the list view of the jobs.
        This PID can be overriden in the individual flexform.
   Default
        none

bookmarksPid
""""""""""""""
.. container:: table-row

   Property
        bookmarksPid
   Data type
        int
   Description
        Define the page-ID that is used to display the bookmarks / wishlist.
        This PID can be overriden in the individual flexform.
   Default
        none

applicationFormPid
""""""""""""""
.. container:: table-row

   Property
        applicationFormPid
   Data type
        int
   Description
        Define the page-ID that is used to display the application form.
        This PID can be overriden in the individual flexform.
   Default
        none

list.itemsPerPage
""""""""""""""
.. container:: table-row

   Property
        list.itemsPerPage
   Data type
        int
   Description
        Maximum number of jobs to display per page in the list view.
   Default
        none

fallbackImages.stelle
""""""""""""""
.. container:: table-row

   Property
        fallbackImages.stelle
   Data type
        string
   Description
       Fallback image to show, in case no image exists for the job.

       .. code-block:: bash

          fallbackImages {
               stelle = EXT:zvoove/Resources/Public/Images/fallback-vacancy.jpg
          }

   Default
        EXT:zvoove/Resources/Public/Images/fallback-vacancy.jpg

application.*
""""""""""""""
.. container:: table-row

   Property
        application.*
   Data type
        array
   Description
       Various settings to decide, if storing of applications in local database is enabled or not

       .. code-block:: bash

          # settings for the application form (based on tx_form)
          application {
               
               # send application-data to zvoove?
               sendToZvoove {
                    enabled = 1
               }

               # save application-data in the local database?
               saveInDatabase {
                    enabled = 1
                    pidApplicant = 
                    pidApplication = 
               }
          }


search.*
""""""""""""""
.. container:: table-row

   Property
        search.*
   Data type
        array
   Description
       Various settings for defining search-options and filter-criteria to sync with zvoove

       .. code-block:: bash

          # setting for searchform and filter-criteria
          search {
               # These options will be retrieved from the API in \nn\t3::Stelle()->getAllFilters()
               filters {
                    contract {
                         label = Type of contract
                         entity = StelleVertragsart.ChildEntity
                         queryParam = vaUuids
                         fieldInStelle = Vertragsarten 
                         fieldLocal = catVa
                    }
                    department {
                         label = Department
                         entity = StelleAbteilung.Abteilung
                         queryParam = abtUuids
                         fieldInStelle = Abteilung
                         fieldLocal = catAbt
                    }
               }

               # Service to find cities
               location {
                    # class / method to convert location input in searchform to lng / lat coordinates
                    geoCoding = Nng\Zvoove\Services\GoogleApiService::convertAddressToCoordinates

                    # class / method to convert lng / lat coordinates to address
                    reverseGeoCoding = Nng\Zvoove\Services\GoogleApiService::convertCoordinatesToAddress

                    # class / method to return a list of suggestions for location searchform during typing
                    suggest = Nng\Zvoove\Services\SuggestService::getLocations

                    # Maximum number of results from suggest
                    suggestMax = 20
               }

               jobs {
                    # class / method to return a list of suggestions for jobs during typing 
                    suggest = Nng\Zvoove\Services\SuggestService::getJobs
               }
          }

fileUpload.*
""""""""""""""
.. container:: table-row

   Property
        fileUpload.*
   Data type
        array
   Description
       If you are saving the application data and uploads locally, you can define the path to the upload-folder here.

       **Make sure, the path is not accessibly by public!!!**

       .. code-block:: bash

          # where to upload new files.
          fileUpload {
               path = 1:/zoove/
               protectDirectory = 1
               appendHash = 1
          }

externalMedia.*
""""""""""""""
.. container:: table-row

   Property
        externalMedia.*
   Data type
        array
   Description
       Where to copy the files (job-images) from zvoove

       .. code-block:: bash

          # where to copy the files from zvoove
          externalMedia {
               path = 1:/zvoove_media/
          }

sync.*
""""""""""""""
.. container:: table-row

   Property
        sync.*
   Data type
        array
   Description
       How to sync the jobs with zvoove.
         
       **Option 1: Sync all entries in one run.**

       The script will loop until all items have been updated

       - setup your cronjob interval to 15+ minutes
       - set ``processInterval`` to the interval you want to update the database.
            It doesn't matter which interval was set in the cronjob or scheduler. This time is the one
            that is relevant for the update frequency
       - set ``processAll = 1``
       - set ``maxItemsPerRequest`` and ``maxItemsPerLocalSync`` to the number of items to request per batch.
            This means, that during the loop, only this number of items will be retrieved from the API
            and updated. The script will run until all items were updated.
       
       **Option 2: Sync entries in batches**

       The script will process the entries step by step

       - setup your cronjob to a small interval (e.g. 5 minutes)
       - set ``processAll = 0``
       - with every call from the scheduler only a small batch of jobs will be processed.
            It might take several runs until everything is updated.

       .. code-block:: bash

          sync {

               # if external media should be copied to fileadmin. Path is defined in `externalMedia.path`
               copyMedia = 1

               # process all entries in a single run? (`0` will execute the update in small portions with every scheduler call)
               processAll = 1

               # if `processAll = 1`: Total max number of loop-runs to prevent DoS.
               # Must be higher than `( [number_of_jobs_in_zvoove] / $maxItemsPerRequest) + ([number_of_jobs_in_zvoove] / $maxItemsPerLocalSync)`
               processAllMaxRuns = 2000

               # how often to update the list from the API (in seconds, 3600 = every hour)
               processInterval = 3600

               # in case there was a problem during last execution, restart the sync after a max duration of (in seconds)
               maxExecutionTime = 7200

               # only update entries with newer modification date
               respectModificationDate = 1

               # Max items during loading the overview of all vacancies to sync
               maxItemsPerRequest = 15

               # Max items during syncing detail-data with local database
               maxItemsPerLocalSync = 15

               # automatically append (m/w/d) if not already in title?
               appendGender = 1

               # automatically clean HTML-code from zvoove?
               cleanHtml = Aufgaben,Stellenziel,description

               # where to save objects (categories / filters)
               objectsPid = 
          }
