.. include:: ../Includes.txt

.. _configuration_yaml:

============
YAML Configuration
============

RouteEnhancer
--------------

If you would like to display nicer URLs for the jobs, please include the default YAML by 
inserting the following code at the end of your site config.yaml.

.. code-block:: yaml

     # Insert this at the end of your site config.yaml 
     imports:
       -
         resource: 'EXT:zvoove/Configuration/Yaml/default.yaml'
