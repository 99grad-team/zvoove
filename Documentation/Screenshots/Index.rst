.. include:: ../Includes.txt

.. _screenshots:

====================
Screenshots
====================

List of all jobs
-----------------------------

Show a list of all jobs in the frontend. As all templates are fluid-templates, you can customize the design to fit your needs.

.. figure:: fe-listview.jpg
   :class: with-shadow
   :alt: zvoove list of jobs
   :width: 100%

Filtering
-----------------------------

The jobs can be filtered by the criteria defined in your zvoove admin-area. Alle filters and labels are automatically synchronised
with TYPO3.

.. figure:: fe-search.gif
   :class: with-shadow
   :alt: zvoove search filter
   :width: 100%

Job Details
-----------------------------

Display the details of a job in the design you like.

.. figure:: fe-single.jpg
   :class: with-shadow
   :alt: job detail view
   :width: 100%

Bookmarking Jobs
-----------------------------

You can bookmark a jobs and display all jobs in your cart as a list. The bookmark function is cookie-based and works without login.

.. figure:: fe-merkliste.jpg
   :class: with-shadow
   :alt: job bookmark view
   :width: 100%

Application Form
-----------------------------

Send applications directly from TYPO3 to your zvoove inbox - without using iframes or leaving your website.

.. figure:: fe-form.jpg
   :class: with-shadow
   :alt: application form
   :width: 100%
