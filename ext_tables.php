<?php

defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function( $extKey )
	{
		// register icon
		\nn\t3::Registry()->icon('zvoove-plugin', 'EXT:zvoove/Resources/Public/Icons/Extension.svg');

		// register backend-module
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
			\nn\t3::Registry()->getVendorExtensionName($extKey),
			'web',
			'mod1',
			'',
			\nn\t3::Registry()->parseControllerActions([
				\Nng\Zvoove\Controller\ModController::class => 'index, resetSync, syncAll',
			]),
			[
				'access'	=> 'user,group',
				'icon'	 	=> 'EXT:zvoove/Resources/Public/Icons/Extension.svg',
				'labels'	=> 'LLL:EXT:zvoove/Resources/Private/Language/locallang_mod1.xml',
				'navigationComponentId' => '',
				'inheritNavigationComponentFromMainModule' => false,
			]
		);
	},
'zvoove');