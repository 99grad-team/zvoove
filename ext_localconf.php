<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
	function( $extKey )
	{
		// register the utilities for `\nn\zv::Example()->method()`
		$extPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extKey);		
		require_once($extPath . 'Classes/Utilities/zv.php');
		require_once($extPath . 'Classes/Utilities/aliases.php');

		// register PageTsConfig
		\nn\t3::Registry()->addPageConfig('@import "EXT:zvoove/Configuration/TypoScript/page.ts"');

		// register the plugins
		\nn\t3::Registry()->configurePlugin( 'Nng\Zvoove', 'list', 
			[\Nng\Zvoove\Controller\MainController::class => 'list'],
			[\Nng\Zvoove\Controller\MainController::class => 'list']
		);
		\nn\t3::Registry()->configurePlugin( 'Nng\Zvoove', 'show', 
			[\Nng\Zvoove\Controller\MainController::class => 'show'],
			[\Nng\Zvoove\Controller\MainController::class => '']
		);
		\nn\t3::Registry()->configurePlugin( 'Nng\Zvoove', 'search', 
			[\Nng\Zvoove\Controller\MainController::class => 'search'],
			[\Nng\Zvoove\Controller\MainController::class => 'search']
		);
		\nn\t3::Registry()->configurePlugin( 'Nng\Zvoove', 'form', 
			[\Nng\Zvoove\Controller\MainController::class => 'form, perform'],
			[\Nng\Zvoove\Controller\MainController::class => 'form, perform']
		);
		\nn\t3::Registry()->configurePlugin( 'Nng\Zvoove', 'cart', 
			[\Nng\Zvoove\Controller\MainController::class => 'cart'],
			[\Nng\Zvoove\Controller\MainController::class => 'cart']
		);
	},
'zvoove');
