<?php
declare(strict_types = 1);

namespace Nng\Zvoove\Controller;

/**
 * This controller handles AJAX request sent with `typeNum`.
 * The Routing is defined in TypoScript.
 * 
 */
class AjaxController
{
	/**
     * @var array
     */
    protected $arguments = [];

	/**
	 * constructor
	 * 
	 * @return void
	 */
	public function __construct()
	{
		$this->arguments = \nn\t3::Request()->GP('zv') ?: [];
	}

	/**
	 * Endpoint for retrieving suggestion-lists for given keywords
	 * 
	 * /?type=20220210&zv[type]=job&zv[query]=Helfer
	 * 
	 * @return void
	 */
	public function suggestAction() 
	{
		$results = [];
		$type = $this->arguments['type'] ?? false;
		$query = $this->arguments['query'] ?? '';

		switch ( $type ) {
			case 'jobs':
				$results = \nn\zv::Suggest()->jobs( $query );
				break;
			case 'locations':
				$results = \nn\zv::Suggest()->locations( $query );
				break;
		}

		return json_encode($results);
	}
}