<?php

namespace Nng\Zvoove\Controller;

use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Backend Module
 * 
 */
class ModController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{
    /**
	 * Backend Template Container
	 * 
	 * @var string
	 */
	protected $defaultViewObjectName = \TYPO3\CMS\Backend\View\BackendTemplateView::class;

	/**
	 * 	Initialize View
	 * 
	 */
	public function initializeView ( ViewInterface $view ) 
	{
		parent::initializeView($view);

		if (!$view->getModuleTemplate()) return;
		
		$pageRenderer = $view->getModuleTemplate()->getPageRenderer();

		if (\nn\t3::t3Version() < 11) {
			$pageRenderer->loadRequireJsModule('TYPO3/CMS/Zvoove/Bootstrap');
		}
        $pageRenderer->loadRequireJsModule('TYPO3/CMS/Zvoove/Zvoove');
		
        $template = $view->getModuleTemplate();
        $template->setFlashMessageQueue($this->controllerContext->getFlashMessageQueue());
        $template->getDocHeaderComponent()->disable();
	}

	/**
	 * Main view: List of jobs in the backend module
	 * 
	 * @return string
	 */
	public function indexAction () 
	{
		// Get complete list of entries as array (performance!)
		$entries = \nn\t3::Db()->findAll( 'tx_zvoove_domain_model_stelle' );
		$lastRun = \nn\t3::Registry()->get('zvoove', 'lastRun');

		$this->view->assignMultiple([
			'entries' 	=> $entries,
			'lastRun' 	=> $lastRun,
		]);

		return $this->view->render();
	}

	/**
	 * Reset sync.
	 * 
	 * @return string
	 */
	public function resetSyncAction() 
	{
		// Clear page cache
		\nn\t3::Cache()->clearPageCache();

		// Reset tstamp to FORCE update of entries
		\nn\t3::Db()->update( \Nng\Zvoove\Utilities\Stelle::TABLENAME, ['api_tstamp'=>0], true );

		// Reset TYPO3 registry to beginning point
		$progress = \nn\zv::Sync()->init( \Nng\Zvoove\Utilities\Sync::IDLE );

		\nn\t3::Registry()->set('zvoove', 'lastRun', time());

		return json_encode( $progress );
	}
	
	/**
	 * Sync ALL entries.
	 * Called via AJAX from the backend-module.
	 * 
	 * @return string
	 */
	public function syncAllAction() 
	{
		if ($progress = \nn\zv::Sync()->exec()) {
			return json_encode( $progress );
		}
		return json_encode(['error'=>1, 'status'=>100]);
	}

}