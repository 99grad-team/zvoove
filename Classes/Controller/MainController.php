<?php
declare(strict_types = 1);

namespace Nng\Zvoove\Controller;

use \Nng\Zvoove\Domain\Repository\VacancyRepository;
use \Nng\Zvoove\Domain\Repository\StelleRepository;
use \Nng\Zvoove\Pagination\QueryBuilderPaginator;

use TYPO3\CMS\Core\Pagination\ArrayPaginator;
use TYPO3\CMS\Extbase\Pagination\QueryResultPaginator;
use TYPO3\CMS\Core\Pagination\SimplePagination;
use TYPO3\CMS\Extbase\Http\ForwardResponse;

class MainController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

	/**
	 * @var VacancyRepository
	 */
	protected $vacancyRepository = null;
	
	/**
	 * Dependency injection
	 * 
	 */
	public function __construct( 
		VacancyRepository $vacancyRepository, 
		StelleRepository $stelleRepository
	) 
	{
		$this->vacancyRepository = $vacancyRepository;
		$this->stelleRepository = $stelleRepository;
	}

	/**
	 * Set storagePid and default view variables for all views
	 * 
	 * @return void
	 */
	private function initDefaultSettings() 
	{
		$storagePid = \nn\t3::Settings()->getStoragePid() ?: false;
		if (!$storagePid) {
			\nn\t3::Db()->ignoreEnableFields( $this->stelleRepository );
		}
		$this->settings = \nn\t3::Settings()->getMergedSettings();
		$this->view->assignMultiple([
			'settings' => $this->settings,
		]);
	}
	
	/**
	 * Get the filters from the current get/post request
	 * 
	 * @return array
	 */
	private function getCurrentFilters() 
	{
		$formData = [];

		// options / multi-select dropdowns
		$allFilters = \nn\zv::Stelle()->getAllFiltersFromLocalDB();
		$queryParams = array_column($allFilters, 'queryParam');

		$gpVars = \nn\t3::Request()->GP('tx_zvoove_search');
		$filtersFromFlexform = $this->settings['flexform']['filters'] ?? [];

		foreach ($queryParams as $key) {
			$filterValue = $gpVars[$key] ?? $filtersFromFlexform[$key] ?? false;
			if ($filterValue) {
				if (!is_array($filterValue)) {
					$filterValue = \nn\t3::Arrays($filterValue)->trimExplode();
				}
				if ($filterValue) {
					$formData[$key] = $filterValue;
				}
			}
		}

		// additional filters
		$additionalFilters = ['radius', 'keywords'];
		foreach ($additionalFilters as $key) {
			if ($gpVars[$key] ?? false) {
				$formData[$key] = $gpVars[$key] ?? [];
			}
		}
		
		// if location was passed, convert to geo-positions
		if ($location = $gpVars['location'] ?? false) {
			if ($geo = \nn\zv::Geo()->getCity($location)) {
				$formData['lat'] = $geo['lat'];
				$formData['lon'] = $geo['lng'];
				$formData['location'] = $geo['title'];
			}
			unset($gpVars['lon']);
			unset($gpVars['lat']);
		}

		// geo-coordinates were passed
		if ($lon = $gpVars['lon'] ?? false) {
			$formData['lon'] = $lon;
			$formData['lat'] = $gpVars['lat'];
			if ($address = \nn\zv::Geo()->getLocation($formData['lon'], $formData['lat'])) {
				$formData['location'] = $address['locality'] ?? '';
			}
		}

		return $formData;
	}

	/**
	 * Set view variables for filters and searchforms
	 * 
	 * @return void
	 */
	private function initFilterSettings() 
	{
		$filters = [
			'options' 	=> \nn\zv::Stelle()->getAllFiltersFromLocalDB(),
			'gp'		=> $this->getCurrentFilters(),
		];

		$this->view->assignMultiple([
			'filters' 	=> $filters,
		]);
	}


	/**
	 * Searchform
	 * 
	 * @return void
	 */
	public function searchAction() 
	{
		$this->initDefaultSettings();
		$this->initFilterSettings();
	}

	/**
	 * List of vacancies
	 * 
	 * @return void
	 */
	public function listAction() 
	{
		$this->initDefaultSettings();
		$this->initFilterSettings();

		$sorting =  $this->settings['list']['sorting'] ?? false;
		$this->stelleRepository->setSorting( $sorting );

		$args = $this->request->getArguments();
		$currentPage = intval($args['currentPage'] ?? 1);
		$itemsPerPage = intval($this->settings['list']['itemsPerPage']);

		$filters = $this->getCurrentFilters();
		$queryBuilder = StelleRepository::createCriteriaFilterQuery( $filters );

		$paginator = new QueryBuilderPaginator($queryBuilder, $currentPage, $itemsPerPage);
		$pagination = new SimplePagination($paginator);

		$this->view->assignMultiple([
			'total'         => $paginator->getTotalItems(),
			'paginator'     => $paginator,
			'pagination'    => $pagination,
		]);
	}

	/**
	 * Single view of vacancy
	 * 
	 * @return void
	 */
	public function showAction() 
	{
		$this->initDefaultSettings();
		$args = $this->request->getArguments();

		$uid = intval($args['stelle']);
		$vacancy = $this->stelleRepository->findByUid( $uid );

		$this->view->assignMultiple([
			'item' => $vacancy,
		]);
	}
	
	/**
	 * Show application form
	 * 
	 * @return void
	 */
	public function formAction() 
	{
		$this->initDefaultSettings();
		$args = $this->request->getArguments();

		$uid = intval($args['stelle'] ?? 0);
		$vacancy = $uid ? $this->stelleRepository->findByUid( $uid ) : false;
		$title = $vacancy ? $vacancy->getTitle() : '';

		// inject `stelle.uid` and `stelle.title` in to the form
		$formDefinition = \nn\zv::Form()->getFormDefinition($this->settings['persistenceIdentifier'] ?? '');
		\nn\zv::Form()->setDefaultValueForType( $formDefinition, 'ZvooveJobtitle', $title );
		\nn\zv::Form()->addHiddenField( $formDefinition, 'stelleUid', $uid);
		\nn\zv::Form()->setControllerAction( $formDefinition, 'perform' );

		$this->view->assignMultiple([
			'item' 				=> $vacancy,
			'formDefinition' 	=> $formDefinition,
		]);
	}

	/**
	 * Called after submitting the form
	 * 
	 * @return void
	 */
	public function performAction() 
	{
		if (\nn\t3::t3Version() >= 11) {
			return new ForwardResponse('form');
		}
		$this->forward('form');
	}

	/**
	 * Wishlist
	 * 
	 * @return void
	 */
	public function cartAction() 
	{
		$this->initDefaultSettings();

		$cookie = $_COOKIE ?? [];
		$wishlist = \nn\t3::Arrays( $cookie['zv-wishlist'] ?? '' )->intExplode();
		$jobs = $this->stelleRepository->findByUids( $wishlist );

		if ($wishlist) {
			$remainingUids = [];
			foreach ($jobs as $job) {
				$remainingUids[] = $job->getUid();
			}
			setcookie('zv-wishlist', join(',', $remainingUids), time() + 86400 * 3, '/');
		}

		$this->view->assignMultiple([
			'jobs' => $jobs
		]);
	}
}