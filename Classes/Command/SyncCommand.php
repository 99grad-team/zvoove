<?php
declare(strict_types = 1);

namespace Nng\Zvoove\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * You can call this from command line (TYPO3 cli):
 * 
 * non-composer installation:
 * ```
 * ./typo3/sysext/core/bin/typo3 zvoove:sync
 * ```
 * 
 * composer installation:
 * ```
 * ./vendor/bin/typo3 zvoove:sync
 * ```
 * 
 */
class SyncCommand extends Command
{
    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        $this->setHelp('Sync jobs between the local database and the zvoove API.');
    }

    /**
     * Executes the command for syncing entries
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int error code
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title($this->getDescription());

        $result = \nn\zv::Sync()->run();
        $status = $result['status'] ?? 0;

        $stats = $result['results'] ?? false;
        if (!$stats) {
            return Command::SUCCESS;
        }

        $anyUpdated = $stats['updated'] || $stats['deleted'] || $stats['inserted'] || $stats['errors'];

        array_walk( $stats, function( &$item, $key ) {
            $item = "{$key}: " . count($item);
        });

        if ($status == \Nng\Zvoove\Utilities\Sync::FINISHED && $anyUpdated) {
            \nn\t3::Log()->info('zvoove scheduler executed: ' . join("\n", $stats));
        }

        $io->writeln( join("\n", $stats) . "\n" );

        return Command::SUCCESS;
    }

}