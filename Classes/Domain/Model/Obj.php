<?php
namespace Nng\Zvoove\Domain\Model;

use \TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * 
 */
class Obj extends AbstractEntity
{   
	/**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $objUuid = '';
   
	/**
     * @var string
     */
    protected $entity = '';
	
	/**
	 * @return  string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param   string  $title  
	 * @return  self
	 */
	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getObjUuid() {
		return $this->objUuid;
	}

	/**
	 * @param   string  $objUuid  
	 * @return  self
	 */
	public function setObjUuid($objUuid) {
		$this->objUuid = $objUuid;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getEntity() {
		return $this->entity;
	}

	/**
	 * @param   string  $entity  
	 * @return  self
	 */
	public function setEntity($entity) {
		$this->entity = $entity;
		return $this;
	}
}
