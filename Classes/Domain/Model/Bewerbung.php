<?php
namespace Nng\Zvoove\Domain\Model;

use \TYPO3\CMS\Extbase\Domain\Model\FileReference;
use \TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * A simplyfied Model to create an application.
 * This only implements the required fields.
 * 
 * ```
 * $user = new \Nng\Zvoove\Domain\Model\Bewerber();
 * $user->setEmail('test@99grad.de')
 *		->setGender('$$theobjectuuidofthismodel$$') 
 *		->setFirstname('David') 
 *		->setLastname('Bascom');
 *
 * $application = new \Nng\Zvoove\Domain\Model\Bewerbung();
 * $application->setBewerber( $user )
 * 			->setFiles( $objectStorage );
 * 
 * print_r( $application->toArray() );
 * ```
 */
class Bewerbung extends AbstractEntity
{
	/**
     * @var string
     */
    protected $jobid = '';
	
	/**
     * @var string
     */
    protected $mandantid = '';
	
	/**
     * @var string
     */
    protected $applicationid = '';

	/**
	 * Relation to a certain applicant (`Bewerber`)
	 * 
     * @var \Nng\Zvoove\Domain\Model\Bewerber
     */
    protected $bewerber;

    /**
	 * Files specifically related to a single application
	 * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    protected $files;

    /**
	 * constructor
     * 
	 */
	public function __construct() {
		$this->initStorageObjects();
	}
	
	/**
	 * Initializes all \TYPO3\CMS\Extbase\Persistence\ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->files = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the BewerbungDTO as required by the zvoove API.
	 * 
	 * Example:
	 * ```
	 * {"Mitarbeiter":{"Vorname":"David","Nachname":"Bascom","Email":"abo1@99grad.de","Adressen":[],"Anrede":{"ObjectUuid":"b8d63db8-094d-4e06-b988-1c8c5d3ce89b"}},"Bewerbung":{"Mandant":null,"Bewerbungsdatum":"2022-01-27T00:00:00.000Z","IstInitiativ":true,"Stelle":null,"Vertragsarten":null,"Quelle":null,"KuendigungsfristZusatz":0,"Fragen":[]},"HasParsedCV":false,"UrlSlug":"purus-ag","Skills":[],"Zertifikate":[]}
	 * ```
	 * @return array
	 */
	public function toArray() {
		$bewerberData = [];
		if ($bewerber = $this->getBewerber()) {
			$bewerberData = $bewerber->toArray();
		}
		return [
			'Mitarbeiter' 	=> $bewerberData,
			'Bewerbung' 	=> $this->getBewerbung(),
			'HasParsedCV'	=> false,
			'UrlSlug'		=> 'purus-ag',
			'Skills' 		=> $this->getSkills(),
			//'Werdegaenge' 	=> $this->getWerdegaenge(),			
			'Zertifikate' 	=> $this->getZertifikate(),			
		];
	}

	/**
	 * @return array
	 */
	public function getBewerbung() {
		$obj = [
			'Mandant' => null,
			'Bewerbungsdatum' => date(DATE_ATOM, time()),
			'IstInitiativ' => true,
			'Stelle' => null,
			'Vertragsarten' => null,
			'Quelle' => null,
			'KuendigungsfristZusatz' => 0,
			'Fragen' => [],
		];

		if ($jobid = $this->getJobid()) {
			$mandantid = $this->getMandantid();
			$obj['IstInitiativ'] = false;
			$obj['Mandant'] = [
				'ObjectUuid' => $mandantid
			];
			$obj['Stelle'] = [
				'MandantUuid' => $mandantid,
				'ObjectUuid' => $jobid,
			];
		}
		return $obj;
	}

	/**
	 * @return array
	 */
	public function getMitarbeiter() {
		return [
			'Vorname'	=> $this->firstname,
			'Nachname'	=> $this->lastname,
			'Email'		=> $this->email,
			'Telefon'	=> $this->phone,
			'Adressen'	=> [],
			'Anrede'	=> [
				'ObjectUuid' => $this->gender,
			],
		];
	}

	/**
	 * @return array
	 */
	public function getSkills() {
		return [];
	}

	/**
	 * @return array
	 */
	public function getWerdegaenge() {
		return [];
	}
	
	/**
	 * @return array
	 */
	public function getZertifikate() {
		return [];
	}

	/**
	 * @return  \TYPO3\CMS\Extbase\Persistence\ObjectStorage
	 */
	public function getFiles() {
		return $this->files;
	}

	/**
	 * @param   \TYPO3\CMS\Extbase\Persistence\ObjectStorage  $files  
	 * @return  self
	 */
	public function setFiles($files) {
		$this->files = $files;
		return $this;
	}

	/**
	 * @return  \Nng\Zvoove\Domain\Model\Bewerber
	 */
	public function getBewerber() {
		return $this->bewerber;
	}

	/**
	 * @param   \Nng\Zvoove\Domain\Model\Bewerber  $bewerber  
	 * @return  self
	 */
	public function setBewerber($bewerber) {
		$this->bewerber = $bewerber;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getApplicationid() {
		return $this->applicationid;
	}

	/**
	 * @param   string  $applicationid  
	 * @return  self
	 */
	public function setApplicationid($applicationid) {
		$this->applicationid = $applicationid;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getJobid() {
		return $this->jobid;
	}

	/**
	 * @param   string  $jobid  
	 * @return  self
	 */
	public function setJobid($jobid) {
		$this->jobid = $jobid;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getMandantid() {
		return $this->mandantid;
	}

	/**
	 * @param   string  $mandantid  
	 * @return  self
	 */
	public function setMandantid($mandantid) {
		$this->mandantid = $mandantid;
		return $this;
	}
}
