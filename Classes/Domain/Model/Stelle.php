<?php
namespace Nng\Zvoove\Domain\Model;

use \TYPO3\CMS\Extbase\Domain\Model\FileReference;
use \TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Model for a vacancy (`Stelle`)
 * 
 */
class Stelle extends AbstractEntity
{

    /**
     * @var int
     */
    protected $apiTstamp;
    
	/**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $jobid = '';

    /**
     * @var string
     */
    protected $location = '';

    /**
     * @var string
     */
    protected $zip = '';

    /**
     * @var string
     */
    protected $contract = '';

    /**
     * @var string
     */
    protected $data = '';

    /**
     * @var array
     */
    protected $_data = '';

	/**
     * @var array
     */
    protected $_jsonLd = '';

	/**
     * @var int
     */
    protected $dateFrom;

	/**
     * @var int
     */
    protected $dateTo;

	/**
     * @var float
     */
    protected $lat;

	/**
     * @var float
     */
    protected $lng;
	
	/**
     * @var float
     */
    protected $distance;

	/**
     * @var string
     */
    protected $country;

	/**
     * @var string
     */
    protected $district;

 	/**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    protected $images;

 	/**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    protected $imagesContact;

 	/**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Nng\Zvoove\Domain\Model\Obj>
     */
    protected $catVa;
 	
	/**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Nng\Zvoove\Domain\Model\Obj>
     */
    protected $catAbt;

	/**
	 * constructor
	 */
	public function __construct() {
		$this->initStorageObjects();
	}
	
	/**
	 * Initializes all \TYPO3\CMS\Extbase\Persistence\ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->images = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->imagesContact = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->catVa = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->catAbt = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Return first image with fallback to placeholder image
	 * @return string
	 */
	public function getPreviewImage() {
		if ($image = $this->getFirstImage()) {
			return \nn\t3::File()->getPublicUrl($image);
		}
		return \nn\zv::File()->getFallbackImage( 'stelle' );
	}

	/**
	 * Return first image
	 * @return  \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	public function getFirstImage() {
		foreach ($this->images as $image) {
			return $image;
		}
	}

	/**
	 * @return  \TYPO3\CMS\Extbase\Persistence\ObjectStorage
	 */
	public function getImages() {
		return $this->images;
	}

	/**
	 * @param   \TYPO3\CMS\Extbase\Persistence\ObjectStorage  $images  
	 * @return  self
	 */
	public function setImages($images) {
		$this->images = $images;
		return $this;
	}
	
	/**
	 * @return  \TYPO3\CMS\Extbase\Persistence\ObjectStorage
	 */
	public function getImagesContact() {
		return $this->imagesContact;
	}

	/**
	 * @param   \TYPO3\CMS\Extbase\Persistence\ObjectStorage  $imagesContact  
	 * @return  self
	 */
	public function setImagesContact($imagesContact) {
		$this->imagesContact = $imagesContact;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param   string  $title  
	 * @return  self
	 */
	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getMandantid() {
		$data = $this->getData();
		return $data['Mandant']['ObjectUuid'] ?? '';
	}
	
	/**
	 * @return  string
	 */
	public function getJobid() {
		return $this->jobid;
	}

	/**
	 * @param   string  $jobid  
	 * @return  self
	 */
	public function setJobid($jobid) {
		$this->jobid = $jobid;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getLocation() {
		return $this->location;
	}

	/**
	 * @param   string  $location  
	 * @return  self
	 */
	public function setLocation($location) {
		$this->location = $location;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getZip() {
		return $this->zip;
	}

	/**
	 * @param   string  $zip  
	 * @return  self
	 */
	public function setZip($zip) {
		$this->zip = $zip;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getContract() {
		return $this->contract;
	}

	/**
	 * @param   string  $contract  
	 * @return  self
	 */
	public function setContract($contract) {
		$this->contract = $contract;
		return $this;
	}

	/**
	 * @return  array
	 */
	public function getJsonLd() {
		if ($cache = $this->_jsonLd) return $cache;
		return $this->_jsonLd = json_decode($this->getData()['JsonLd'], true) ?: [];
	}

	/**
	 * @return  array
	 */
	public function getData() {
		if ($cache = $this->_data) return $cache;
		return $this->_data = json_decode($this->data, true);
	}

	/**
	 * @param   string  $data  
	 * @return  self
	 */
	public function setData($data) {
		if (is_array($data)) {
			$data = json_encode( $data );
		}
		$this->data = $data;
		return $this;
	}

	/**
	 * @return  int
	 */
	public function getDateFrom() {
		return $this->dateFrom;
	}

	/**
	 * @param   int  $dateFrom  
	 * @return  self
	 */
	public function setDateFrom($dateFrom) {
		$this->dateFrom = $dateFrom;
		return $this;
	}

	/**
	 * @return  int
	 */
	public function getDateTo() {
		return $this->dateTo;
	}

	/**
	 * @param   int  $dateTo  
	 * @return  self
	 */
	public function setDateTo($dateTo) {
		$this->dateTo = $dateTo;
		return $this;
	}

	/**
	 * @return  int
	 */
	public function getApiTstamp() {
		return $this->apiTstamp;
	}

	/**
	 * @param   int  $apiTstamp  
	 * @return  self
	 */
	public function setApiTstamp($apiTstamp) {
		$this->apiTstamp = $apiTstamp;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSalary() 
	{
		$data = $this->getData();

		if (!$data['Gehalt']) return '';
		if (!$data['GehaltBis']) return '';

		$from = number_format($data['Gehalt'] ?: '', 2, ',', '.');
		$to = number_format($data['GehaltBis'] ?: '', 2, ',', '.');

		$currency = $data['GehaltWaehrung'] ?: '';

		$time = $data['GehaltZeitraum'] ?: '';
		$time = \nn\t3::LL()->get( 'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:time.' . $time );

		return "{$from} – {$to} {$currency} {$time}";
	}

	/**
	 * @return string
	 */
	public function getWorkHours() 
	{
		$data = $this->getData();

		$from = $data['Arbeitsstunden'] ?: '';
		$to = $data['ArbeitsstundenBis'] ?: '';

		$time = $data['ArbeitsstundenZeitraum'] ?: '';
		$time = \nn\t3::LL()->get( 'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:time.' . $time );
		$hours = \nn\t3::LL()->get( 'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:hours' );

		return "{$from} – {$to} {$hours} {$time}";
	}

	/**
	 * Get link to view on the zvoove job-portal
	 * 
	 * @return string
	 */
	public function getUriPortal() {
		$data = $this->getJsonLd();
		return $data['url'] ?? '';
	}

	/**
	 * @return  float
	 */
	public function getLat() {
		return $this->lat;
	}

	/**
	 * @param   float  $lat  
	 * @return  self
	 */
	public function setLat($lat) {
		$this->lat = $lat;
		return $this;
	}

	/**
	 * @return  float
	 */
	public function getLng() {
		return $this->lng;
	}

	/**
	 * @param   float  $lng  
	 * @return  self
	 */
	public function setLng($lng) {
		$this->lng = $lng;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * @param   string  $country  
	 * @return  self
	 */
	public function setCountry($country) {
		$this->country = $country;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getDistrict() {
		return $this->district;
	}

	/**
	 * @param   string  $district  
	 * @return  self
	 */
	public function setDistrict($district) {
		$this->district = $district;
		return $this;
	}

	/**
	 * @return  \TYPO3\CMS\Extbase\Persistence\ObjectStorage
	 */
	public function getCatVa() {
		return $this->catVa;
	}

	/**
	 * @param   \TYPO3\CMS\Extbase\Persistence\ObjectStorage  $catVa  
	 * @return  self
	 */
	public function setCatVa($catVa) {
		$this->catVa = $catVa;
		return $this;
	}

	/**
	 * @return  \TYPO3\CMS\Extbase\Persistence\ObjectStorage
	 */
	public function getCatAbt() {
		return $this->catAbt;
	}

	/**
	 * @param   \TYPO3\CMS\Extbase\Persistence\ObjectStorage  $catAbt  
	 * @return  self
	 */
	public function setCatAbt($catAbt) {
		$this->catAbt = $catAbt;
		return $this;
	}

	/**
	 * @return  float
	 */
	public function getDistance() {
		return $this->distance;
	}

	/**
	 * @param   float  $distance  
	 * @return  self
	 */
	public function setDistance($distance) {
		$this->distance = $distance;
		return $this;
	}
}
