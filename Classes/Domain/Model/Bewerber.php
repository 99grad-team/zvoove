<?php
namespace Nng\Zvoove\Domain\Model;

use \TYPO3\CMS\Extbase\Domain\Model\FileReference;
use \TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Model for a single applicant (`Bewerber`) that sends an application (`Bewerbung`)
 * 
 * ```
 * $person = new \Nng\Zvoove\Domain\Model\Bewerber();
 * $person
 * 	->setEmail('test@99grad.de')
 *	->setGender('$$theobjectuuidofthismodel$$') 
 *	->setFirstname('David') 
 *	->setLastname('Bascom');
 * 
 * print_r( $application->toArray() );
 * ```
 */
class Bewerber extends AbstractEntity
{
    /**
	 * The values for gender come from the API and can be determined using
	 * 
	 * ```
	 * \nn\zv::Katalog()->getOptions( 'Mitarbeiter.Anrede' );
	 * ```
	 * 
	 * Example:
	 * ```
	 * [
	 * 	'128faa85-1bdb-42db-a7ad-ae81ec56b522' => 'Frau',
	 * 	'b8d63db8-094d-4e06-b988-1c8c5d3ce89b' => 'Herr',
	 * ]
	 * ```
	 * 
     * @var string
     */
    protected $gender = 'b8d63db8-094d-4e06-b988-1c8c5d3ce89b';

    /**
     * @var string
     */
    protected $email = '';

    /**
     * @var string
     */
    protected $phone = '';

	/**
     * @var string
     */
    protected $firstname = '';

	/**
     * @var string
     */
    protected $lastname = '';

    /**
	 * General files related to applicant (independent of specific application)
	 * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    protected $files;

    /**
	 * constructor
     * 
	 */
	public function __construct() {
		$this->initStorageObjects();
	}
	
	/**
	 * Initializes all \TYPO3\CMS\Extbase\Persistence\ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->files = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * @return array
	 */
	public function toArray() {
		return [
			'Vorname'	=> $this->firstname,
			'Nachname'	=> $this->lastname,
			'Email'		=> $this->email,
			'Telefon'	=> $this->phone,
			'Adressen'	=> [],
			'Anrede'	=> [
				'ObjectUuid' => $this->gender,
			],
		];
	}

	/**
	 * @return  string
	 */
	public function getGender() {
		return $this->gender;
	}

	/**
	 * @param   string  $gender  gender
	 * @return  self
	 */
	public function setGender($gender) {
		$this->gender = $gender;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getFirstname() {
		return $this->firstname;
	}

	/**
	 * @param   string  $firstname  
	 * @return  self
	 */
	public function setFirstname($firstname) {
		$this->firstname = $firstname;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getLastname() {
		return $this->lastname;
	}

	/**
	 * @param   string  $lastname  
	 * @return  self
	 */
	public function setLastname($lastname) {
		$this->lastname = $lastname;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param   string  $email  
	 * @return  self
	 */
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}

	/**
	 * @return  string
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * @param   string  $phone  
	 * @return  self
	 */
	public function setPhone($phone) {
		$this->phone = $phone;
		return $this;
	}

}
