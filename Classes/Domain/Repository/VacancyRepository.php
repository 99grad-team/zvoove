<?php
namespace Nng\Zvoove\Domain\Repository;

use Nng\Zvoove\Domain\Model\Stelle;
use Nng\Zvoove\Domain\Repository\StelleRepository;

/**
 * Repository for retrieving list and details to vacancies.
 * 
 */
class VacancyRepository extends AbstractApiRepository {

	/**
	 * Get list of ALL vacancies
	 * 
	 * @return array
	 */
	public function findAll() {

		//return \nn\zv::Katalog()->getOptions( 'Mitarbeiter', 'Anrede' );

		//return \nn\zv::File()->getSysFileForStreamUuid( 'd198aef9-a9c5-4afe-a9d0-85c6936f1e3b' );

//		\nn\zv::MandantOeffentlicheDatei()->getAsStreamContent( 'd198aef9-a9c5-4afe-a9d0-85c6936f1e3b' );

		\nn\zv::Sync()->updateStellen( '25733bea-c1e2-4321-a8fb-9328d3edb893', true );

		$stelleRepository = \nn\t3::injectClass( StelleRepository::class );
		$vacancy = $stelleRepository->findByUid( 3 );
//		$vacancy = \nn\zv::Stelle()->getStellenByIds( ['25733bea-c1e2-4321-a8fb-9328d3edb893'] );
		return $vacancy;

		//return \nn\zv::Sync()->run();

		/*
		$stelleRepository = \nn\t3::injectClass( StelleRepository::class );

		$stelle = new Stelle();
		$stelle->setTitle('Yeah!');
		return \nn\t3::Db()->getRepositoryForModel( Stelle::class );

		return $stelle;

		return \nn\t3::Db()->findIn('tx_zvoove_domain_model_stelle', 'uid', ['5a'] );
		//*/

		/*
		$applicants = \nn\zv::Bewerber()->getAll();
		$applicant = \nn\zv::Bewerber()->getById( $applicants[10]['ObjectUuid'] );
		return $applicant;
		\nn\zv::Bewerber()->downloadDatei( $applicant['Dateien'][1] );
		//*/

		/*
		$result = \nn\zv::Sync()->updateStellen();
		return $result;
		//*/

		/*
		$vacancies = \nn\zv::BewerberPortal()->getAllStellenboersen();
		return $vacancies;
		//*/

		/*
		$uuid = \nn\zv::Stelle()->getStellenFiltered()[2]['StelleUuid'];
		$vacancy = \nn\zv::Stelle()->getStellenByIds( [$uuid] );
		return $vacancy;
		//*/

		/*
		$vacancies = \nn\zv::Stelle()->getStellenFiltered();
		return $vacancies;
		//*/

		/*
		$vacancies = \nn\zv::Stelle()->getStellen();
		return $vacancies;
		//*/

		/*
		$vacancies = \nn\zv::BewerberPortal()->getAllStellenGroupedByStellenboersen();
		return $vacancies;
		//*/

		/*
		$filter = [];
		$vacancies = \nn\zv::BewerberPortal()->getStellenAnonymous( 'purus-ag', $filter );
		return $vacancies;
		//*/

		/*
		$applicants = \nn\zv::Bewerber()->getAll();
		$applicant = \nn\zv::Bewerber()->getById( $applicants[0]['ObjectUuid'] );
		return $applicants;
		//*/

		/*
		$result = \nn\zv::Bewerbung()->getNeuanlageModel();
		return $result;
		//*/

		/*
		$application = new \Nng\Zvoove\Domain\Model\Bewerbung();
		$application
			->setEmail('david@99grad.de')
			->setGender('b8d63db8-094d-4e06-b988-1c8c5d3ce89b')
			->setFirstname('David') 
			->setLastname('Bascom');
		
		$result = \nn\zv::BewerberPortal()->createBewerber( $application );
		return $result;
		//*/

		return [];
	}
}