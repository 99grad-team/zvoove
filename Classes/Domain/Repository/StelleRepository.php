<?php
namespace Nng\Zvoove\Domain\Repository;

use Nng\Zvoove\Domain\Model\Stelle;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class StelleRepository extends \TYPO3\CMS\Extbase\Persistence\Repository 
{	
	/**
	 * Table with the vacancies
	 */
	const TBL_STELLE 	= 'tx_zvoove_domain_model_stelle';
	const TBL_OBJ 		= 'tx_zvoove_domain_model_obj';
	const TBL_OBJ_MM 	= 'tx_zvoove_domain_model_stelle_obj_mm';

	/**
	 * Set sorting for results
	 * 
	 * @param string key	 sorting-options as defined in the pageTsConfig
	 * @return void
	 */
	public function setSorting( $key = '' ) 
	{
		if ($key && $config = \nn\t3::Settings()->getPageConfig('tx_zvoove.sorting.' . $key)) {
			if ($val = $config['value'] ?? false) {
				$parts = \nn\t3::Arrays($val)->trimExplode(',');
				$field = array_shift( $parts );
				$ascDesc = array_shift( $parts ) ?: 'ASC';
				$ordering = [$field => $ascDesc];
				\nn\t3::Db()->orderBy( $this, $ordering );
			}
		}
	}

	/**
	 * Find vacancies by a list of uids
	 * 
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByUids( $uidList = [] ) 
	{
		$query = $this->createQuery();
		if (!$uidList) {
			$query->matching($query->equals('uid', 0));
			return $query->execute();
		}
		$query->matching($query->in('uid', $uidList));
		return $query->execute();
	}

	/**
	 * Find vacancies by a list of jobids
	 * 
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByJobids( $uidList = [] ) 
	{
		$query = $this->createQuery();

		// no list passed? Return empty QueryResult
		if (!$uidList) {
			$query->matching($query->equals('uid', 0));
			return $query->execute();
		}
		$query->matching($query->in('jobid', $uidList));
		return $query->execute();
	}

	/**
	 * Find vacancies by search criteria.
	 * Uses the local database as a source.
	 * 
	 * ```
	 * [
	 *  'keywords'  => 'xxx',
	 *  'pageSize'  => 5,			// number of results to retrieve
	 *  'pageNum'	=> 1,			// page to retrieve (first page is 1!)
	 *  'abtUuids'  => [],			// ObjectUuids of `Abteilungen`
	 *  'vaUuids'	=> [],			// ObjectUuids of `Vertragsarten`
	 *  'lat'		=> 50.041381,	// Latitude
	 *  'lon'		=> 8.5800547,	// Longitude
	 *  'radius'	=> 50,			// Radius (km)
	 *  ...
	 * ]
	 * ```
	 * @param array $filters
	 * @return array
	 */
	public function findByCriteria( $filters = [] ) 
	{
		$queryBuilder = self::createCriteriaFilterQuery( $filters );
		return self::getResults( $queryBuilder );
	}

	/**
	 * Find vacancies by search criteria.
	 * Uses the local database as a source.
	 * 
	 * @param array $filters
	 * @return \TYPO3\CMS\Core\Database\Query\QueryBuilder
	 */
	public static function createCriteriaFilterQuery( $filters = [] ) 
	{
		$table = self::TBL_STELLE;
		$queryBuilder = \nn\t3::Db()->getQueryBuilder( $table );
		$queryBuilder->select('tbl_stelle.*')->from( $table, 'tbl_stelle' );

		$keyword = \nn\zv::General()->stripGender($filters['keywords'] ?? '');
		$swords = \nn\t3::Arrays($keyword)->trimExplode();
		$settings = \nn\t3::Settings()->get('tx_zvoove');

		$constraints = [];
		
		// constraint to certain searchwords?
		if ($swords) {
			$or = [];
			foreach ($swords as $sword) {
				$sword = $queryBuilder->createNamedParameter('%' . $queryBuilder->escapeLikeWildcards($sword) . '%');
				$or[] = $queryBuilder->expr()->like('tbl_stelle.title', $sword );
				$or[] = $queryBuilder->expr()->like('tbl_stelle.data', $sword );
			}
			$constraints[] = $queryBuilder->expr()->orX( ...$or );
		}

		// limit number of results? Will be overriden, if QueryBuilderPaginator is in use.
		if ($pageSize = $filters['pageSize'] ?? false) {
			$pageNum = ($filters['pageNum'] ?? 1)-1;
			$queryBuilder->setMaxResults( $pageSize );
			$queryBuilder->setFirstResult( $pageSize * $pageNum );
		}

		// contstraint to filter criteria?
		$searchFilters = $settings['search']['filters'] ?? [];
		foreach ($searchFilters as $entity=>$config) {
			
			$key = $config['queryParam'] ?? false;
			$fieldLocal = $config['fieldLocal'] ?? false;

			if (!$fieldLocal || !$key) continue;

			if ($objUuids = $filters[$key] ?? false) {
				if ($localUids = \nn\zv::Sync()->convertObjectUuidsToLocalUids($objUuids)) {
					$localUids = \nn\t3::Arrays($localUids)->intExplode();
					// `WHERE tbl_stelle.uid IN (SELECT uid_local FROM tx_zvoove_domain_model_stelle_obj_mm WHERE `uid_foreign` IN (26) ) ...
					$subQuery = \nn\t3::Db()->getQueryBuilder( self::TBL_OBJ_MM );
					$subQuery->select('uid_local')->from( self::TBL_OBJ_MM )
						->where( $subQuery->expr()->in('uid_foreign', $localUids));
					$constraints[] = $queryBuilder->expr()->in('tbl_stelle.uid', $subQuery->getSQL());					
				}
			}
		}

		// constraint to geo-position?
		if ($lat = $filters['lat'] ?? false) {

			$lat = floatval($lat);
			$lon = floatval($filters['lon']);
			$radius = intval($filters['radius']) ?: 500;

			$queryBuilder->addSelectLiteral('
				(6371 * acos(cos(radians( '.$lat.' )) * cos(radians( tbl_stelle.lat )) * cos(radians( tbl_stelle.lng ) - radians( '.$lon.' )) + sin(radians( '.$lat.' )) * sin(radians( tbl_stelle.lat )))) AS distance'
			);

			$queryBuilder->orderBy('distance');
			$queryBuilder->having('distance < ' . $radius);
		}

		// AND all constraints
		if ($constraints) {
			$queryBuilder->andWhere(
				$queryBuilder->expr()->andX( ...$constraints )
			);
		}

		return $queryBuilder;
	}

	/**
	 * Retrieve the results for a given QueryBuilder.
	 * Convert the raw database result-array to TYPO3 `Stelle` Models.
	 * ```
	 * \Nng\Zvoove\Domain\Repository\StelleRepository::getResults( $queryBuilder );
	 * self::getResults( $queryBuilder );
	 * ```
	 * @param \TYPO3\CMS\Core\Database\Query\QueryBuilder $queryBuilder
	 * @return array
	 */
	public static function getResults( $queryBuilder = null ) 
	{
		$dataMapper = \nn\t3::injectClass( DataMapper::class );
		$rows = $queryBuilder->execute()->fetchAll();
		return $dataMapper->map(Stelle::class, $rows);
	}

}