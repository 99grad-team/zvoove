<?php
declare(strict_types = 1);

namespace Nng\Zvoove\Domain\Repository;

/**
 * This in NOT a typical TYPO3 repository, but the connection to the zoove Api.
 * We are understanding the API as a kind of "external repository"
 * 
 */
abstract class AbstractApiRepository {


}