<?php
namespace Nng\Zvoove\ViewHelpers\Format;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use Nng\Nnhelpers\ViewHelpers\AbstractViewHelper;

/**
 * Clean html-code and inline-styles from zvoove
 * 
 * ```
 * {namespace zv=Nng\Zvoove\ViewHelpers}
 * 
 * // remove inline-styles
 * {item.data.Aufgaben->zv:format.clean()}
 * ```
 */
class CleanViewHelper extends AbstractViewHelper
{
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('str', 'string', '');
	}

	/**
	 * @return string
	 */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext )
	{
		$str = $arguments['str'] ?: $renderChildrenClosure();
		return \nn\zv::General()->cleanHtml( $str );
	}

}
