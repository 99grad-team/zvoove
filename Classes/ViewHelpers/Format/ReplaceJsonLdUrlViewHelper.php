<?php
namespace Nng\Zvoove\ViewHelpers\Format;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use Nng\Nnhelpers\ViewHelpers\AbstractViewHelper;

/**
 * Replace URL in JsonLd from europersonal.com with URL of current page
 * 
 * ```
 * {namespace zv=Nng\Zvoove\ViewHelpers}
 * 
 * // replace URL in JsonLd from zvoove with URL of current page
 * {item.data.JsonLd->zv:format.replaceJsonLdUrl()}
 * ```
 */
class ReplaceJsonLdUrlViewHelper extends AbstractViewHelper
{
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('json', 'string', '');
	}

	/**
	 * @return string
	 */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext )
	{
		$json = $arguments['json'] ?: $renderChildrenClosure();
		if (is_string($json)) {
			$json = json_decode($json, true);
		}
		$json['url'] = \nn\t3::Environment()->getBaseUrl() . ltrim( \nn\t3::Request()->getUri(), '/' );
		return json_encode($json);
	}

}
