<?php
namespace Nng\Zvoove\ViewHelpers\Format;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use Nng\Nnhelpers\ViewHelpers\AbstractViewHelper;

/**
 * Append the (m/w/d) gender to a string, if not exists.
 * 
 * ```
 * {namespace zv=Nng\Zvoove\ViewHelpers}
 * 
 * // append with default settings
 * {job.title->zv:format.appendGender()}
 * 
 * // append `(m/f/d)` to str
 * {job.title->zv:format.appendGender(append:'(m/f/d)')}
 * 
 * // append only if str doesn't contain `(m/`
 * {job.title->zv:format.appendGender(append:'(m/f/d)', check:'(m/')}
 * ```
 */
class AppendGenderViewHelper extends AbstractViewHelper
{
    public function initializeArguments() 
    {
		parent::initializeArguments();
		$this->registerArgument('str', 'string', '');
		$this->registerArgument('check', 'string', '', false );
		$this->registerArgument('append', 'string', '', false );
   	}

    /**
     * @return string
     */
    public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext )
    {
       $str = $arguments['str'] ?: $renderChildrenClosure();
       return \nn\zv::General()->appendGender( $str, $arguments['append'], $arguments['check'] );
    }
}
