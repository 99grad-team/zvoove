<?php

namespace Nng\Zvoove\ViewHelpers\Api;

use Nng\Nnhelpers\ViewHelpers\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Returns a list of options from the zvoove API, e.g. for showing
 * a select-DropDown.
 *
 * {namespace zv=Nng\Zvoove\ViewHelpers}
 *
 * <zv:api.katalogOptions path="Bewerber.Anrede" />
 * {zv:api.katalogOptions(path:'Bewerber.Anrede')}
 *
 */
class KatalogOptionsViewHelper extends AbstractViewHelper {

    /**
	 * @return void
	 */
	public function initializeArguments() {
		parent::initializeArguments();
    	$this->registerArgument('path', 'string', 'Path in Catalogue/Katalog', false);
	}
	
    /**
     * @return array
     */
    public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext ) {
		foreach ($arguments as $k=>$v) {
			${$k} = $v;
		}

		if (!$path) return [];
        
        $options = \nn\zv::Katalog()->getOptions( $path );
        return $options;
    }
    
}