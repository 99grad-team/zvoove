<?php
namespace Nng\Zvoove\ViewHelpers\Uri;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use Nng\Nnhelpers\ViewHelpers\AbstractViewHelper;

/**
 * Create a link to the suggest-proxy
 * 
 * ```
 * {namespace zv=Nng\Zvoove\ViewHelpers}
 * 
 * <input data-zv-suggest="{zv:uri.suggest(type:'jobs')}" />
 * ```
 */
class SuggestViewHelper extends AbstractViewHelper
{
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('type', 'string', '');
		$this->registerArgument('query', 'string', '');
	}

	/**
	 * @return string
	 */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext )
	{
		$type = $arguments['type'];
		$query = $arguments['query'];

        return \nn\t3::Page()->getAbsLink([
            'type' => 20220210,
            'zv' => [
                'type' => $type,
                'query' => $query,
            ],
        ]);
	}

}
