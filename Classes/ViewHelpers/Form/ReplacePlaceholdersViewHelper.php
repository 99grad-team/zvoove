<?php
declare(strict_types=1);

namespace Nng\Zvoove\ViewHelpers\Form;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use Nng\Nnhelpers\ViewHelpers\AbstractViewHelper;

/**
 * Replace [a]...[/a] in text with real links
 * 
 * ```
 * {namespace zv=Nng\Zvoove\ViewHelpers}
 * 
 * {element.properties.text->zv:form.replacePlaceholders(pid:element.properties.pageUid)}
 * ```
 */
class ReplacePlaceholdersViewHelper extends AbstractViewHelper
{
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('str', 'string', '');
		$this->registerArgument('pid', 'string', '');
		$this->registerArgument('target', 'string', '');
	}

	/**
	 * @return string
	 */
	public static function renderStatic( array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext )
	{
		$str = $arguments['str'] ?: $renderChildrenClosure();
		$pid = $arguments['pid'];
		$target = $arguments['target'] ? ' target="blank"' : '';

		$link = \nn\t3::Page()->getLink( $pid );
		if (!$str) return '';
		
		$str = strtr($str, [
			'[a]'   => "<a href=\"{$link}\"{$target}>", 
			'[/a]'  => '</a>'
		]);
		return $str;
	}

}

