<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Services;

class GoogleApiService
{
	/**
	 * @var array
	 */
	protected $cache;

	/**
	 * Convert address-string to coordinates using the Google Api.
	 * You must define an apiKey in the Extension Manager to use this method.
	 * 
	 * @param string $address
	 * @return array
	 */
	public static function convertAddressToCoordinates( $address = '' )
	{
		$apiKey = \nn\t3::Environment()->getExtConf('zvoove')['googleGeoApiKey'] ?? false;
		if (!$apiKey) return [];

		return \nn\t3::Geo( $apiKey )->getCoordinates( $address ) ?: [];
    }
	
	/**
	 * Convert address-string to coordinates using the Google Api.
	 * You must define an apiKey in the Extension Manager to use this method.
	 * 
	 * @param string|float $lng
	 * @param string|float $lat
	 * @return array
	 */
	public static function convertCoordinatesToAddress( $lng = '', $lat = '' )
	{
		$apiKey = \nn\t3::Environment()->getExtConf('zvoove')['googleGeoApiKey'] ?? false;
		if (!$apiKey) return [];

		return \nn\t3::Geo( $apiKey )->getAddress( $lng, $lat ) ?: [];
    }

}