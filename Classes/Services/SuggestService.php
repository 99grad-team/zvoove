<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Services;

class SuggestService
{
	/**
	 * Get list of suggested cities from string.
	 * Used as "lookahead" during the input of the user while entering a city 
	 * 
	 * If you would like to use a different method for the lookup, override the
	 * TypoScript setup at `plugin.tx_zvoove.settings.search.location.suggest`.
	 * 
	 * You should not call this method directly, but use the wrapper which will
	 * delegate the Query to the Service defined in TypoScript.
	 * 
	 * ```
	 * \nn\zv::Suggest()->locations('Wiesb');
	 * ```
	 * 
	 * @param string $str
	 * @param int $limit
	 * @return array
	 */
	public static function getLocations( $str = '', $limit = 20 )
	{
		$path = \nn\t3::File()->absPath('EXT:zvoove/Resources/Private/Suggest/de.locations.php');
	
		$entries = require( $path );
		$results = [];

		foreach ($entries as $name) {
			if (stripos( $name, $str ) === false) continue;
			$results[] = [
				'title' => $name,
			];
			if ($limit && $limit <= count($results)) {
				break;
			}
		}

		return $results;
    }
	
	/**
	 * Get list of suggested jobs from string.
	 * Used as "lookahead" during the input of the user while entering a job-title 
	 * 
	 * If you would like to use a different method for the lookup, override the
	 * TypoScript setup at `plugin.tx_zvoove.settings.search.jobs.suggest`.
	 * 
	 * You should not call this method directly, but use the wrapper which will
	 * delegate the Query to the Service defined in TypoScript.
	 * 
	 * ```
	 * \nn\zv::Suggest()->jobs('Helfer');
	 * ```
	 * 
	 * @param string $str
	 * @return array
	 */
	public static function getJobs( $str = '', $limit = 20 )
	{
		$entries = \nn\zv::Stelle()->getStellenFiltered([
			'keywords' 	=> $str, 
			'pageSize' 	=> $limit,
			'pageNum'	=> 1
		], true);

		$results = [];
		foreach ($entries as $entry) {
			$results[] = [
				'title' 	=> $entry->getTitle(),
				'jobid' 	=> $entry->getJobid(),
				'location' 	=> $entry->getLocation(),
			];
		}
		return $results;
    }

}