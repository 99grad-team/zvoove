<?php 
namespace Nng\Zvoove\Finishers;

use \TYPO3\CMS\Form\Domain\Finishers\AbstractFinisher;

class CreateBewerberFinisher extends AbstractFinisher 
{
	/**
	 * @var array
	 */
	protected $defaultOptions = [
		'fieldGender' 		=> '',
		'fieldFirstname' 	=> '',
		'fieldEmail' 		=> '',
		'fieldPhone' 		=> '',
		'fieldLastname' 	=> '',
		'fieldSubscribe' 	=> '',
		'fieldFiles' 		=> [],
	];

	/**
	 * Execute the finisher
	 * 
	 * @return void
	 */
	protected function executeInternal()
	{
		$formValues = $this->finisherContext->getFormValues();

		$settings = \nn\t3::Settings()->get('tx_zvoove') ?? [];

		$stelleRepository = \nn\t3::injectClass( \Nng\Zvoove\Domain\Repository\StelleRepository::class );
		$applicantRepository = \nn\t3::injectClass( \Nng\Zvoove\Domain\Repository\BewerberRepository::class );
		$applicationRepository = \nn\t3::injectClass( \Nng\Zvoove\Domain\Repository\BewerbungRepository::class );

		// ignore storagePid
		\nn\t3::Db()->ignoreEnableFields( $stelleRepository );
		\nn\t3::Db()->ignoreEnableFields( $applicantRepository );
		\nn\t3::Db()->ignoreEnableFields( $applicationRepository );

		$filesVarName = trim($this->parseOption('fieldFiles'), '{}');
		$request = $this->finisherContext->getControllerContext()->getRequest();

		$zvooveSettings = $settings['application']['sendToZvoove'] ?? [];
		$dbSettings = $settings['application']['saveInDatabase'] ?? [];
		$saveInDb = $dbSettings['enabled'] ?? false;

		$result = [
			'email' 		=> $this->parseOption('fieldEmail'),
			'phone' 		=> $this->parseOption('fieldPhone'),
			'firstname'		=> $this->parseOption('fieldFirstname'),
			'lastname'		=> $this->parseOption('fieldLastname'),
			'gender'		=> $this->parseOption('fieldGender'),
			'files'			=> $request->getArguments()[$filesVarName] ?? [],
		];

		// if applicant already exists in DB, use him
		$applicant = $applicantRepository->findOneByEmail( $result['email'] );
		if (!$applicant) {
			$applicant = new \Nng\Zvoove\Domain\Model\Bewerber();
		}

		// update applicant data from from
		$applicant
			->setEmail($result['email'])
			->setPhone($result['phone'])
			->setGender($result['gender'])
			->setFirstname($result['firstname']) 
			->setLastname($result['lastname'])
			->setPid(intval($dbSettings['pidApplicant'] ?? 0));

		// Persist user to create an UID
		if ($saveInDb) {
			if ($applicant->getUid()) {
				$applicantRepository->update( $applicant );
			} else {
				$applicantRepository->add( $applicant );
			}
			\nn\t3::Db()->persistAll();
		}

		// Add `ObjectUuids` for the `Mandant` and `Stelle`
		$jobid = '';
		$mandantid = '';
		if ($stelleUid = $formValues['stelleUid'] ?? false) {
			if ($stelle = $stelleRepository->findByUid( $stelleUid ) ?? false) {
				$jobid = $stelle->getJobid();
				$mandantid = $stelle->getMandantid();
			} 
		}
		
		// Unique application-ID
		$applicationid = strtolower( join('-', [
			$jobid,
			date('Ymd'),
			$applicant->getUid() ?: $applicant->getLastname()
		]));

		// create a application (`Bewerbung`)
		$application = new \Nng\Zvoove\Domain\Model\Bewerbung();
		$application->setBewerber( $applicant )
			->setMandantid( $mandantid)
			->setJobid( $jobid )
			->setApplicationid( $applicationid )
			->setPid(intval($dbSettings['pidApplication'] ?? 0));
	
		// move uploaded files to destination folder
		$files = \nn\zv::File()->processFileUploads( $result['files'] );

		// convert files to FileReferences and merge to the model
		if ($files) {
			\nn\t3::Obj()->merge( $application, ['files'=>$files]);
		}

		// may we proudly present... a nice Model!
		//\nn\t3::debug($application);

		$errors = false;

		// send the application to the zvoove API
		if ($zvooveSettings['enabled'] ?? false) {
			$result = \nn\zv::BewerberPortal()->createBewerber( $application );
			$errors = $result['success'] !== true;
		}

		// clean up sensitive data, in case no local storage was enabled
		if (!$saveInDb) {
			foreach ($files as $file) {
				\nn\t3::File()->unlink( $file['publicUrl'] );
			}
		} else {
			$applicationRepository->add( $application );
			\nn\t3::Db()->persistAll();
		}
		
		if ($errors) {
			\nn\t3::Exception('There was a problem sending the data to the zvoove API: ' . $result['message'] );
		}

	}
}