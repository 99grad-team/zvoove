<?php
  
declare(strict_types=1);
  
namespace Nng\Zvoove\Pagination;

use Nng\Zvoove\Domain\Repository\StelleRepository;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;

class QueryBuilderPaginator extends \TYPO3\CMS\Core\Pagination\AbstractPaginator
{
	private $queryBuilder;
	private $totalItems = -1;  
	private $paginatedItems = [];
  
	public function __construct(
		QueryBuilder $queryBuilder,
		int $currentPageNumber = 1,
		int $itemsPerPage = 10
	) {
		$this->queryBuilder = $queryBuilder;
		$this->setCurrentPageNumber($currentPageNumber);
		$this->setItemsPerPage($itemsPerPage);
		$this->updateInternalState();
	}
  
	/**
	 * Get the items for the current page.
	 * 
	 * @return array|iterable
	 */
	public function getPaginatedItems(): iterable
	{
		return $this->paginatedItems;
	}
  
	/**
	 * Limit the number of results to currently selected pageNum.
	 * Called by TYPO3 before rendering the results.
	 * 
	 * @return array
	 */
	protected function updatePaginatedItems(int $limit, int $offset): void
	{
		$this->queryBuilder
			->setMaxResults($limit)
			->setFirstResult($offset);
		$this->paginatedItems = StelleRepository::getResults( $this->queryBuilder );
	}
  
	/**
	 * Return the total amount of results for current QueryBuilder.
	 * Builds a subrequest and return the `COUNT()` of result rows.
	 * 
	 * This method is called by TYPO3 before `updatePaginatedItems()`.
	 * 
	 * @return int
	 */
	protected function getTotalAmountOfItems(): int
	{	
		if ($this->totalItems > -1) {
			return $this->totalItems;
		}

		// how low-level can you get? ;)
		$params = $this->queryBuilder->getParameters();

		$queryBuilder = \nn\t3::Db()->getQueryBuilder( \Nng\Zvoove\Domain\Repository\StelleRepository::TBL_STELLE );
		$queryBuilder->getRestrictions()->removeAll();
		$queryBuilder->addSelectLiteral('COUNT(tbl.uid) AS total FROM (' . $this->queryBuilder->getSQL() . ') AS tbl')
			->setParameters( $params );
		
		$total = $queryBuilder->execute()->fetchColumn(0);
		return $this->totalItems = $total ?: 0;
	}
  
	/**
	 * Return the number of items on the current page
	 * 
	 * @return int
	 */
	protected function getAmountOfItemsOnCurrentPage(): int
	{
		return count($this->paginatedItems);
	}

	/**
	 * Public method to get the total number of results from QueryBuilder.
	 * 
	 * @return int
	 */
	public function getTotalItems(): int 
	{
		return $this->getTotalAmountOfItems();
	}
}