<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

/**
 * The `MandantOeffentlicheDatei`-endpoint is actually the (undocumented) API which is
 * used on the company's subdomain of zvoove to retrieve media-files from the zvoove
 * File Abstraction Layer.
 * 
 */
class MandantOeffentlicheDatei extends AbstractUtility
{
	/**
     * @var string
     */
    protected $apiVersion = '/api/v1/';

	/**
	 * Api for sending requests WITHOUT authorization
     * @var Api
     */
    protected $api = null;

	/**
	 * Api for sending requests WITH authorization
	 * @var Api
     */
    protected $apiAuth = null;

	/**
	 * Create instances of Api to send authorized / unauthorized requests
	 * @return void
	 */
	public function __construct() 
	{
		parent::__construct();
		
		$apiKey = \nn\t3::Settings()->getExtConf('zvoove')['openStelleApiKey'] ?? '';

		$this->api 		= \nn\zv::Api();
		$this->apiAuth 	= \nn\zv::Api( $apiKey );

		$this->api->setApiVersionPath( $this->apiVersion );
		$this->apiAuth->setApiVersionPath( $this->apiVersion );
	}

	/**
	 * Return array with infos and raw binary data of the file
	 * 
	 * ```
	 * // get array with information and filedata
	 * \nn\zv::MandantOeffentlicheDatei()->getAsStreamContent( 'some-media-uuid' );
	 * 
	 * // output (show) file in browser
	 * \nn\zv::MandantOeffentlicheDatei()->getAsStreamContent( 'some-media-uuid', true );
	 * ```
	 * @param string $uuid
	 * @return array|null
	 */
	public function getAsStreamContent( $uuid = '', $output = false ) 
	{
		$result = $this->api->getStream( 'MandantOeffentlicheDatei/GetAsStreamContent', [
			'uuid' 		=> $uuid,
			'option'	=> '',
		]);

		if ($output) {
			header( 'Content-Type: ' . $result['type'] );
			echo $result['data']; 
			die();
		}

		return $result;
	}

}