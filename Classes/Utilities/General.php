<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

/**
 * General and commonly used methods
 * 
 */
class General extends AbstractUtility 
{
	/**
	 * Append (m/w/d) to string, if not already present
	 * 
	 * ```
	 * \nn\zv::General()->appendGender('title');
	 * \nn\zv::General()->appendGender('title', '(m/w/d)', '(m');
	 * ```
	 * @param string $str
	 * @return array
	 */
	public function appendGender( $str = '', $append = '', $ifNot = '' ) 
	{
		if (!$str) return '';
		
		$defaultLanguage = \nn\t3::Environment()->getDefaultLanguage();

		if (!$append) {
			// `(m/w/d)`
			$append = \nn\t3::LL()->get( 'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:gender.append', null, null, null, $defaultLanguage );
		}
		if (!$ifNot) {
			// `(m`
			$ifNot = \nn\t3::LL()->get( 'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:gender.appendIfNot', null, null, null, $defaultLanguage );
		}

        if (stripos($str, $ifNot) === false) {
            return trim($str).' ' . $append;
        }

		return $str;
	}

	/**
	 * Strip (m/w/d) from a string
	 * 
	 * ```
	 * \nn\zv::General()->stripGender('title (m/w/d)');
	 * ```
	 * @param string $str
	 * @return array
	 */
	public function stripGender( $str = '' ) 
	{
		return trim(preg_replace('/\(.*\)/', '', $str));
	}

	/**
	 * Remove inline-styles and markup that was passed from zvoove RTE and
	 * would make the display on the website ugly.
	 * 
	 * ```
	 * \nn\zv::General()->cleanHtml('<span style="ugly: stuff;"><i>someting</i></span>');
	 * ```
	 * @param string $str
	 * @return string
	 */
	public function cleanHtml( $str = '' ) 
	{
		if (!$str) return '';

		// only keep certain tags
		$str = strip_tags( $str, '<p><br><b><strong><ul><li><a><i>' );

		// replace &nbsp; with normal spaces
		$str = str_replace( '&nbsp;', ' ', $str );

		// remove style-attributes
		$str = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $str);

		// remove empty tags, e.g. `<p> </p>`
		$str = preg_replace('/(<[^>]+)\s*(<[^>]+)/i', '', $str);

		return $str;
	}

}