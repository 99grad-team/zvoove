<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

use Nng\Zvoove\Domain\Model\Stelle;
use Nng\Zvoove\Domain\Repository\StelleRepository;

/**
 * Helpers for syncing vacancies between the zvoove API and the local database
 * 
 */
class Sync extends AbstractUtility {

	/**
	 * Enumerations for the steps during sync
	 */
	const IDLE 					= 0;	// Idle, waiting to begin processing
	const UPDATE_LIST_STELLEN 	= 1;	// The list of `Stellen` is being retrieved from the API
	const CLEAN_DB 				= 2;	// Remove entries from local DB that are missing in zvoove
	const UPDATE_OBJECTS 		= 3;	// Update filter-categories from zvoove
	const UPDATE_LOCAL_DB 		= 4;	// The list of `Stellen` is being updated in local DB
	const FINISHED 				= 100;	// Done with processing

	/**
	 * Container (template) for statistics
	 */
	protected $stats = [
		'processed' => [],
		'updated' 	=> [],
		'deleted'	=> [],
		'inserted'	=> [],
		'errors'	=> []
	];

	/**
	 * Flag, if process should be aborted, e.g. if an error occurs
	 * @var boolean
     */
    protected $aborted = false;

	/**
	 * Sync-Settings from TypoScript setup
	 * @var array
     */
    protected $syncSettings = [];
	
	/**
	 * Local objects
	 * @var array
     */
    protected $localObjects = [];

	/**
	 * Repository for local `Stelle`-entries
	 * @var StelleRepository
     */
    protected $stelleRepository = null;


	/**
	 * Constructor
	 * @return void
	 */
	public function __construct() 
	{
		parent::__construct();
		$this->stelleRepository = \nn\t3::injectClass( StelleRepository::class );
		$this->syncSettings = $this->settings['sync'] ?? [];
	}
	
	/**
	 * Get all Object from local DB and cache.
	 * Key is the `ObjectUuid` from zvoove.
	 * 
	 * ```
	 * \nn\t3::Sync()->getLocalObjects();
	 * \nn\t3::Sync()->getLocalObjects('department');
	 * \nn\t3::Sync()->getLocalObjects('department', 'title');
	 * ```
	 * 
	 * @return array
	 */
	public function getLocalObjects ( $entity = '', $key = 'obj_uuid', $value = '' ) 
	{
		$localObjects = $this->localObjects ?? false;
		
		if (!$localObjects) {
			$tableName = 'tx_zvoove_domain_model_obj';
			$existingObjects = \nn\t3::Db()->findAll($tableName);
			$localObjects = $this->localObjects = $existingObjects;
		}

		if ($entity) {
			$localObjects = array_filter( $localObjects, function ($a) use ($entity) {
				return $a['entity'] == $entity;
			});
		}

		if ($key) {
			$values = !$value ? array_values($localObjects) : array_column($localObjects, $value);
			$localObjects = array_combine( 
				array_column($localObjects, $key),
				array_values($values)
			);
		}

		return $localObjects;
	}

	/**
	 * Convert a list of zvoove objectUuids to local uids
	 * 
	 * ```
	 * \nn\t3::Sync()->convertObjectUuidsToLocalUids( ['asafd-dfdfejd-4858lfdf', '...'] );
	 * // => [2, 3, ...]
	 * ```
	 * @param array $objectUuids
	 * @return array
	 */
	public function convertObjectUuidsToLocalUids( $objectUuids ) 
	{
		$map = $this->getLocalObjects( null, 'obj_uuid', 'uid' );
		$result = [];
		foreach ($objectUuids as $uuid) {
			if (!isset($map[$uuid])) continue;
			$result[] = $map[$uuid];
		}
		return $result;
	}

	/**
	 * Abort the process.
	 * Resets everything, flags the process as aborted (to end any running loop)
	 * 
	 * @param string $reason
	 * @return void
	 */
	public function abort( $reason = '' ) 
	{
		$this->aborted = true;
		\nn\zv::Sync()->init( self::IDLE );

		// let the next cron call execute the run() directly
		\nn\t3::Registry()->set('zvoove', 'lastRun', 0);

		\nn\t3::Log()->error('\nn\zv::Sync()->run() was aborted. Reason: ' . $reason );
	}

	/**
	 * This method is the main entry point for the syncing process. It is called by the scheduler
	 * and successively takes care of calling all steps neccessary for syncing the data between
	 * the zvoove API and the local database.
	 * 
	 * The scheduler should be calling this script every few minutes.
	 * 
	 * ```
	 * \nn\zv::Sync()->run();
	 * ```
	 * @return void
	 */
	public function run() 
	{
		$lastRun = \nn\t3::Registry()->get('zvoove', 'lastRun') ?? 0;
		$timeSinceLastExecution = time() - $lastRun;

		$progress = \nn\t3::Registry()->get('zvoove', 'sync');
		$status = $progress['status'] ?? self::IDLE;

		// check if last execution was aborted or if there was a timeOut
		if ($status != self::IDLE) {

			$tstamp = $progress['tstamp'] ?? 0;
			$execDuration = time() - $tstamp;

			// timeout! Write error in Log and reset
			if ($execDuration > $this->syncSettings['maxExecutionTime']) {
				\nn\t3::Log()->error('Timeout during execution of \nn\zv::Sync()->run()');
				$progress = \nn\t3::Registry()->set('zvoove', 'sync', [
					'status' => self::IDLE
				]);
			}
		}

		// process all vacancies in a single run?
		if ($this->syncSettings['processAll']) {

			if ($timeSinceLastExecution > $this->syncSettings['processInterval']) {
								
				$status = $progress['status'] ?? self::IDLE;
				if ($status != self::IDLE) {
					return $progress;
				}

				$processAllMaxRuns = intval($this->syncSettings['processAllMaxRuns']);

				while (($status != self::FINISHED) && !$this->aborted) {

					// Avoid infinite loops (potential DoS)
					$processAllMaxRuns--;
					if ($processAllMaxRuns <= 0) {
						$this->abort('Infinite loop on \nn\zv::Sync()->run(). Check the settings for processAllMaxRuns!');
						break;
					}

					// catch errors and exceptions
					$error = false;
					try {
						$progress = $this->exec();
					} catch( \Error $e ) {
						$error = $e;
					} catch( \Exception $e ) {
						$error = $e;
					} 

					if ($this->aborted) {
						break;
					}

					if ($error) {
						\nn\t3::Log()->error('Exception during execution of \nn\zv::Sync()->run()', $error->getMessage() );
						$this->abort('Error or Exception when running \nn\zv::Sync()->run()');
						break;
					}

					if (!$progress) {
						\nn\t3::Log()->error('No $progress returned during execution of \nn\zv::Sync()->run()' );
						$this->abort('No $progress returned.');
						break;
					}

					$status = $progress['status'];
				}

				$this->clearCache($progress);
				\nn\t3::Registry()->set('zvoove', 'lastRun', time());
			}

		} else {

			// execute in "portions", not a single run
			$status = $progress['status'] ?? self::IDLE;

			// if currently not doing anything and time to next execution not reached: Abort
			if ($status == self::IDLE) {
				if ($timeSinceLastExecution < $this->syncSettings['processInterval']) {
					return $progress;
				}
			}

			// in progress. Process next batch
			$progress = $this->exec();

			// finished? Then stop and delay till next execution time is reached
			if ($progress['status'] == self::FINISHED) {
				\nn\t3::Registry()->set('zvoove', 'lastRun', time());
				$this->clearCache($progress);
			}
		}

		return $progress;
	}

	/**
	 * Execute the next task.
	 * 
	 * ```
	 * \nn\zv::Sync()->exec();
	 * ```
	 * @return void
	 */
	public function exec() 
	{
		$progress = \nn\t3::Registry()->get('zvoove', 'sync');
		$status = $progress['status'] ?? self::IDLE;

		// done with last execution – or first execution ever
		if ($status == self::IDLE) {
			$progress = $this->init();
			$status = $progress['status'] ?? -1;
		}

		// successively load all vacancies from zvoove API
		if ($status == self::UPDATE_LIST_STELLEN) {
			$progress = $this->exec_updateListStellen();
			$status = $progress['status'] ?? -1;
		}

		// successively load all vacancies from zvoove API
		if ($status == self::CLEAN_DB) {
			$progress = $this->exec_cleanDb();
			$status = $progress['status'] ?? -1;
		}
		
		// update all objects / filter-categories from zvoove API
		if ($status == self::UPDATE_OBJECTS) {
			$progress = $this->exec_createFilterRelations();
			$status = $progress['status'] ?? -1;
		}

		// successively load all vacancies from zvoove API
		if ($status == self::UPDATE_LOCAL_DB) {
			$progress = $this->exec_updateLocalDb();
			$status = $progress['status'] ?? -1;
		}

		// done with everything
		if ($status == self::FINISHED) {
			\nn\t3::Registry()->set('zvoove', 'sync', [
				'status' 	=> self::IDLE,
				'results' 	=> $this->stats,
			], true);
		}

		// something went wrong
		if (!$this->aborted && ($status == -1 || !$progress)) {
			$this->abort('\nn\t3::Sync()->exec() resulted in status -1 and/or empty $progress');
		}

		return $progress;
	}

	/**
	 * Reset everything and prepare for synchronisation between zvoove API and local database.
	 */
	public function clearCache( $progress = [] ) 
	{
		$stats = $progress['results'];
		$anyUpdated = $stats['updated'] || $stats['deleted'] || $stats['inserted'];
		if ($anyUpdated) {
			\nn\t3::Cache()->clearPageCache();
		}
	}

	/**
	 * Reset everything and prepare for synchronisation between zvoove API and local database.
	 * Clears the cache from the TYPO3 registry and restarts the process
	 * ```
	 * // set status to UPDATE_LIST_STELLEN
	 * \nn\zv::Sync()->init();
	 * 
	 * // Set status to idle
	 * \nn\zv::Sync()->init( self::IDLE );
	 * \nn\zv::Sync()->init( \Nng\Zvoove\Utilities\Sync::IDLE );
	 * ```
	 * @return void
	 */
	public function init( $status = false ) 
	{
		
		if ($status === false) {
			$status = self::UPDATE_LIST_STELLEN;
		}

		return \nn\t3::Registry()->set('zvoove', 'sync', [
			'tstamp'	=> time(),
			'status' 	=> $status,
			'items'		=> [],
			'total'		=> 0,
			'current'	=> 0, 
			'results'	=> $this->stats,
		], true);
	}

	/**
	 * Get the next batch (page) of vacancies (`Stellen`) from the zvoove API.
	 * Save the jobid and last modification date in the TYPO3 registry - but do
	 * not compare yet to the entries in the local database.
	 * ```
	 * \nn\zv::Sync()->exec_updateListStellen();
	 * ```
	 * @return array
	 */
	public function exec_updateListStellen() 
	{		
		$progress = \nn\t3::Registry()->get('zvoove', 'sync');

		$maxItemsPerRequest = $this->syncSettings['maxItemsPerRequest'];
		$currentPage = $progress['current'] ?? 0;
		$currentPage++;

		$result = \nn\zv::Stelle()->getStellenFiltered([
			'pageNo' 	=> $currentPage,
			'pageSize' 	=> $maxItemsPerRequest,
		]);

		$success = $result['success'] ?? false;

		// something went wrong? Log and exit
		if (!$success) {
			$this->abort('\nn\zv::Sync()->exec_updateListStellen() resulted in no success after calling \nn\zv::Stelle()->getStellenFiltered()');
			\nn\t3::Log()->error('zvoove', 'exec_updateListStellen()', $result['message']);
			return;
		}

		$totalPages = ceil($result['total'] / $maxItemsPerRequest);
		$status = $currentPage >= $totalPages ? self::CLEAN_DB : self::UPDATE_LIST_STELLEN;
		
		// flatten and parse results
		$items = $result['items'];
		array_walk( $items, function ( &$item ) {
			$item = [
				'jobid' 	=> $item['StelleUuid'],
				'tstamp'	=> strtotime($item['LastUpdateTime']),
			];
		});

		foreach ($items as $item) {
			$progress['items'][] = $item;
		}

		$progress = \nn\t3::Registry()->set('zvoove', 'sync', array_merge($progress, [
			'status'	=> $status,
			'current' 	=> $currentPage,
			'total'		=> $totalPages,
		]), true);

		return $progress;
	}

	/**
	 * Remove all entries in local database that are not present in the zvoove API.
	 * This method must be called after `exec_updateListStellen()` has completed and
	 * before `exec_updateLocalDb()` is called.
	 * 
	 * ```
	 * \nn\zv::Sync()->exec_cleanDb();
	 * ```
	 * @return array
	 */
	public function exec_cleanDb() 
	{
		$progress = \nn\t3::Registry()->get('zvoove', 'sync');
		$items = $progress['items'];

		$jobUuids = array_column($items, 'jobid');
		$removedEntries = \nn\t3::Db()->findNotIn( \Nng\Zvoove\Utilities\Stelle::TABLENAME, 'jobid', $jobUuids );
		foreach ($removedEntries as $entry) {
			$jobid = $entry['jobid'];
			\nn\t3::Db()->delete( \Nng\Zvoove\Utilities\Stelle::TABLENAME, ['jobid'=>$jobid] );
			$progress['results']['deleted'][] = $jobid;
		}

		$progress = \nn\t3::Registry()->set('zvoove', 'sync', array_merge($progress, [
			'status' => self::UPDATE_OBJECTS
		]), true);

		return $progress;
	}

	/**
	 * Iterate through list of all vacancies retrieved from zvoove API.
	 * Load details for the vacancies and update the entry in the local database.
	 * 
	 * ```
	 * \nn\zv::Sync()->exec_updateLocalDb();
	 * ```
	 * @return array
	 */
	public function exec_updateLocalDb() 
	{
		$progress = \nn\t3::Registry()->get('zvoove', 'sync');
		
		// grab next items to update from list
		$maxItemsPerLocalSync = intval($this->syncSettings['maxItemsPerLocalSync']);
		$items = array_splice($progress['items'], 0, $maxItemsPerLocalSync);

		$results = $this->updateStellen( $items );
		foreach ($results as $k=>$result) {
			$progress['results'][$k] = array_merge($progress['results'][$k] ?: [], $result);
		}

		// nothing left to process?
		$status = !$progress['items'] ? self::FINISHED : self::UPDATE_LOCAL_DB;

		$progress = \nn\t3::Registry()->set('zvoove', 'sync', array_merge($progress, [
			'status' => $status,
		]), true);

		return $progress;
	}

	/**
	 * Updates a local `Stelle` DB-entry (or multiple entries) by retrieving the
	 * data from the API for the given entries and updating (or inserting) the
	 * local data.
	 * 
	 * ```
	 * // pass a single jobUuid
	 * \nn\zv::Sync()->updateStellen( $jobid );
	 * 
	 * // ...or a list of jobUuids
	 * \nn\zv::Sync()->updateStellen( [$jobid, $jobid] );
	 * 
	 * // ...or an array with objects
	 * \nn\zv::Sync()->updateStellen( [['jobid'=>'...', 'tstamp'=>''], ...] );
	 * 
	 * // pass `true` to force an update, even if external modification date has not changed
	 * \nn\zv::Sync()->updateStellen( $jobid, true );
	 * ```
	 * @param array|string $jobsToUpdate
	 * @return array
	 */
	public function updateStellen( $jobsToUpdate = [], $forceUpdate = false ) 
	{		
		// normalize to array
		if (!is_array($jobsToUpdate)) {
			$jobsToUpdate = [$jobsToUpdate];
		}
		foreach ($jobsToUpdate as &$job) {
			if (!is_array($job)) {
				$job = [
					'jobid' 	=> $job,
					'tstamp' 	=> time(),
				];
			}
		}

		$respectModificationDate = $this->syncSettings['respectModificationDate'];

		$jobsToUpdateByJobUuid = \nn\t3::Arrays( $jobsToUpdate )->key('jobid')->toArray();
		$jobUuids = array_keys( $jobsToUpdateByJobUuid );
		
		$result = [
			'processed' => $jobUuids,
			'updated' => [],
			'inserted' => [],
		];

		$localEntries = \nn\t3::Db()->findIn( \Nng\Zvoove\Utilities\Stelle::TABLENAME, 'jobid', $jobUuids );
		$localEntriesByJobid = \nn\t3::Arrays( $localEntries )->key('jobid')->toArray();

		// Exclude entries which were not changed
		if ($respectModificationDate && !$forceUpdate) {
			foreach ($jobsToUpdateByJobUuid as $uuid=>$job) {
				$localTstamp = $localEntriesByJobid[$uuid]['api_tstamp'] ?? 0;
				if ($job['tstamp'] <= $localTstamp) {
					unset($jobsToUpdateByJobUuid[$uuid]);
				}
			}
		}

		$jobUuids = array_keys( $jobsToUpdateByJobUuid );
		$pid = $this->settings['jobs']['pid'] ?? 0;

		$apiEntries = \nn\zv::Stelle()->getStellenByIds( $jobUuids );

		if (!$apiEntries) {
			$result['errors'] = $jobUuids;
			return $result;
		}

		foreach ($apiEntries as $vacancy) {

			$uuid = $vacancy['StelleUuid'];
			$existingJob = $localEntriesByJobid[$uuid] ?? false;
			$tstamp = $jobsToUpdateByJobUuid[$uuid]['tstamp'] ?? time();

			$dateFrom = strtotime($vacancy['Beginn'] ?: $vacancy['DatumAb']);

			if (!$existingJob) {
				$stelle = new Stelle();
			} else {
				$stelle = $this->stelleRepository->findByUid( $existingJob['uid'] );
			}

			// process files: key is field in local Model, value-array are fields to parse
			$falMapping = [
				'images' 		=> ['Image1Uuid'],
				'imagesContact' => ['ImageKontaktUuid'],
			];

			$media = [];
			if ($this->syncSettings['copyMedia']) {
				foreach ($falMapping as $prop=>$list) {
					$media[$prop] = [];
					foreach ($list as $mediaField) {

						// get file-stream from API and copy to local fileadmin
						if ($mediaUuid = $vacancy[$mediaField] ?? false) {
							if ($sysFile = \nn\zv::File()->getSysFileForStreamUuid( $mediaUuid )) {
								$media[$prop][] = \nn\t3::File()->getPublicUrl( $sysFile );
							}
						}
					}
				}
			}

			$title = $vacancy['Bezeichnung'];

			// automatically append (m/w/d)?
			if ($this->syncSettings['appendGender']) {
				$title = \nn\zv::General()->appendGender( $title );
			}

			// remove unwanted tags and inline-styles?
			if ($fieldsToClean = $this->syncSettings['cleanHtml']) {
				$fieldsToClean = \nn\t3::Arrays($fieldsToClean)->trimExplode();
				$jsonLd = json_decode($vacancy['JsonLd'] ?? '', true) ?: [];
				foreach ($fieldsToClean as $k) {

					// clean standard zvoove data
					if ($str = $vacancy[$k] ?? false) {
						$cleaned = \nn\zv::General()->cleanHtml( $str );
						$vacancy[$k] = $cleaned;
					}

					// clean schema JSON
					if ($str = $jsonLd[$k] ?? false) {
						$cleaned = \nn\zv::General()->cleanHtml( $str  );
						$jsonLd[$k] = $cleaned;
					}
				}
				$vacancy['JsonLd'] = json_encode( $jsonLd );
			}

			$data = [
				'dateFrom'		=> $dateFrom,
				'apiTstamp'		=> $tstamp,
				'jobid' 		=> $uuid,
				'title' 		=> $title,
				'location' 		=> $vacancy['EinsatzortOrt'],
				'zip' 			=> $vacancy['EinsatzortPlz'],
				'country' 		=> $vacancy['EinsatzortLandIso002'],
				'district' 		=> $vacancy['EinsatzortBundesland'],
				'lat' 			=> floatval( $vacancy['EinsatzortGeografischeBreite'] ),
				'lng' 			=> floatval( $vacancy['EinsatzortGeografischeLaenge'] ),
				'contract' 		=> $vacancy['VertragsartBezeichnung'] ?? $vacancy['VertragsartenString'],
				'pid'			=> $pid,
				'data'			=> $vacancy,
				'images'		=> $media['images'] ?? [],
				'imagesContact'	=> $media['imagesContact'] ?? [],
			];

			$existingObjectsByUuid = $this->getLocalObjects( null, 'obj_uuid', 'uid' );

			// Map relations to uids of Objects in local database
			$mapSettings = $this->settings['search']['filters'] ?? [];
			foreach ($mapSettings as $entity=>$config) {
				$fieldLocal = $config['fieldLocal'] ?? false;
				if (!$fieldLocal) continue;
				if ($fieldInStelle = $config['fieldInStelle'] ?? false) {
					$mapping = array_flip(array_column($vacancy[$fieldInStelle] ?? [], 'ObjectUuid'));
					$uids = array_values(array_intersect_key( $existingObjectsByUuid, $mapping ));
					$data[$fieldLocal] = $uids;
				}
			}

			$stelle = \nn\t3::Obj()->merge( $stelle, $data );

			if (!$existingJob) {
				$result['inserted'][] = $stelle->getJobid();
				$this->stelleRepository->add( $stelle );
			} else {
				$result['updated'][] = $stelle->getJobid();
				$this->stelleRepository->update( $stelle );
			}

			\nn\t3::Db()->persistAll();
			\nn\t3::Slug()->create( $stelle );
			
		}
		
		return $result;
	}

	/**
	 * In zvoove, every category / fitler-criteria for contract-type, department etc.
	 * is stored as an `Object`. The job / vacancy (`Stelle`) has MM-relations to these Objects.
	 * 
	 * This method syncs the `Object` from zvoove with the table `tx_zvoove_domain_model_obj`
	 * in the local database. This is important to set relations between the locally stored
	 * `Stelle` can be categorized and filtered by these Objects.
	 * 
	 * The TypoScript settings `plugin.tx_zvoove.settings.search.filters` define, which `Objects`
	 * should be retrieved from zvoove.
	 * 
	 * We are using low-level queries from EXT:nnhelpers which increases performance drastically.
	 * 
	 * ```
	 * \nn\zv::Sync()->exec_createFilterRelations();
	 * ```
	 * @return array
	 */
	public function exec_createFilterRelations() 
	{
		$progress = \nn\t3::Registry()->get('zvoove', 'sync');

		$pid = $this->syncSettings['objectsPid'];
		$tableName = 'tx_zvoove_domain_model_obj';

		// get existing object in local DB
		$existingObjectsByUuid = $this->getLocalObjects();

		// get all objects related to filters from zvoove, disable cache
		$allFilters = \nn\zv::Stelle()->getAllFilters( false );

		// if anything went wrong with request from API, this array will be empty
		if ($allFilters) {

			$allObjectsByUuid = [];

			foreach ($allFilters as $entityKey=>$v) {
				$options = $v['options'] ?? [];
				$allObjectsByUuid = array_merge( $allObjectsByUuid, $options );

				foreach ($options as $uuid=>$title) {
					$existingObj = $existingObjectsByUuid[$uuid] ?? false;
			
					if (!$existingObj) {
						// Object doesn't exist? INSERT it in local DB
						\nn\t3::Db()->insert($tableName, [
							'pid' => $pid,
							'title' => $title,
							'entity' => $entityKey,
							'obj_uuid' => $uuid,
							'tstamp' => time(),
							'crdate' => time(),
						]);
					} else {
						// Object exist? UPDATE the title, if it changed
						if ($title != $existingObj['title'] || $entityKey != $existingObj['entity']) {
							\nn\t3::Db()->update($tableName, [
								'title' => $title,
								'entity' => $entityKey,
								'tstamp' => time(),
							], $existingObj['uid']);
						}
					}
				}
			}

			// Find entities which are not in use anymore
			$diff = array_diff(array_keys($existingObjectsByUuid), array_keys($allObjectsByUuid));
			if ($diff) {
				foreach ($diff as $obj_uuid) {
					\nn\t3::Db()->delete($tableName, ['obj_uuid'=>$obj_uuid]);
				}
			}
		}		

		$progress = \nn\t3::Registry()->set('zvoove', 'sync', array_merge($progress, [
			'status' => self::UPDATE_LOCAL_DB
		]), true);

		return $progress;
	}
}