<?php 

namespace Nng\Zvoove\Utilities;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

/**
 * Helper for making GET/POST request via CURL to the zoove API.
 * 
 */
class Api {

	/**
     * @var string
     */
    protected $apiKey = '';

	/**
     * @var string
     */
    protected $apiVersionPath = '/api/public/v1/';

	/**
     * @var string
     */
    protected $apiBaseUrl = '';

	/**
	 * Constructor
	 * 
	 * ```
	 * \nn\zv::Api( $apiKey );
	 * ```
	 * @param string $apiKey
	 * @return void
	 */
	public function __construct( $apiKey = '' ) 
	{
		$this->apiKey = $apiKey;
		$this->setApiVersionPath( $this->apiVersionPath );
	}

	/**
	 * Send a GET request
	 * 
	 * ```
	 * \nn\zv::Api( $apiKey )->GET( '/api/test/to/endpoint' );
	 * \nn\zv::Api( $apiKey )->GET( '/api/test/to/endpoint', ['test'=>123] );
	 * \nn\zv::Api( $apiKey )->GET( '/api/test/to/endpoint', ['test'=>123], $headers );
	 * ```
	 * @return array
	 */
	public function GET( $url = '', $requestParams = [], $additionalHeaders = [] ) 
	{
		return $this->request( 'GET', $url, $requestParams, $additionalHeaders );
	}

	/**
	 * Send a GET request, return RAW data (no parsing of json)
	 * 
	 * ```
	 * \nn\zv::Api( $apiKey )->rawGET( '/api/test/to/endpoint' );
	 * \nn\zv::Api( $apiKey )->rawGET( '/api/test/to/endpoint', ['test'=>123] );
	 * \nn\zv::Api( $apiKey )->rawGET( '/api/test/to/endpoint', ['test'=>123], $headers );
	 * ```
	 * @return string
	 */
	public function rawGET( $url = '', $requestParams = [], $additionalHeaders = [] ) 
	{
		return $this->request( 'GET', $url, $requestParams, $additionalHeaders, true );
	}
	
	/**
	 * Send a GET request, get stream data. 
	 * return filedata, size, headers, filename and data from stream.
	 * 
	 * ```
	 * \nn\zv::Api( $apiKey )->getStream( '/api/test/endpoint' );
	 * ```
	 * @return array
	 */
	public function getStream( $url = '', $requestParams = [], $additionalHeaders = [] ) 
	{
		$headers = $this->getHeaders();

		if ($additionalHeaders) {
			$headers = array_merge( $headers, $additionalHeaders );
		}

		$client = new Client([
			'base_uri' 	=> $this->apiUrl,
			'headers' 	=> $headers,
			'query'		=> $requestParams
		]);

		try {
			$response = $client->request('GET', $url);
		} catch( \Exception $e ) {
			return [
				'success'	=> false,
				'status'	=> $e->getCode(),
				'message'	=> $e->getMessage(),
				'data'		=> '',
			];
		}

		$responseHeaders = $response->getHeaders();
		array_walk($responseHeaders, function( &$a ) {
			if (is_array($a)) {
				$a = array_pop($a);
			}
		});

		$type = $responseHeaders['Content-Type'];
		$filename = $this->getFilenameFromContentDisposition( $responseHeaders['Content-Disposition'] );

		if (!$filename) {
			$filename = uniqid();
		}

		// add suffix from mimetype
		if (!\nn\t3::File()->suffix($filename)) {
			$suffix = \nn\t3::File()->suffixForMimeType( $type );
			$filename = \nn\t3::File()->addSuffix( $filename, $suffix );
		}

		if (\nn\t3::File()->isForbidden($filename)) {
			return [
				'success' 	=> 'false', 
				'status'	=> '403', 
				'message'	=> 'Filetype is no allowed'
			];
		}

		return [
			'headers'	=> $responseHeaders,
			'size'		=> intval($responseHeaders['Content-Length']),
			'type'		=> $type,
			'filename'	=> $filename,
			'data' 		=> $response->getBody()->getContents()
		];
	}

	/**
	 * Extract filename from `Content-Disposition`-Header
	 * 
	 * ```
	 * \nn\zv::Api()->getFilenameFromContentDisposition( 'inline; filename=Airport_Services_Terminal_Flughafen.jpg' );
	 * ```
	 * @return string
	 */
	public function getFilenameFromContentDisposition( $header = '' ) 
	{
		if (preg_match('/filename="(.+?)"/', $header, $matches)) {
			return $matches[1];
		}
		if (preg_match('/filename=([^; ]+)/', $header, $matches)) {
			return rawurldecode($matches[1]);
		}
		return false;
	}

	/**
	 * Send a POST request
	 * 
	 * ```
	 * \nn\zv::Api( $apiKey )->POST( '/api/test/to/endpoint' );
	 * \nn\zv::Api( $apiKey )->POST( '/api/test/to/endpoint', ['test'=>123] );
	 * \nn\zv::Api( $apiKey )->POST( '/api/test/to/endpoint', ['test'=>123], $headers );
	 * ```
	 * @return array
	 */
	public function POST( $url = '', $requestParams = [], $additionalHeaders = [] ) 
	{
		return $this->request( 'POST', $url, $requestParams, $additionalHeaders );
	}

	/**
	 * Get default headers for request
	 * ```
	 * \nn\zv::Api( $apiKey )->getHeaders();
	 * ```
	 * @return array
	 */
	public function getHeaders() 
	{
		$headers = [
			'Accept' 		=> 'application/json',
			'Content-Type' 	=> 'application/x-www-form-urlencoded; charset=utf-8',
		];
		if ($apiKey = $this->apiKey) {
			$headers['X-ApiKey'] = $apiKey; 
		}
		return $headers;
	}

	/**
	 * Generic method for sending the CURL request.
	 * Use this for simple GET / POST requests that don't need multipart/formdata
	 * 
	 * ```
	 * \nn\zv::Api( $apiKey )->request( 'GET', '/endpoint', ['test'=>123] );
	 * \nn\zv::Api( $apiKey )->request( 'GET', '/endpoint', ['test'=>123], [], true );
	 * \nn\zv::Api( $apiKey )->request( 'GET', '/endpoint', ['test'=>123], $headers, true );
	 * ```
	 * @return array
	 */
	public function request( $method = 'GET', $url = '', $requestParams = [], $additionalHeaders = [], $returnRaw = false ) 
	{	
		$headers = $this->getHeaders();

		if ($additionalHeaders) {
			$headers = array_merge( $headers, $additionalHeaders );
		}

		if (!$returnRaw) {
			$headers['Accept-Encoding'] = 'gzip, deflate';
		}

		$client = new Client([
			'base_uri' 	=> $this->apiUrl,
			'headers' 	=> $headers,
			'query'		=> $requestParams
		]);

		return $this->exec( $client, $method, $url, $returnRaw );
	}

	/**
	 * Generic method for sending a multipart/formdata CURL request
	 * 
	 * ```
	 * \nn\zv::Api( $apiKey )->sendMultipartRequest( '/endpoint', ['test'=>123] );
	 * ```
	 * @param string $url
	 * @param array $postData
	 * @return array
	 */
	public function sendMultipartRequest( $url = '', $formDataArray = [] ) 
	{
		$multipartFormData = [];
		foreach ($formDataArray as $k=>$v) {
			
			if (is_array($v)) {
				$v = json_encode($v);
			}
			if (\nn\t3::Obj()->isStorage($v)) {
				foreach ($v as $fal) {
					if ($publicUrl = \nn\t3::File()->getPublicUrl($fal)) {
						$v = \GuzzleHttp\Psr7\Utils::tryFopen( \nn\t3::File()->absPath($publicUrl), 'r');
						$multipartFormData[] = [
							'name'		=> $k,
							'contents'	=> $v,
						];			
					}
				}
				continue;
			}
			$multipartFormData[] = [
				'name'		=> $k,
				'contents'	=> $v,
			];
		}

		$client = new Client([
			'base_uri' 	=> $this->apiUrl,
			'headers' 	=> $this->getHeaders(),
			'multipart' => $multipartFormData,
		]);

		return $this->exec( $client, 'POST', $url );
	}
	
	/**
	 * Generic method for POSTing RAW body data via CURL request
	 * 
	 * ```
	 * \nn\zv::Api( $apiKey )->postJSON( '/endpoint', 'something' );
	 * \nn\zv::Api( $apiKey )->postJSON( '/endpoint', ['test'=>123] );
	 * ```
	 * @param string $url
	 * @param array|string $body
	 * @return array
	 */
	public function postJSON( $url = '', $body = '' ) 
	{
		$client = new Client([
			'base_uri' 	=> $this->apiUrl,
			'headers' 	=> $this->getHeaders(),
			'json' 		=> $body,
		]);
		return $this->exec( $client, 'POST', $url );
	}


	/**
	 * Generic method for sending the CURL request
	 * 
	 * ```
	 * \nn\zv::Api()->exec( $client, '/endpoint', ['test'=>123] );
	 * ```
	 * @param \GuzzleHttp\Client $client
	 * @param string $method
	 * @param string $url
	 * @param boolean $returnRaw
	 * @return array
	 */
	public function exec( $client, $method = 'GET', $url = '', $returnRaw = false )
	{
		try {
			$response = $client->request($method, $url);
		} catch( \Exception $e ) {
			return [
				'success'	=> false,
				'status'	=> $e->getCode(),
				'message'	=> $e->getMessage(),
				'data'		=> [],
			];
		}
		
		$contents = $response->getBody()->getContents();
		if ($returnRaw) {
			return $contents;
		}

		return [
			'success'	=> true,
			'status' 	=> $response->getStatusCode(),
			'message'	=> $response->getReasonPhrase(),
			'data'		=> json_decode( $contents, true ),
		];
	}

	/**
	 * @return  string
	 */
	public function getApiVersionPath() {
		return $this->apiVersionPath;
	}

	/**
	 * @param   string  $apiVersionPath  
	 * @return  self
	 */
	public function setApiVersionPath($apiVersionPath) {
		$this->apiVersionPath = $apiVersionPath;
		$this->apiUrl = rtrim(\nn\t3::Settings()->getExtConf('zvoove')['apiUrl'] ?? '', '/') . $this->apiVersionPath;
		return $this;
	}
}