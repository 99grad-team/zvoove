<?php
declare(strict_types = 1);

namespace nn;

use Nng\Zvoove\Utilities\Api;
use Nng\Zvoove\Utilities\Bewerber;
use Nng\Zvoove\Utilities\BewerberPortal;
use Nng\Zvoove\Utilities\Bewerbung;
use Nng\Zvoove\Utilities\File;
use Nng\Zvoove\Utilities\Form;
use Nng\Zvoove\Utilities\General;
use Nng\Zvoove\Utilities\Geo;
use Nng\Zvoove\Utilities\Katalog;
use Nng\Zvoove\Utilities\MandantOeffentlicheDatei;
use Nng\Zvoove\Utilities\Stelle;
use Nng\Zvoove\Utilities\Suggest;
use Nng\Zvoove\Utilities\Sync;
use Nng\Zvoove\Utilities\TCA;

class zv 
{	
	/**
	 * ```
	 * \nn\zv::Api( $apiKey )->methodName();
	 * ```
	 * @return Api
	 */
	public static function Api( $apiKey = '' ) 
	{
		return new Api( $apiKey );		
	}

	/**
	 * ```
	 * \nn\zv::Bewerber()->methodName();
	 * ```
	 * @return Bewerber
	 */
	public static function Bewerber() 
	{
		return \nn\t3::injectClass( Bewerber::class );		
	}

	/**
	 * ```
	 * \nn\zv::BewerberPortal()->methodName();
	 * ```
	 * @return BewerberPortal
	 */
	public static function BewerberPortal() 
	{
		return \nn\t3::injectClass( BewerberPortal::class );		
	}

	/**
	 * ```
	 * \nn\zv::Bewerbung()->methodName();
	 * ```
	 * @return Bewerbung
	 */
	public static function Bewerbung() 
	{
		return \nn\t3::injectClass( Bewerbung::class );
	}

	/**
	 * ```
	 * \nn\zv::File()->methodName();
	 * ```
	 * @return File
	 */
	public static function File() {
		return \nn\t3::injectClass( File::class );		
	}
	
	/**
	 * ```
	 * \nn\zv::Form()->methodName();
	 * ```
	 * @return Form
	 */
	public static function Form() {
		return \nn\t3::injectClass( Form::class );		
	}

	/**
	 * ```
	 * \nn\zv::General()->methodName();
	 * ```
	 * @return General
	 */
	public static function General() {
		return \nn\t3::injectClass( General::class );		
	}
	
	/**
	 * ```
	 * \nn\zv::Geo()->methodName();
	 * ```
	 * @return Geo
	 */
	public static function Geo() {
		return \nn\t3::injectClass( Geo::class );		
	}

	/**
	 * ```
	 * \nn\zv::Katalog()->methodName();
	 * ```
	 * @return Katalog
	 */
	public static function Katalog() {
		return \nn\t3::injectClass( Katalog::class );		
	}
	
	/**
	 * ```
	 * \nn\zv::MandantOeffentlicheDatei()->methodName();
	 * ```
	 * @return MandantOeffentlicheDatei
	 */
	public static function MandantOeffentlicheDatei() {
		return \nn\t3::injectClass( MandantOeffentlicheDatei::class );		
	}

	/**
	 * ```
	 * \nn\zv::Stelle()->methodName();
	 * ```
	 * @return Stelle
	 */
	public static function Stelle() {
		return \nn\t3::injectClass( Stelle::class );		
	}

	/**
	 * ```
	 * \nn\zv::Suggest()->methodName();
	 * ```
	 * @return Suggest
	 */
	public static function Suggest() {
		return \nn\t3::injectClass( Suggest::class );		
	}
	
	/**
	 * ```
	 * \nn\zv::Sync()->methodName();
	 * ```
	 * @return Sync
	 */
	public static function Sync() {
		return \nn\t3::injectClass( Sync::class );		
	}
	
	/**
	 * ```
	 * \nn\zv::TCA()->methodName();
	 * ```
	 * @return TCA
	 */
	public static function TCA() {
		return \nn\t3::injectClass( TCA::class );		
	}
}