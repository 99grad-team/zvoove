<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

use Nng\Zvoove\Domain\Repository\StelleRepository;

/**
 * Helper for making GET/POST request via CURL to the zoove API.
 * 
 */
class Stelle extends AbstractUtility {

	/**
	 * The table containing the local `Stelle`-entries
	 */
	const TABLENAME = 'tx_zvoove_domain_model_stelle';

	/**
	 * Api for sending requests WITHOUT authorization
     * @var Api
     */
    protected $api = null;

	/**
     * @var string
     */
    protected $apiVersion = '/api/v1/';

	/**
	 * Api for sending requests WITH authorization
	 * @var Api
     */
    protected $apiAuth = null;
	
	/**
	 * @var StelleRepository
     */
    protected $stelleRepository = null;
	
	/**
	 * Create instances of Api to send authorized / unauthorized requests
	 */
	public function __construct() 
	{
		parent::__construct();
		$apiKey = \nn\t3::Settings()->getExtConf('zvoove')['openStelleApiKey'] ?? '';
		$this->api 		= \nn\zv::Api();

		$this->apiAuth 	= \nn\zv::Api( $apiKey );
		$this->apiAuth->setApiVersionPath( $this->apiVersion );

		$this->stelleRepository = \nn\t3::injectClass( StelleRepository::class );
		\nn\t3::Db()->ignoreEnableFields( $this->stelleRepository );
	}

	/**
	 * Get list of all vacancies
	 * ```
	 * \nn\zv::Stelle()->getStellen();
	 * \nn\zv::Stelle()->getStellen( 'helfer' );
	 * \nn\zv::Stelle()->getStellen( $filter = [] );
	 * ```
	 * @param array|string $filter
	 * @return array
	 */
	public function getStellen( $filter = [] ) 
	{
		if (is_string($filter)) {
			$filter = [
				'searchterm' => $filter,
			];
		}

		$filter = array_merge([
			'searchterm' => '',
			'boerse' => '',
		], $filter );

		$result = $this->api->GET( 'Stelle/GetStellen', $filter );
		return $result['data']['Items'] ?? [];
	}

	/**
	 * Get list of vacancies by filter criteria
	 * 
	 * ```
	 * // get results from API
	 * \nn\zv::Stelle()->getStellenFiltered( $filter = [] );
	 * 
	 * // get results from local database instead of API
	 * \nn\zv::Stelle()->getStellenFiltered( $filter = [], true );
	 * ```
	 * @return array
	 */
	public function getStellenFiltered( $filter = [], $useLocalDatabase = false ) 
	{
		// zvoove uses pipe-symbol to delimit multi-select values
		$joinFields = ['vaUuids'=>'|', 'abtUuids'=>'|'];
		foreach ($joinFields as $k=>$delimiter) {
			if ($val = $filter[$k] ?? false) {
				if (is_array($val)) {
					$filter[$k] = join($delimiter, $val);
				}
			}
		}

		// create filter
		$filter = array_merge([
			'keywords' 				=> '',
			'pageNo' 				=> 1,
			'pageSize' 				=> 9999999,
			'lat' 					=> '',
			'lon' 					=> '',
			'radius' 				=> '',
			'abtUuids' 				=> '',
			'vaUuids' 				=> '',
			'boerse' 				=> '',
			'generateCoordinates' 	=> '',
			'additionalProperties' 	=> '',
		], $filter );

		if ($useLocalDatabase) {
			return $this->stelleRepository->findByCriteria( $filter );
		}
		
		$result = $this->api->GET( 'Stelle/GetStellenFiltered', $filter );
		return [
			'success'	=> $result['success'],
			'message'	=> $result['message'],
			'items' 	=> $result['data']['Items'] ?? [],
			'total' 	=> $result['data']['TotalItems'] ?? 0,
		];
	}

	/**
	 * Get details to a single vacancies by uuid
	 * ```
	 * \nn\zv::Stelle()->getStelleById( '8116e4f5' );
	 * ```
	 * @param string $uuid
	 * @return array
	 */
	public function getStelleById( $uuid = '' ) 
	{
		$result = $this->api->GET( 'Stelle/GetStelleById', ['stelleUuid'=>$uuid] );
		return $result['data'] ?? [];
	}

	/**
	 * Get details to multiple vacancies by their uuids
	 * ```
	 * \nn\zv::Stelle()->getStellenByIds( ['8116e4f5', '1221e4f5', ...] );
	 * ```
	 * @param array $uuids	list of uuids
	 * @return array
	 */
	public function getStellenByIds( $uuids = [] ) 
	{
		$result = $this->api->postJSON( 'Stelle/GetStellenByIds', $uuids );

		$data = $result['data'] ?? [];

		foreach ($data as &$row) {
			$row['Abteilung'] = $this->convertStringToObjects( $row['Abteilung'], 'department' );
		}
		
		return $data;
	}

	/**
	 * The ugliest method in the complete extension.
	 * 
	 * There are certain fields in the `Stelle` returned from zvoove that are not an Storage of
	 * Objects, but a comma-seperated string of the object labels. Example: `Abteilung`. 
	 * There are no public endpoints available to get the list of objects here.
	 * 
	 * Currently, the only option left is to compare the string values and map them to Objects.
	 * 
	 * ```
	 * \nn\zv::Stelle()->convertStringToObjects('Helfer, Intern', 'department');
	 * ```
	 * 
	 * Example return array:
	 * ```
	 * [
	 * 	['ObjectUuid' => 'f8b1a493-4d2f-4d3b-a300-1a00549cd3f5', 'Bezeichnung' => 'Helfer'],
	 * 	['ObjectUuid' => 'adb1a493-131a-4d3b-a320-14002a9cd3f5', 'Bezeichnung' => 'Intern'],
	 * ]
	 * ```
	 * 
	 * @param string|array $str
	 * @param string $entity
	 * @param string $delimiter
	 * @return array
	 */
	public function convertStringToObjects( $str = '', $entity = 'department', $delimiter = ',' ) {
		if (is_array($str)) return $str;

		$objects = \nn\zv::Sync()->getLocalObjects( $entity, 'title', 'obj_uuid' );
		if (!$objects) return [];
		$list = explode(',', strtr( $str, $objects ));
		$objectsByUuid = array_flip($objects);

		$results = [];
		foreach ($list as $k=>&$v) {
			$v = trim($v);
			if (!isset($objectsByUuid[$v])) continue;
			$results[] = [
				'ObjectUuid' => $v,
				'Bezeichnung' => $objectsByUuid[$v],
			];
		}

		return $results;
	}

	/**
	 * Get all available filters for the job search.
	 * 
	 * These values are used to render the multiselect-dropdowns in the search form. 
	 * Currently, zvoove only supports filtering by:
	 * 
	 * - departments (`Abteilung`)
	 * - type of contract (`Vertragsarten`)
	 * 
	 * ```
	 * // get all results (cache will be used, if exists)
	 * \nn\zv::Stelle()->getAllFilters();
	 * 
	 * // disable cache
	 * \nn\zv::Stelle()->getAllFilters( false );
	 * ```
	 * @return array
	 */
	public function getAllFilters( $useCache = true ) 
	{
		$filters = $this->settings['search']['filters'] ?? [];
		if (!$filters) return [];

		if ($useCache && ($cache = $this->getCache('zv.filters'))) {
			return $cache;
		}

		$results = [];
		foreach ($filters as $key=>$config) {
			$entity = $config['entity'];
			if ($options = \nn\zv::Katalog()->getOptions( $entity, false, $useCache )) {
				$results[$key] = array_merge($config, [
					'options'		=> $options,
				]);
			}
		}

		return $this->setCache('zv.filters', $results);
	}

	/**
	 * Same as `getAllFilters()` - but results are retrieved from the local database.
	 * 
	 * ```
	 * \nn\zv::Stelle()->getAllFiltersFromLocalDB();
	 * ```
	 * @return array
	 */
	public function getAllFiltersFromLocalDB() 
	{
		$filters = $this->settings['search']['filters'] ?? [];
		if (!$filters) return [];

		if ($cache = $this->getCache('zv.filtersLocal')) {
			return $cache;
		}

		$results = [];
		foreach ($filters as $key=>$config) {
			if ($options = \nn\zv::Sync()->getLocalObjects($key, 'obj_uuid', 'title')) {
				$results[$key] = array_merge($config, [
					'options' => $options,
				]);
			}
		}

		return $this->setCache('zv.filtersLocal', $results);
	}

}