<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

use TYPO3\CMS\Form\Mvc\Persistence\FormPersistenceManager;
use TYPO3\CMS\Form\Service\DatabaseService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Helpers for dealing with the TYPO3 Form Framework
 * 
 */
class Form extends AbstractUtility 
{
	/**
	 * Return all forms available
	 * ```
	 * \nn\zv::Form()->getAvailableFormDefinitions();
	 * ```
	 * @return array
	 */
	public function getAvailableFormDefinitions() 
	{
		$databaseService = \nn\t3::injectClass( DatabaseService::class );
		$allReferencesForFileUid = $databaseService->getAllReferencesForFileUid();
        $allReferencesForPersistenceIdentifier = $databaseService->getAllReferencesForPersistenceIdentifier();

		$formPersistenceManager = \nn\t3::injectClass(FormPersistenceManager::class);

        $availableFormDefinitions = [];
        foreach ($formPersistenceManager->listForms() as $formDefinition) {
            $referenceCount  = 0;
            if (
                isset($formDefinition['fileUid'])
                && array_key_exists($formDefinition['fileUid'], $allReferencesForFileUid)
            ) {
                $referenceCount = $allReferencesForFileUid[$formDefinition['fileUid']];
            } elseif (array_key_exists($formDefinition['persistenceIdentifier'], $allReferencesForPersistenceIdentifier)) {
                $referenceCount = $allReferencesForPersistenceIdentifier[$formDefinition['persistenceIdentifier']];
            }

            $formDefinition['referenceCount'] = $referenceCount;
            $availableFormDefinitions[] = $formDefinition;
        }

        return $availableFormDefinitions;
	}

	/**
	 * Return configuration for a single form
	 * ```
	 * \nn\zv::Form()->getFormDefinition( $persistenceIdentifier );
	 * \nn\zv::Form()->getFormDefinition( 'EXT:path/to/the/form.yaml' );
	 * ```
	 * @param string $persistenceIdentifier
	 * @return array
	 */
	public function getFormDefinition( $persistenceIdentifier = '' ) 
	{
		$formPersistenceManager = \nn\t3::injectClass(FormPersistenceManager::class);
		$formDefinition = $formPersistenceManager->load( $persistenceIdentifier );
		return $formDefinition;
	}

	/**
	 * Create an array of availabale forms that can be selected in the `form.xml`
	 * Flexform.
	 * ```
	 * <config>
	 * 	<itemsProcFunc>nn\zv\Flexform->insertAvailableForms</itemsProcFunc>
	 * 	...
	 * </config>
	 * ```
	 * @return array
	 */
	public function insertAvailableForms( $config, $a = null ) 
	{
		if (!$config['items']) {
			$config['items'] = [];
		}
		if ($forms = $this->getAvailableFormDefinitions()) {
			foreach ($forms as $form) {
				$config['items'][] = [$form['identifier'], $form['persistenceIdentifier']];
			}
		}				
		return $config;
	}

	/**
	 * Set a default value for a certain form-type in the form configuration
	 * Recursively iterates through the renderables, sets value in referenced $formDefinition.
	 * 
	 * @return void
	 */
	public function setDefaultValueForType( &$formDefinition = [], $type = '', $value = '' ) 
	{
		if ($formDefinition['renderables'] ?? false) {
			foreach ($formDefinition['renderables'] ?? [] as $n=>$renderable) {
				if ($renderable['renderables'] ?? false) {
					$this->setDefaultValueForType( $formDefinition['renderables'][$n], $type, $value );
				}
				if ($renderable['type'] != $type) continue;
				$formDefinition['renderables'][$n]['defaultValue'] = $value;
			}
		}
	}
	
	/**
	 * Add an additional hidden field to the form
	 * 
	 * ```
	 * $formDefinition = \nn\zv::Form()->getFormDefinition('EXT:path/to/my/form.yaml');
	 * \nn\zv::Form()->addHiddenField( $formDefinition, 'stelleUid', $uid);
	 * 
	 * ```
	 * @return void
	 */
	public function addHiddenField( &$formDefinition = [], $identifier = '', $value = '' ) 
	{
		if (!isset($formDefinition['renderables'][0]['renderables'])) {
			$formDefinition['renderables'][0]['renderables'] = [];
		}
		$formDefinition['renderables'][0]['renderables'][$identifier] = [
			'identifier' 	=> $identifier,
			'type' 			=> 'Hidden',
			'label' 		=> 'Added dynamically',
			'defaultValue' 	=> $value,
			'properties'	=> [
				'elementClassAttribute' => GeneralUtility::camelCaseToLowerCaseUnderscored($identifier)
			],
		];
	}

	/**
	 * Set controller Action for form
	 * 
	 * @return void
	 */
	public function setControllerAction( &$formDefinition = [], $action = '' ) 
	{
		$formDefinition['renderingOptions']['controllerAction'] = $action;
	}

}