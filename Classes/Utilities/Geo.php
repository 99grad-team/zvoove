<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

/**
 * Methods for geo en/decoding
 * 
 */
class Geo extends AbstractUtility 
{

	/**
	 * Table that caches the result
	 */
	const TABLENAME = 'tx_zvoove_domain_model_geo';

	/**
	 * Convert a location string to geocoordinates.
	 * 
	 * ```
	 * \nn\zv::Geo()->getCoordinates('Blumenstrasse 2, 65189 Wiesbaden, Deutschland');
	 * ```
	 * @param string $address
	 * @return array
	 */
	public function getCoordinates( $address = '' ) 
	{
		$processor = $this->settings['search']['location']['geoCoding'] ?? false;

		try {
			$result = \nn\t3::call( $processor, $address );
		} catch ( \Exception $e ) {
			\nn\t3::Log()->error( $e->getMessage() );
			$result = [];			
		}

		return $result;
	}
	

	/**
	 * Get city-name and country for given location
	 * ```
	 * \nn\zv::Geo()->getLocation( 5.1203, 43.10102);
	 * ```
	 * @param float|string $lat;
	 * @param float|string $lng;
	 * @return array
	 */
	public function getLocation( $lng = null, $lat = null ) 
	{
		$processor = $this->settings['search']['location']['reverseGeoCoding'] ?? false;
		
		try {
			$result = \nn\t3::call( $processor, $lng, $lat );
		} catch ( \Exception $e ) {
			\nn\t3::Log()->error( $e->getMessage() );
			$result = [];			
		}

		return $result;
	}

	/**
	 * Get geocoordinates for a given city and country
	 * 
	 * Will try to retrieve data from local database.
	 * 
	 * If no matches were found, will automatically use MapApi and store result as new
	 * entry in database to reduce subsequent requests.
	 * 
	 * ```
	 * \nn\zv::Geo()->getCity('Wiesbaden');
	 * \nn\zv::Geo()->getCity('St. Louis', 'USA');
	 * ```
	 * @param string $city;
	 * @param string $country;
	 * @return array
	 */
	public function getCity( $city = '', $country = 'DEU') 
	{
		$city = trim($city);
		$localResult = \nn\t3::Db()->findOneByValues( self::TABLENAME, ['title'=>$city]);

		// no result? then get it from Geo API
		if (!$localResult) {

			$apiResult = $this->getCoordinates( "{$city}, $country" );
			
			$city = $apiResult['city'] ?? false;
			if (!$city) return [];
			
			// the city-name might have been corrected / changed by the API? Check local DB again...
			$localResult = \nn\t3::Db()->findOneByValues( self::TABLENAME, ['title'=>$city]);
		}
		
		// still no result? then create one locally
		if (!$localResult) {

			// map data from API to local DB fields
			$data = [
				'title' 	=> $city,
				'country' 	=> $apiResult['country_short'],
				'district' 	=> $apiResult['administrative_area_level_1'],
				'lng' 		=> $apiResult['lng'],
				'lat' 		=> $apiResult['lat'],
			];

			// persist the data
			$localResult = \nn\t3::Db()->insert( self::TABLENAME, $data );
		}

		return [
			'uid' 		=> $localResult['uid'],
			'title' 	=> $localResult['title'],
			'country' 	=> $localResult['country'],
			'district' 	=> $localResult['district'],
			'lng' 		=> $localResult['lng'],
			'lat' 		=> $localResult['lat'],
		];

	}
}