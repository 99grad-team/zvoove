<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

/**
 * Methods for lookahed / suggestion
 * 
 */
class Suggest extends AbstractUtility 
{
	/**
	 * Return a list of suggested locations / cityNames from search string.
	 * ```
	 * \nn\zv::Suggest()->locations('Wiesb');
	 * \nn\zv::Suggest()->locations('Wiesb', 100);
	 * ```
	 * @param string $str
	 * @return array
	 */
	public function locations( $str = '', $limit = 20 ) 
	{
		$processor = $this->settings['search']['location']['suggest'] ?? false;
		return $this->callMethod( $processor, $str, $limit );		
	}

	/**
	 * Return a list of suggested jobs from search string.
	 * ```
	 * \nn\zv::Suggest()->jobs('Helfer');
	 * \nn\zv::Suggest()->jobs('Helfer', 100);
	 * ```
	 * @param string $str
	 * @return array
	 */
	public function jobs( $str = '', $limit = 20 ) 
	{
		$processor = $this->settings['search']['jobs']['suggest'] ?? false;
		return $this->callMethod( $processor, $str, $limit );		
	}
}