<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

use Nng\Zvoove\Domain\Model\Stelle;
use Nng\Zvoove\Domain\Repository\StelleRepository;

/**
 * Abstract methods for all Utilities
 * 
 */
abstract class AbstractUtility extends \Nng\Nnhelpers\Singleton {

	/**
	 * Settings from TypoScript setup
	 * @var array
     */
    protected $settings = [];
	
	/**
	 * Local cache
	 * @var array
     */
    protected $cache = [];

	/**
	 * Constructor
	 * @return void
	 */
	public function __construct() 
	{
		$this->settings = \nn\t3::Settings()->getSettings('tx_zvoove');
	}
	
	/**
	 * Centralized call of method
	 * 
	 * ```
	 * $this->callMethod( 'Nng\Some\Class->method', $param );
	 * $this->callMethod( 'Nng\Some\Class::method', $param );
	 * ```
	 * 
	 * @param string $processor
	 * @param mixed $param
	 * @return mixed
	 */
	public function callMethod( $processor = '', $param = '', $param2 = '' ) 
	{
		if (!$processor) {
			return [];
		}
		try {
			$result = \nn\t3::call( $processor, $param, $param2 );
		} catch ( \Exception $e ) {
			\nn\t3::Log()->error( $e->getMessage() );
			$result = [];			
		}
		return $result;
	}

	/**
	 * Get something from cache.
	 * 
	 * First tries to retrieve something from local cache, then loads
	 * it from the DB Cache.
	 * 
	 * @param string $identifer
	 * @return array
	 */
	public function getCache( $identifier = '', $useCacheFramework = true ) 
	{
		if ($cache = $this->cache[$identifier] ?? false) {
			return $cache;
		}
		if (!$useCacheFramework) {
			return null;
		}
		return $this->cache[$identifier] = \nn\t3::Cache()->get( $identifier );
	}
	
	/**
	 * Write something in the cache.
	 * 
	 * @param string $identifer
	 * @param mixed $data
	 * @return array
	 */
	public function setCache( $identifier = '', $data = '', $useCacheFramework = true ) 
	{
		if (!$useCacheFramework) {
			return $this->cache[$identifier] = $data;
		}
		return $this->cache[$identifier] = \nn\t3::Cache()->set( $identifier, $data );
	}

}