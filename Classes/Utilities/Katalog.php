<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

/**
 * zvoove uses the term `Katalag` and means "value map or option".
 * 
 * Example: To set the gender to `Mr` ("Herr"), you need to pass the identifier for `Mr`. 
 * This is the `ObjectUuid` of the option `Mr`. Consequently, to create a select-dropdown
 * which the user can select his/her/its gender, you will need to retrieve the `Catalogue`
 * ("Katalog") of all available options.
 * 
 * Here is the GET request needed to accomplish that:
 * `/api/public/v1/Katalog/GetByRelationName?entityName=Mitarbeiter&relationName=Anrede&nameProperty=Bezeichnung&searchText=&ignoreSelectable=false`
 * 
 */
class Katalog extends AbstractUtility
{
	/**
	 * Api for sending requests WITHOUT authorization
     * @var Api
     */
    protected $api = null;

	/**
	 * Api for sending requests WITH authorization
	 * @var Api
     */
    protected $apiAuth = null;

	/**
	 * Create instances of Api to send authorized / unauthorized requests
	 */
	public function __construct() 
	{
		parent::__construct();
		$apiKey = \nn\t3::Settings()->getExtConf('zvoove')['openStelleApiKey'] ?? '';
		$this->api 		= \nn\zv::Api();
		$this->apiAuth 	= \nn\zv::Api( $apiKey );
	}

	/**
	 * Get `Katalog` for given entity and field (relation name).
	 * Simplified wrapper for `getByRelationName()`.
	 * 
	 * Returns an array with `ObjectUuid` as key an `Bezeichnung` as label.
	 * 
	 * ```
	 * \nn\zv::Katalog()->getOptions( 'Mitarbeiter.Anrede' );
	 * \nn\zv::Katalog()->getOptions( 'Mitarbeiter', 'Anrede' );
	 * \nn\zv::Katalog()->getOptions( 'Mitarbeiter', 'Anrede', false );
	 * ```
	 * 
	 * @return array
	 */
	public function getOptions( $entityName = '',  $relationName = false, $useCache = true ) 
	{
		if ($relationName === false) {
			list($entityName, $relationName) = \nn\t3::Arrays( $entityName )->trimExplode('.');
		}

		$cacheIdentifier = "zv.{$entityName}.$relationName";

		if ($useCache && $cache = $this->getCache($cacheIdentifier)) {
			return $cache;
		}

		$result = $this->getByRelationName([
			'entityName'	=> $entityName,
			'relationName' 	=> $relationName,
		]);

		$options = array_combine( 
			array_column($result, 'ObjectUuid'),
			array_column($result, 'Bezeichnung')
		);

		if (!$useCache) {
			return $options;
		}

		return $this->setCache( $cacheIdentifier, $options );
	}

	/**
	 * Get `Katalog` for given entity and field (relation name).
	 * 
	 * ```
	 * \nn\zv::Katalog()->getByRelationName( $filter = [] );
	 * ```
	 * 
	 * @return array
	 */
	public function getByRelationName( $filter = [] ) 
	{
		$filter = array_merge([
			'entityName' 		=> '',
			'relationName' 		=> '',
			'nameProperty' 		=> 'Bezeichnung',
			'searchText' 		=> '',
			'ignoreSelectable' 	=> false,
		], $filter);
		$result = $this->api->GET( 'Katalog/GetByRelationName', $filter );
		return $result['data']['Items'] ?? [];
	}

}