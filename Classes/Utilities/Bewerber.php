<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

/**
 * 
 */
class Bewerber extends AbstractUtility {

	/**
	 * Api for sending requests WITHOUT authorization
     * @var Api
     */
    protected $api = null;

	/**
	 * Api for sending requests WITH authorization
	 * @var Api
     */
    protected $apiAuth = null;

	/**
	 * Create instances of Api to send authorized / unauthorized requests
	 */
	public function __construct() 
	{
		parent::__construct();
		$apiKey = \nn\t3::Settings()->getExtConf('zvoove')['openBewerberApiKey'] ?? '';
		$this->api 		= \nn\zv::Api();
		$this->apiAuth 	= \nn\zv::Api( $apiKey );
	}

	/**
	 * Get list of all applicants
	 * ```
	 * \nn\zv::Bewerber()->getAll( '...' );
	 * ```
	 */
	public function getAll() 
	{
		return $this->getBySearchTerm();
	}
	
	/**
	 * Get list of applicants by a given keyword
	 * ```
	 * \nn\zv::Bewerber()->getBySearchTerm( '...' );
	 * ```
	 */
	public function getBySearchTerm( $searchterm = '' ) 
	{
		$result = $this->apiAuth->GET( 'Bewerber/GetBySearchTerm', ['searchterm'=>$searchterm] );
		return $result['data']['Items'] ?? [];
	}

	/**
	 * Get details to a single applicants
	 * ```
	 * \nn\zv::Bewerber()->getById( $uid );
	 * ```
	 */
	public function getById( $uid = '' ) 
	{
		$result = $this->apiAuth->GET( 'Bewerber/GetById', ['bewerberUuid'=>$uid] );
		return $result['data'] ?? [];
	}

	/**
	 * Download a file.
	 * 
	 * ```
	 * \nn\zv::Bewerber()->downloadDatei( $fileObj );
	 * \nn\zv::Bewerber()->downloadDatei( ['ObjectUuid'=>'...', ...] );
	 * ```
	 * 
	 * Full example:
	 * ```
	 * $applicants = \nn\zv::Bewerber()->getBySearchTerm('juliloule');
     * $applicant = \nn\zv::Bewerber()->getById( $applicants[0]['ObjectUuid'] );
     * \nn\zv::Bewerber()->downloadDatei( $applicant['Dateien'][1] );
	 * ```
	 */
	public function downloadDatei( $fileObj = [] ) 
	{
		$fileData = $this->getDateiById( $fileObj['ObjectUuid'] );
		\nn\t3::File()->sendDownloadHeader( $fileObj['Dateiname'], strlen($fileData) );
		echo $fileData;
		die();
	}
	
	/**
	 * Download a profile.
	 * (!) NOT TESTED YET
	 * 
	 * ```
	 * \nn\zv::Bewerber()->downloadProfile( $profileObj );
	 * \nn\zv::Bewerber()->downloadProfile( ['ObjectUuid'=>'...', ...] );
	 * ```
	 * return void
	 */
	public function downloadProfile( $profileObj = [] ) 
	{
		$fileData = $this->getDateiById( $profileObj['ObjectUuid'] );
		\nn\t3::File()->sendDownloadHeader( $profileObj['Dateiname'], strlen($fileData) );
		echo $fileData;
		die();
	}

	/**
	 * Get raw file data from a applicants by its ObjectUuid
	 * ```
	 * \nn\zv::Bewerber()->getDateiById( $uid );
	 * ```
	 * @return string
	 */
	public function getDateiById( $uid = '' ) 
	{
		return $this->apiAuth->rawGET( 'Bewerber/getDateiById', ['dateiUuid'=>$uid] );
	}
	
	/**
	 * Get raw profile pdf from a applicants by its ProfileUuid
	 * (!) NOT TESTED YET
	 * ```
	 * \nn\zv::Bewerber()->getProfileAsPdf( $profileUuid );
	 * ```
	 * @return string
	 */
	public function getProfileAsPdf( $uid = '' ) 
	{
		return $this->apiAuth->rawGET( 'Bewerber/GetProfileAsPdf', ['profileUuid'=>$uid] );
	}
	
}