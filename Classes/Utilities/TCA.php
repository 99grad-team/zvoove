<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

/**
 * Get configurations for TCA
 * 
 */
class TCA extends AbstractUtility 
{
	/**
	 * Get `Katalog` for given entity and field (relation name).
	 * Simplified wrapper for `getByRelationName()`.
	 * 
	 * Returns an array with `ObjectUuid` as key an `Bezeichnung` as label.
	 * 
	 * ```
	 * [
	 * 	'config' => \nn\zv::TCA()->getOptions( 'Mitarbeiter.Anrede' )
	 * ]
	 * ```
	 * 
	 * @return array
	 */
	public function getOptions( $entityName = '',  $relationName = false, $useCache = true ) 
	{
		$options = \nn\zv::Katalog()->getOptions( $entityName, $relationName, $useCache );
		$tcaItems = [];
		foreach ($options as $k=>$v) {
			$tcaItems[] = [$v, $k];
		}
		return [
			'type' 			=> 'select',
			'renderType' 	=> 'selectSingle',
			'items' 		=> $tcaItems,
		];
	}
	
	/**
	 * Get `filters` defined in `plugin.tx_zvoove.settings.search.filters`.
	 * These can be selected for an `Obj` that will be filter-criteria of a `Stelle`
	 * 
	 * ```
	 * [
	 * 	'config' => \nn\zv::TCA()->getFilters()
	 * ]
	 * ```
	 * 
	 * @return array
	 */
	public function getFilters( $entityName = '',  $relationName = false, $useCache = true ) 
	{
		$tcaItems = [];
		foreach ($this->settings['search']['filters'] ?? [] as $k=>$v) {
			$tcaItems[] = [$v['label'], $k];
		}
		return [
			'type' 			=> 'select',
			'renderType' 	=> 'selectSingle',
			'items' 		=> $tcaItems,
		];
	}

	/**
	 * Insert filters for flexform
	 * 
	 * ```
	 * <config>
	 * 	<type>select</type>
	 * 	<renderType>selectCheckBox</renderType>
	 * 	<itemsProcFunc>nn\zv\Flexform->insertFilterOptions</itemsProcFunc>
	 * 	<entity>department</entity>
	 * </config>
	 * ```
	 * @return array
	 */
	public function insertFilterOptions( &$params = '' ) 
	{
		$entity = $params['config']['entity'] ?? '';
		$options = \nn\zv::Sync()->getLocalObjects($entity, 'obj_uuid', 'title');
		foreach ($options as $objUuid=>$title) {
			$params['items'][] = [$title, $objUuid];
		}
	}
}