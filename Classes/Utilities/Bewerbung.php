<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

/**
 * (!!) NOT IN USE
 * 
 * This endpoint is documented in the zvoove API, but the documentation is
 * unclear and parameters are documented wrong.
 * 
 * For sending applications (`Bewerbung`) from the website we are using an
 * undocumented Endpoint:
 * 
 * see `BewerberPortal`
 * 
 */
class Bewerbung extends AbstractUtility {

	/**
	 * Api for sending requests WITHOUT authorization
     * @var Api
     */
    protected $api = null;

	/**
	 * Api for sending requests WITH authorization
	 * @var Api
     */
    protected $apiAuth = null;

	/**
	 * Create instances of Api to send authorized / unauthorized requests
	 */
	public function __construct() 
	{
		parent::__construct();
		$apiKey = \nn\t3::Settings()->getExtConf('zvoove')['openStelleApiKey'] ?? '';
		$this->api 		= \nn\zv::Api();
		$this->apiAuth 	= \nn\zv::Api( $apiKey );
	}

	/**
	 * Create an application
	 * ```
	 * \nn\zv::Bewerbung()->create( \Nng\Zvoove\Domain\Model\Bewerbung );
	 * \nn\zv::Bewerbung()->create( [...] );
	 * ```
	 * @param \Nng\Zvoove\Domain\Model\Bewerbung $data
	 */
	public function create( $data = [] ) 
	{	
		\nn\t3::Exception('NOT IMPLEMENTED YET!');
		$formData = [
			'bewerbung' => $data->toArray(),
		];
		$result = $this->apiAuth->sendMultipartRequest( 'Bewerbung/Create', $formData );
		return $result;
		return $result['data']['Items'] ?? [];
	}

	/**
	 * Get application template (a dummy Model for the application)
	 * ```
	 * \nn\zv::Bewerbung()->getNeuanlageModel();
	 * ```
	 */
	public function getNeuanlageModel() 
	{
		$result = $this->apiAuth->GET( 'Bewerbung/GetNeuanlageModel' );
		return $result;
		return $result['data']['Items'] ?? [];
	}

}