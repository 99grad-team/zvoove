<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Helpers for dealing with files and folders
 * 
 */
class File extends AbstractUtility {

	/**
	 * Move uploaded files to destination folder.
	 * Creates the upload-folder, if it doesn't exist.
	 * 
	 * ```
	 * \nn\zv::File()->processFileUploads( [...] );
	 * ```
	 * @return array
	 */
	public function processFileUploads( $files = [] ) 
	{
		if (!$files) {
			return [];
		}

		$settings = $this->settings['fileUpload'] ?? [];
		$path = $settings['path'] ?? '';
		
		if (!$path) {
			\nn\t3::Exception('zvoove: upload-folder not defined in TypoScript Setup. Did you include the TypoScript template in the root-page?');
		}

		if (!\nn\t3::File()->mkdir( $path )) {
			\nn\t3::Exception('upload-folder ' . $path . ' could not be created.');
		}

		// `protectDirectory` must be expicitly set to `0` to NOT protect the directory
		$this->protectFolder( $path, $settings['protectDirectory'] ?? true );

		$path = rtrim( $path, '/' ) . '/';

		// move all uploaded files from `tmp` folder to destination
		$processedFiles = [];
		foreach ($files as &$file) {
			if ($tmpName = $file['tmp_name'] ?? false) {

				$filename = $file['name'];

				// double check: No hacker file-types, please!
				if (\nn\t3::File()->isForbidden($filename)) {
					continue;
				}

				$basename = pathinfo( $filename, PATHINFO_BASENAME );
				$suffix = pathinfo( $filename, PATHINFO_EXTENSION );

				if ($settings['appendHash'] ?? false) {
					$basename .= '-' . uniqid();
				}

				$dest = $path . $basename . '.' . $suffix;				
				$file['name'] = \nn\t3::File()->moveUploadedFile( $tmpName, $dest );

				$processedFiles[] = [
					'publicUrl' => $file['name']
				];
			}
		}
		
		return $processedFiles;
	}

	/**
	 * Protect/Unprotect a folder by creating an `.htaccess` file and adding
	 * or removing the line `Deny from all`.
	 * ```
	 * \nn\zv::File()->protectFolder( '1:/path/to/folder' );
	 * \nn\zv::File()->protectFolder( 'fileadmin/path/to/folder' );
	 * ```
	 * @return void
	 */
	public function protectFolder( $path = '', $protect = true ) 
	{
		$htaccessPath = rtrim($path, '/') . '/.htaccess';
		
		if (!\nn\t3::File()->exists($path)) return;

		$contents = '';
		if (\nn\t3::File()->exists($htaccessPath)) {
			$contents = \nn\t3::File()->read( $htaccessPath );
			if ($protect && strpos($contents, 'Deny from all') !== false) return;
			if (!$protect && strpos($contents, 'Deny from all') === false) return;
		}

		if (!$protect) {
			$contents = str_replace("Deny from all\n", '', $contents);
		} else {
			$contents = "Deny from all\n" . $contents;
		}
		
		\nn\t3::File()->write($htaccessPath, $contents);
	}

	/**
	 * Create a local `sys_file` for given media-uuid.
	 * Checks, if `sys_file` already exists for given media-uuid.
	 * 
	 * ```
	 * \nn\zv::File()->getSysFileForStreamUuid( 'some-media-uuid' );
	 * ```
	 * @return \TYPO3\CMS\Core\Resource\File
	 */
	public function getSysFileForStreamUuid( $uuid = '' ) 
	{
		if (!$uuid) {
			return false;
		}
		// sys_file already exists? Return local copy.
		if ($entry = \nn\t3::Db()->findOneByValues('sys_file', ['zvoove_id'=>$uuid])) {
			$resourceFactory = GeneralUtility::makeInstance(ResourceFactory::class);
			$file = $resourceFactory->getFileObject( $entry['uid'] );
			return $file;
		}
		
		// where to copy files from zvoove to local fileadmin
		$uploadPath = $this->settings['externalMedia']['path'] ?? false;
		if (!$uploadPath) {
			return false;
		}
		
		// get file from zvoove
		$fileData = \nn\zv::MandantOeffentlicheDatei()->getAsStreamContent( $uuid );
		if (!$fileData['data']) {
			return false;
		}
		
		// create unique, absolute filename for destination
		$filename = rtrim($uploadPath, '/') . '/' . $fileData['filename'];
		$filename = \nn\t3::File()->absPath( $filename );
		$filename = \nn\t3::File()->uniqueFilename( $filename );
		
		// write file to destination, create subfolders if neccessary
		\nn\t3::File()->write( $filename, $fileData['data'] );
		
		if ($sysFile = \nn\t3::Fal()->createSysFile( $filename, false )) {
			$uid = $sysFile->getUid();
			\nn\t3::Db()->update('sys_file', ['zvoove_id'=>$uuid], $uid);
			return $sysFile;
		}
		\nn\t3::Exception($filename);

		return false;
	}

	/**
	 * Return placeholder image for job
	 * ```
	 * \nn\zv::File()->getFallbackImage( $type );
	 * ```
	 * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	public function getFallbackImage ( $type = 'stelle' ) 
	{
		$identifier = 'zv.file.' . $type;
		if ($cache = $this->getCache( $identifier, false )) {
			return $cache;
		}
		if ($path = $this->settings['fallbackImages'][$type] ?? false) {
			$path = \nn\t3::File()->resolvePathPrefixes( $path );
			return $this->setCache( $identifier, $path );	
		}
	}

}