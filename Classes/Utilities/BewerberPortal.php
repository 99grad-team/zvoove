<?php 
declare(strict_types = 1);

namespace Nng\Zvoove\Utilities;

/**
 * The `BewerberPortal`-endpoint is actually the (undocumented) API which is
 * used for the zvoove form provided with the iframe-implementation.
 * 
 * Certain methods seem to only be accessible using this endpoint due to 
 * lacking documentation of the regular zvoove API.
 * 
 */
class BewerberPortal extends AbstractUtility
{
	/**
     * @var string
     */
    protected $apiVersion = '/api/v1/';

	/**
	 * Api for sending requests WITHOUT authorization
     * @var Api
     */
    protected $api = null;

	/**
	 * Api for sending requests WITH authorization
	 * @var Api
     */
    protected $apiAuth = null;

	/**
	 * Create instances of Api to send authorized / unauthorized requests
	 * @return void
	 */
	public function __construct() 
	{
		parent::__construct();
		
		$apiKey = \nn\t3::Settings()->getExtConf('zvoove')['openStelleApiKey'] ?? '';

		$this->api 		= \nn\zv::Api();
		$this->apiAuth 	= \nn\zv::Api( $apiKey );

		$this->api->setApiVersionPath( $this->apiVersion );
		$this->apiAuth->setApiVersionPath( $this->apiVersion );
	}

	/**
	 * Create an application.
	 * 
	 * It will appear in the InBox of the login-area, e.g.
	 * `https://purus.europersonal.com/recruiting/inbox/bw`
	 * 
	 * ```
	 * \nn\zv::BewerberPortal()->createBewerber( \Nng\Zvoove\Domain\Model\Bewerbung );
	 * \nn\zv::BewerberPortal()->createBewerber( [...] );
	 * ```
	 * @param \Nng\Zvoove\Domain\Model\Bewerbung $data
	 * @return array
	 */
	public function createBewerber( $data = [] ) 
	{
		$formData = [
			'mitarbeiterNeuanlageReadWriteDto' 	=> $data->toArray(),
			'file' => $data->getFiles(),
		];
		$result = $this->api->sendMultipartRequest( 'BewerberPortal/CreateBewerber', $formData );
		return $result;
	}

	/**
	 * Get list of vacancies (`Stellen`) and group them by 
	 * all categories / `Stellenbörsen`.
	 * 
	 * ```
	 * \nn\zv::BewerberPortal()->getAllStellenGroupedByStellenboersen();
	 * ```
	 * @return array
	 */
	public function getAllStellenGroupedByStellenboersen() {

		// get the categories - these contain the slugs
		$categories = $this->getAllStellenboersen();

		// merge the vacancies
		foreach ($categories as &$category) {
			$category['Stellen'] = $this->getStellenAnonymous( $category['UrlSlug'] );
		}
		
		return $categories;
	}

	/**
	 * Get list of all categories / `Stellenbörsen` that exist in the system.
	 * Will contain the title and the slug needed to retrieve a list of filtered vacancies
	 * with `getStellenAnonymous()`
	 * 
	 * ```
	 * \nn\zv::BewerberPortal()->getAllStellenboersen();
	 * ```
	 * @param array $filter
	 * @return array
	 */
	public function getAllStellenboersen() 
	{
		$result = $this->api->GET( 'stellenboerse/GetAll' );
		return $result['data'] ?? [];
	}

	/**
	 * Get all vacancies (`Stellen`) that are visible to the public.
	 * 
	 * ```
	 * \nn\zv::BewerberPortal()->getStellenAnonymous( 'purus-ag', $filter );
	 * ```
	 * 
	 * @param array $filter
	 * @return array
	 */
	public function getStellenAnonymous( $slug = '', $filter = [] ) 
	{
		if (!$slug) {
			return [];
		}

		$filter = array_merge($filter, [
			'abteilung' 			=> '',
			'keywords' 				=> '',
			'pageNo' 				=> 1,
			'pageSize' 				=> 999999,
			'lat' 					=> '',
			'lon' 					=> '',
			'radius' 				=> 25,
			'abtUuids' 				=> '',
			'vaUuids' 				=> '',
			'generateCoordinates'	=> false
		]);

		$headers = [
			'x-stellenboerseslug' => $slug,
		];

		$result = $this->api->GET( 'BewerberPortal/GetStellenAnonymous', $filter, $headers );
		return $result['data']['Items'] ?? [];
	}

}