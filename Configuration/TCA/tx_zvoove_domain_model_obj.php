<?php

	defined('TYPO3_MODE') || die('Access denied.');

	$LL = 'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:';

	$fields = [
		'title' => [
			'exclude' => 1,
			'label'	=> $LL . 'title',
			'config'  => [
				'type'	  => 'input',
				'default'  => '',
			]
		],
		'obj_uuid' => [
			'exclude' => 1,
			'label'	=> $LL . 'obj_uuid',
			'config'  => [
				'type'	  => 'input',
				'default'  => '',
			]
		],
		'entity' => [
			'exclude' => 1,
			'label'	=> $LL . 'entity',
			'config' => \nn\zv::TCA()->getFilters(),
		],
	];

	return [
		'ctrl' => [
			'title'	=> $LL . 'tx_zvoove_domain_model_obj',
			'label' => 'title',
			'sortby' => 'sorting',
			'searchFields' => '',
			'tstamp' => 'tstamp',
			'crdate' => 'crdate',
			'cruser_id' => 'cruser_id',
			'dividers2tabs' => TRUE,    
			'iconfile' => 'EXT:zvoove/Resources/Public/Icons/Extension.svg',
			'languageField' => 'sys_language_uid',
			'transOrigPointerField' => 'l10n_parent',
			'transOrigDiffSourceField' => 'l10n_diffsource',
			'delete' => 'deleted',
			'enablecolumns' => [
				'disabled' => 'hidden',
				'starttime' => 'starttime',
				'endtime' => 'endtime',
				'fe_group' => 'fe_group',
			],   
		],

		'interface' => [],
		
		'types' => [
			'0' => ['showitem' => '
				--div--;Basics,
					--palette--;;1,
					sys_language_uid,l10n_parent,l10n_diffsource,
					' . join(',', array_keys($fields)) . ',
				--div--;Access,
					--palette--;;5,
				'],
		],

		'palettes' => [
			'5' => ['showitem' => 'hidden, starttime, endtime,--linebreak--, fe_group'],
		],

		'columns' => \nn\t3::TCA()->createConfig(
			'tx_zvoove_domain_model_obj',
			true,
			$fields,
		),

	];