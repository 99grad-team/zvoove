<?php
defined('TYPO3_MODE') or die();

$newSysFileColumns = [
    'zvoove_id' => [
        'exclude' => 1,
        'label' => 'media uuid in zvoove',
        'config' => [
			'type' => 'input',
		],
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file', $newSysFileColumns);
