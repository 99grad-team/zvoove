<?php

\nn\t3::Registry()->pluginGroup(
    'Nng\Zvoove',
    'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_group_name',
    [
        'list' => [
            'title'     => 'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_list.name', 
            'icon'      => 'EXT:zvoove/Resources/Public/Icons/Extension.svg',
            'flexform'  => 'FILE:EXT:zvoove/Configuration/FlexForm/list.xml',
        ],
        'show' => [
            'title'     => 'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_show.name', 
            'icon'      => 'EXT:zvoove/Resources/Public/Icons/Extension.svg',
            'flexform'  => 'FILE:EXT:zvoove/Configuration/FlexForm/show.xml'
        ],
        'search' => [
            'title'     => 'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_search.name', 
            'icon'      => 'EXT:zvoove/Resources/Public/Icons/Extension.svg',
            'flexform'  => 'FILE:EXT:zvoove/Configuration/FlexForm/search.xml'
        ],
        'form' => [
            'title'     => 'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_form.name', 
            'icon'      => 'EXT:zvoove/Resources/Public/Icons/Extension.svg',
            'flexform'  => 'FILE:EXT:zvoove/Configuration/FlexForm/form.xml'
        ],
        'cart' => [
            'title'     => 'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_cart.name', 
            'icon'      => 'EXT:zvoove/Resources/Public/Icons/Extension.svg',
            'flexform'  => 'FILE:EXT:zvoove/Configuration/FlexForm/cart.xml'
        ],
    ]
);