<?php

	defined('TYPO3_MODE') || die('Access denied.');
	
	$LL = 'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:';

	$fields = [
		'bewerber' => [
			'exclude' => 1,
			'label'	=> $LL . 'bewerber',
			'config' => [
				'type' => 'inline',
				'foreign_table' => 'tx_zvoove_domain_model_bewerber',
				'maxitems' => 1,
				'appearance' => [
					'collapseAll' => 1,
					'expandSingle' => 1,
				],
			],
		],
		'jobid' => [
			'exclude' => 1,
			'label'	=> $LL . 'jobid',
			'config'  => [
				'type'	  => 'input',
				'default'  => '',
			]
		],
		'applicationid' => [
			'exclude' => 1,
			'label'	=> $LL . 'applicationid',
			'config'  => [
				'type'	  => 'input',
				'default'  => '',
			]
		],
		'files' => [
			'exclude' => 1,
			'label'	=> $LL . 'files',
			'config'  => \nn\t3::TCA()->getFileFieldTCAConfig('files'),
		],
	];

	return [
		'ctrl' => [
			'title'	=> $LL . 'tx_zvoove_domain_model_bewerbung',
			'label' => 'applicationid',
			'searchFields' => '',
			'tstamp' => 'tstamp',
			'crdate' => 'crdate',
			'cruser_id' => 'cruser_id',
			'dividers2tabs' => TRUE,    
			'iconfile' => 'EXT:zvoove/Resources/Public/Icons/Extension.svg',
			'languageField' => 'sys_language_uid',
			'transOrigPointerField' => 'l10n_parent',
			'transOrigDiffSourceField' => 'l10n_diffsource',
			'delete' => 'deleted',
			'enablecolumns' => [
				'disabled' => 'hidden',
				'starttime' => 'starttime',
				'endtime' => 'endtime',
				'fe_group' => 'fe_group',
			],   
		],

		'interface' => [],
		
		'types' => [
			'0' => ['showitem' => '
				--div--;Basics,
					--palette--;;1,
					sys_language_uid,l10n_parent,l10n_diffsource,
					' . join(',', array_keys($fields)) . ',
				--div--;Access,
					--palette--;;5,
				'],
		],

		'palettes' => [
			'5' => ['showitem' => 'hidden, starttime, endtime,--linebreak--, fe_group'],
		],

		'columns' => \nn\t3::TCA()->createConfig(
			'tx_zvoove_domain_model_bewerbung',
			true,
			$fields,
		),

	];