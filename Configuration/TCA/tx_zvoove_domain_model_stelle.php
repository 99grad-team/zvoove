<?php

	defined('TYPO3_MODE') || die('Access denied.');

	$LL = 'LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:';

	$fields = [
		'title' => [
			'exclude' => 1,
			'label'	=> $LL . 'title',
			'config'  => [
				'type'	  => 'input',
				'default'  => '',
			]
		],
		'jobid' => [
			'exclude' => 1,
			'label'	=> $LL . 'jobid',
			'config'  => [
				'type'	  => 'input',
				'default'  => '',
			]
		],
		'location' => [
			'exclude' => 1,
			'label'	=> $LL . 'location',
			'config'  => [
				'type'	  => 'input',
				'default'  => '',
			]
		],
		'zip' => [
			'exclude' => 1,
			'label'	=> $LL . 'zip',
			'config'  => [
				'type'	  => 'input',
				'default'  => '',
			]
		],
		'cat_va' => [
			'label' => $LL . 'contract',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectCheckBox',
				'internal_type' => 'db',
				'allowed' => 'tx_zvoove_domain_model_obj',
				'foreign_table' => 'tx_zvoove_domain_model_obj',
				'foreign_table_where' => ' AND tx_zvoove_domain_model_obj.entity = \'contract\'',
				'size' => 5,
				'minitems' => 0,
				'maxitems' => 100,

				'MM' => 'tx_zvoove_domain_model_stelle_obj_mm',
				'MM_match_fields' => [
					'fieldname' => 'va',
				],
			]
		],
		'cat_abt' => [
			'label' => $LL . 'contract',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectCheckBox',
				'internal_type' => 'db',
				'allowed' => 'tx_zvoove_domain_model_obj',
				'foreign_table' => 'tx_zvoove_domain_model_obj',
				'foreign_table_where' => ' AND tx_zvoove_domain_model_obj.entity = \'department\'',
				'size' => 5,
				'minitems' => 0,
				'maxitems' => 100,
				'MM' => 'tx_zvoove_domain_model_stelle_obj_mm',
				'MM_match_fields' => [
					'fieldname' => 'abt',
				],
			]
		],
		'contract' => [
			'exclude' => 1,
			'label'	=> $LL . 'contract',
			'config'  => [
				'type'	  => 'input',
				'default'  => '',
			]
		],
		'images' => [
			'exclude' => 1,
			'label'	=> $LL . 'images',
			'config'  => \nn\t3::TCA()->getFileFieldTCAConfig('images'),
		],
		'images_contact' => [
			'exclude' => 1,
			'label'	=> $LL . 'images_contact',
			'config'  => \nn\t3::TCA()->getFileFieldTCAConfig('images_contact'),
		],
		'date_from' => [
			'exclude' => 1,
			'label'	=> $LL . 'date_from',
			'config'  => [
				'type'	  => 'input',
				'renderType' => 'inputDateTime',
				'eval' => 'datetime',
			]
		],
		'date_to' => [
			'exclude' => 1,
			'label'	=> $LL . 'date_to',
			'config'  => [
				'type'	  => 'input',
				'renderType' => 'inputDateTime',
				'eval' => 'datetime',
			]
		],
		'api_tstamp' => [
			'exclude' => 1,
			'label'	=> $LL . 'api_tstamp',
			'config'  => [
				'type'	  => 'input',
				'renderType' => 'inputDateTime',
				'eval' => 'datetime',
			]
		],
		'data' => [
			'exclude' => 1,
			'label'	=> $LL . 'data',
			'config'  => [
				'type'	  => 'text',
			]
		],
		'path_segment' => [
            'label' => $LL . 'path_segment',
            'displayCond' => 'VERSION:IS:false',
            'config' => \nn\t3::TCA()->getSlugTCAConfig( 'title' ),
        ],
		'lng' => [
			'exclude' => 1,
			'label'	=> $LL . 'lng',
			'config'  => [
				'type'	  => 'input',
				'default'  => '',
			]
		],
		'lat' => [
			'exclude' => 1,
			'label'	=> $LL . 'lat',
			'config'  => [
				'type'	  => 'input',
				'default'  => '',
			]
		],
		'distance' => [
			'config'  => [
				'type' => 'passthrough'
			]
		],
		'country' => [
			'exclude' => 1,
			'label'	=> $LL . 'country',
			'config'  => [
				'type'	  => 'input',
				'default'  => '',
			]
		],
		'district' => [
			'exclude' => 1,
			'label'	=> $LL . 'district',
			'config'  => [
				'type'	  => 'input',
				'default'  => '',
			]
		],
	];

	return [
		'ctrl' => [
			'title'	=> $LL . 'tx_zvoove_domain_model_stelle',
			'label' => 'title',
			'label_alt' => 'location, zip',
			'label_alt_force' => TRUE,
			'default_sortby' => 'title',
			'searchFields' => '',
			'tstamp' => 'tstamp',
			'crdate' => 'crdate',
			'cruser_id' => 'cruser_id',
			'dividers2tabs' => TRUE,    
			'iconfile' => 'EXT:zvoove/Resources/Public/Icons/Extension.svg',
			'languageField' => 'sys_language_uid',
			'transOrigPointerField' => 'l10n_parent',
			'transOrigDiffSourceField' => 'l10n_diffsource',
			'delete' => 'deleted',
			'enablecolumns' => [
				'disabled' => 'hidden',
				'starttime' => 'starttime',
				'endtime' => 'endtime',
				'fe_group' => 'fe_group',
			],   
		],

		'interface' => [],
		
		'types' => [
			'0' => ['showitem' => '
				--div--;Basics,
					--palette--;;1,
					sys_language_uid,l10n_parent,l10n_diffsource,
					' . join(',', array_keys($fields)) . ',
				--div--;Access,
					--palette--;;5,
				'],
		],

		'palettes' => [
			'5' => ['showitem' => 'hidden, starttime, endtime,--linebreak--, fe_group'],
		],

		'columns' => \nn\t3::TCA()->createConfig(
			'tx_zvoove_domain_model_stelle',
			true,
			$fields,
		),

	];