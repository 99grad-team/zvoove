

tx_zvoove {

	# sorting option for listview, selectable in flexform
	sorting {
		title_asc {
			label = Job-Title, ascending (A–Z)
			value = title, ASC
		}
		title_desc {
			label = Job-Title, descending (Z–A)
			value = title, DESC
		}
		api_desc {
			label = Last modification date, newest first
			value = api_tstamp, DESC
		}
		location_asc {
			label = Location, ascending [A–Z]
			value = location, ASC
		}
	}

	# Define template layouts which will be selectable in flexform
	templateLayouts {
		list {
			default { 
				label = Default
				template = Default
			}
		}
		show {
			default { 
				label = Default
				template = Default
			}
		}
		search {
			default { 
				label = Default
				template = Default
			}
		}
		form {
			default { 
				label = Default
				template = Default
			}
		}
		cart {
			default { 
				label = Default
				template = Default
			}
		}
	}
}


# Add plugin to content element wizard
mod {
	wizards.newContentElement.wizardItems.plugins {
		elements {
			zvoove_list {
				iconIdentifier = zvoove-plugin
				title = LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_list.name
				description = LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_list.description
				tt_content_defValues {
					CType = list
					list_type = zvoove_list
				}
			}
			zvoove_show {
				iconIdentifier = zvoove-plugin
				title = LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_show.name
				description = LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_show.description
				tt_content_defValues {
					CType = list
					list_type = zvoove_show
				}
			}
			zvoove_search {
				iconIdentifier = zvoove-plugin
				title = LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_search.name
				description = LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_search.description
				tt_content_defValues {
					CType = list
					list_type = zvoove_search
				}
			}
			zvoove_form {
				iconIdentifier = zvoove-plugin
				title = LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_form.name
				description = LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_form.description
				tt_content_defValues {
					CType = list
					list_type = zvoove_form
				}
			}
			zvoove_cart {
				iconIdentifier = zvoove-plugin
				title = LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_cart.name
				description = LLL:EXT:zvoove/Resources/Private/Language/locallang_db.xlf:pi_cart.description
				tt_content_defValues {
					CType = list
					list_type = zvoove_cart
				}
			}
		}
		show = *
	}
}