

define(['jquery'], function($) {

	let $container = $('.zvoove-be-module');
	let $progressBase = $('.progress');

	/**
	 * Click on "sync all entries"
	 * 
	 */
	$('.sync-all').click(function () {

		const href = $(this).attr('href');
		const init = $(this).data().init;

		$container.addClass('loading');
		setProgress(0);

		// reset sync to status = IDLE
		$.getJSON( init, function ( progress ) {
			console.log( progress );
			runSync();
		});

		// exec next sync interval
		function runSync() {
			$.getJSON( href, function ( progress ) {

				const status = progress.status;
				console.log( progress );

				if (progress.error) {
					alert('There was a problem synchronising the entries with the zvoove API. Check the TYPO3 log for details.');
				}

				if (status == 0 || status == 100) {
					// done processing all jobs
					updateList();
				} else {
					// process next batch
					if (progress.items) {
						setProgress(1/progress.total * progress.current);
					}
					runSync();
				}
			});
		}

		return false;
	});

	/**
	 * Show progress bar
	 * 
	 */
	function setProgress( p ) {
		$progressBase.css({
			width: (100 * p) + '%'
		});
	}

	/**
	 * Search for keywords
	 * 
	 */
	$('.search').keyup(function () {
		var sword = $(this).val().toLowerCase();

		$('.search-icon').toggle( sword.length == 0 );
		$('.clear-icon').toggle( sword.length > 0 );

		$('.job-item').each(function () {
			var $el = $(this);
			$el.toggle( $el.text().toLowerCase().indexOf(sword) > -1 );
		});
	});

	$('.clear-icon').click(() => {
		$('.search').val('').keyup();
	});

	/**
	 * Reload complete list of entries
	 * 
	 */
	function updateList() {

		setProgress(0);
		$container.addClass('loading');
		$('.job-list').empty();

		$.ajax( window.location.href ).done(function ( result ) {
			$('.job-list').html( $( '.job-list', result ) );
			$('.last-sync').html( $( '.last-sync', result ) );
			$container.removeClass('loading');
		});

	}

	if ($.fn.tooltip) {
		$('[data-toggle="tooltip"]').tooltip();
	}

});