
(function ( $nn ) {
	
	$nn.fn.extend({

		/**
		 * 
		 */
		'suggest': function ( arr ) {

			return this.each(function () {

				var $el = $nn(this);
				var id = 'nn-suggest-list';
				var inp = $el.first();
				var currentFocus;

				if (inp._nnsuggest) {
					inp._nnsuggest.arr = arr;
					$el.trigger('input');
					return;
				}

				var $container = $nn('<ul id="' + id + '" class="nn-suggest-list"></ul>');
				$el.parent().append( $container );
				$el.attr({autocomplete:'off'});

				inp._nnsuggest = {arr:arr};
				$el.addEventListener("input", updateResults);
	
				$el.keydown(function(e) {
					var $items = $container.find('li').removeClass('nn-suggest-active');
					if (e.keyCode == 40) {
						currentFocus++;
					} else if (e.keyCode == 38) {
						currentFocus--;
					} else if (e.keyCode == 13) {
						e.preventDefault();
						if (currentFocus > -1) {
							$items.get(currentFocus).click();
						}
					}
					currentFocus = Math.max(Math.min( currentFocus, inp._nnsuggest.arr.length-1), 0);
					$items.get(currentFocus).addClass('nn-suggest-active');
				});
		
				function updateResults() {
					var val = $el.val();
					val = val.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
					closeAllLists();
					
					if (!val) { return false;}
					currentFocus = -1;
					
					var arr = inp._nnsuggest.arr;
					var regex = new RegExp( val, 'ig');

					for (var i = 0; i < arr.length; i++) {

						var obj = arr[i];
						var label = obj.label || obj.title;

						var pos = label.search( regex );
						if (pos > -1) {
							var div = '<li class="nn-suggest-item">' + label.substr(0, pos) + '<strong>' + label.substr(pos, val.length) + '</strong>' + label.substr(pos + val.length) + '</li>';							
							
							var $b = $nn(div);
							$b.data({obj:obj});
							$b.click(function(e) {
								var data = $nn(this).data();
								inp.value = data.obj.label || data.obj.title;
								$el.trigger('nn.selected.suggest', data.obj );
								closeAllLists();
								return false;
							});

							$container.append($b);
						}
					}
				}

				function closeAllLists() {
					$container.find('li').remove();
				}
				
				document.addEventListener("click", function (e) {
					closeAllLists(e.target);
				});
	
				return this;
			});
		}
	});

	/**
	 * main function
	 */
	$nn(function () {
		
		var suggestTimer = false;
		var $suggestFields = $nn('[data-zv-suggest]');

		$suggestFields.keyup( function ( e ) {
			
			if (e.keyCode < 65) {
				return;
			}

			var $me = $nn(this);
			var url = $me.data().zvSuggest;
			var keyword = $me.val();

			if (keyword.length < 3) {
				return;
			}

			var query = $nn.unserialize(url);
			query.params['zv[query]'] = keyword;
			delete(query.params.cHash);

			url = $nn.serialize(query);
			
			clearTimeout( suggestTimer );
			suggestTimer = setTimeout( function () {
				$nn.getJSON( url ).done( function ( data ) {
					$me.suggest(data);
				});
			}, 200);

		});

	});

})(window.$nn);
