
(function ( $nn ) {		
	$nn(function () {
		
		/**
		 * Click on "find my location" pin:
		 * Determine geoposition of the user and set lat / lng in the search form.
		 * 
		 */
		(function () {

			var $btn = $nn('.zv-geoposition');
			var $location = $nn('[data-zv-input="location"]');
			var $lat = $nn('[data-zv-input="lat"]');
			var $lon = $nn('[data-zv-input="lon"]');

			$btn.hide();

			if ($nn.canGetGeoPosition()) {
				$btn.show();
			}

			$btn.click(function (e) {
				$btn.addClass('nn-spinner');
				$nn.getGeoPosition().then(function(pos) {
					$location.val('');
					$location.attr({placeholder:'✓ Aktueller Standort'});
					$lat.val( pos.lat );
					$lon.val( pos.lng );
				}).finally(function () {
					$btn.removeClass('nn-spinner');
				});
				return false;
			});

		})();
		

		/**
		 * Click on element with `[data-nn-spinner]`:
		 * Show spinner
		 * 
		 */
		$nn(document).on('click', '[data-nn-spinner]', function () {
			$nn(this).addClass('nn-spinner');
		});

		/**
		 * Click on "submit" of search-form:
		 * Show spinner
		 * 
		 */
		$nn('.zv-form, .zv-search-form').submit(function () {
			$nn(this).find('[type="submit"]').addClass('nn-spinner');
		});
		
		/**
		 * Click on "add to wishlist":
		 * Bookmark a job using cookies.
		 * 
		 */
		var gWishList = getWishlist();
		
		$nn(document).on('click', '[data-zv-toggle-wishlist]', function ( e ) {
			var uid = $nn(this).data().zvToggleWishlist;
			gWishList[uid] ? delete(gWishList[uid]) : gWishList[uid] = uid;
			saveWishlist( gWishList );
			return false;
		});
		
		$nn(document).on('click', '[zv-wishlist-delete-all]', function ( e ) {
			saveWishlist({});
			location.reload();
			return false;
		});

		$nn(document).on('click', '[data-zv-remove-wishlist]', function ( e ) {
			var uid = $nn(this).data().zvRemoveWishlist;
			delete(gWishList[uid]);
			$nn(this).closest('.zv-list-item').fadeOut();
			saveWishlist( gWishList );
			if (Object.keys(gWishList).length == 0) {
				location.reload();
			}
			return false;
		});

		// Returns list of current bookmarks as an object with uids as key, e.g. `{52:52, 44:44, 21:21}`
		function getWishlist() {
			var str = $nn.cookie.get('zv-wishlist') || '';
			var arr = str ? str.split(',') : [];
			var obj = {};
			arr.forEach(function ( uid ) {
				obj[uid] = uid;
			});
			return obj;
		}

		// Saves current wishlist `{52:52, 44:44, 21:21}` as simple delimited string (`52,44,21`) in cookie
		function saveWishlist( list ) {
			var arr = [];
			for (var uid in list) {
				arr.push( uid );
			}
			$nn.cookie.set('zv-wishlist', arr.join(','), { expires: 1000 });
			updateWishlistView();
		}

		// Update the divs / icons indicating, which jobs are on the wishlist
		function updateWishlistView() {
			$nn('[data-zv-toggle-wishlist]').each(function ( $el ) {
				var $me = $nn(this);
				var uid = $me.data().zvToggleWishlist;
				$me.toggleClass('zv-on-wishlist', !!gWishList[uid]);
			});
		}
		updateWishlistView();
		
		/**
		 * Filters and searchform
		 * 
		 */

		// Reset the search form
		$nn('.zv-reset-filters').click(function () {
			$nn('[name*="tx_zvoove_search"]').not('[type="hidden"]').val('');
			$nn(this).closest('form').submit();
			return false;
		});

		$nn('[data-zv-ajax-load-results]').change(function () {
			var $me = $nn(this);
			var target = $me.data().zvAjaxLoadResults;
			var $target = $nn(target);
			var $form = $me.closest('form');
			$form.ajaxSubmit().done( function (result) {
				var html = $nn(result).find( target ).html();
				$target.html( html );
			});
		});

	});
})(window.$nn);
