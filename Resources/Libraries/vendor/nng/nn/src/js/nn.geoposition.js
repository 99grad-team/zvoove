
(function ( $nn ) {
	
	$nn.extend({
		
		/**
		 * Determine geo-location of user. 
		 * 
		 * $nn.getGeoPosition()
		 * 	.then(function(geo) { .... })
		 * 	.catch()
		 * 	.finally();
		 * 
		 * @return object
		 */
		'getGeoPosition': function () 
		{
			var _instance = function () {}
			_instance.prototype.then = function ( callback ) {
				this.onThen = callback;
				return this;
			};
			_instance.prototype.catch = function ( callback ) {
				if (!$nn.canGetGeoPosition()) callback();
				this.onCatch = callback;
				return this;
			};
			_instance.prototype.finally = function ( callback ) {
				this.onFinally = callback;
				return this;
			};
			_instance.prototype.exec = function () {
				var me = this;
				if (!$nn.canGetGeoPosition()) {
					return this;
				}
				navigator.geolocation.getCurrentPosition(function(e) {
					var coords = e.coords || {};
					if (me.onThen) me.onThen({
						lat: coords.latitude,
						lng: coords.longitude
					});
					if (me.onFinally) me.onFinally();
				}, function () {
					var err = {status:404, message:'Could not determine coordinates'};
					if (me.onCatch) me.onCatch( err );
					if (me.onFinally) me.onFinally();
				});
				return this;
			};
			var instance = new _instance();
			return instance.exec();
		},

		/**
		 * Check if geo-location can be determined.
		 * 
		 * var can = $nn.canGetGeoPosition();
		 * 
		 * @return boolean
		 */
		'canGetGeoPosition': function () 
		{
			return "geolocation" in navigator;
		}
	});

})(window.$nn);
