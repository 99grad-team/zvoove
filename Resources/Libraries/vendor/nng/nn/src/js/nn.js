/**
 * A minature jQuery replacement, reduced to the bare minimum functions.
 * Written in PlainJS and built to avoid any possible conlict.
 * 
 */
;(function ( namespace ) {

	/**
	 * Class and constructor
	 * 
	 */
	var nn = function ( sel ) {
		this.$elList = [];
		this._delay = 0;
		this.add( sel );
	}

	var $nn = function ( sel ) {
		if (typeof sel == 'function') {
			return $nn(document).ready( sel );
		}
		return new nn( sel );
	}

	$nn.fn = {
		'extend': function ( obj ) {
			for (var n in obj) {
				nn.prototype[n] = obj[n];
			}
		}
	}
	
	$nn.extend = function ( srcObj, overlayObj ) {
		if (!overlayObj) {
			overlayObj = srcObj;
			srcObj = $nn;
		}
		for (var n in overlayObj) {
			srcObj[n] = overlayObj[n];
		}
		return srcObj;
	}
	
	/**
	 * Extend basic functions
	 * 
	 */
	$nn.fn.extend({
		'add': function ( sel ) {
			var me = this;
			if (!sel) return;
			if (sel == document) {
				this.$elList.push( sel );
			} else if (Array.isArray(sel)) {
				sel.forEach(function ( $el ) {
					me.add( $el );
				});
			} else if (false && typeof sel == 'object') {
				this.$elList.push( sel );
			} else if (sel && sel.nodeType) {
				this.$elList.push( sel );
			} else if (sel.trim().charAt(0) == '<') {
				var sel = sel.trim();
				if (sel.indexOf('<body') > -1) {
					sel = '<div>' + (/<body.*?>([\s\S]*)<\/body>/.exec( sel )[1]) + '</div>';
				}
				var div = document.createElement('div');
  				div.innerHTML = sel;
				this.$elList.push( div.firstChild.cloneNode( true ) );
				div.remove();
			} else {
				var $results = document.querySelectorAll( sel );
				for (var n = 0; n < $results.length; n++) {
					this.$elList.push( $results[n] );
				}
			}
			this.length = this.$elList.length;
			return this;
		},
		'addClass': function ( className ) {
			this.delay(function () {
				this.$elList.forEach(function ( $el ) {
					$el.classList.add( className );
				});
			});
			return this;
		},
		'addEventListener': function ( eventname, callback, selector ) {
			for (var i in this.$elList) {
				this.$elList[i].addEventListener( eventname, 
					(function (e) {
						var result = true;
						if (selector) {
							var targetElement = e.target;
							while (targetElement != null) {
								if (targetElement.matches(selector)) {
									result = callback ? callback.apply( targetElement, [e] ) : true;
									if (result === false) {
										e.preventDefault();
										e.stopPropagation();	
									}
									return;
								}
								targetElement = targetElement.parentElement;
							}
						} else {
							result = callback ? callback.apply( this, [e] ) : true;
							if (result === false) {
								e.preventDefault();
								e.stopPropagation();	
							}
						}

					}).bind(this.$elList[i])
				);
			}
			return this;
		},
		'animate': function( obj, dur, func ) {
			if (!this._delay) {
				this._delay = 5;
			}
			if (typeof dur == 'function') {
				func = dur;
				dur = null;
			}
			if (!dur) dur = 250;
			this.delay(function () {
				obj.transition = 'all ' + dur + 'ms';
				this.css(obj);
				$nn.delay( this, function () {
					this.css({transition:''});
					if (func) func.apply( this );
				}, dur - 5);
			});
			return this.delay( dur );
		},
		'append': function( $el, prepend ) {
			var method = prepend ? 'prepend' : 'appendChild';
			for (var i in this.$elList) {
				var $target = this.$elList[i];
				var $ref = i == this.$elList.length-1 ? $el : $el.clone();
				for (var n in $ref.$elList) {
					$target[method]( $ref.$elList[n] );
				}
			}
			return this;
		},
		'attr': function ( obj ) {
			if (typeof obj == 'string') {
				return this.first().getAttribute( obj );
			}
			this.$elList.forEach( function ( el ) {
				for (var k in obj) {
					el.setAttribute(k, obj[k]);
					if (k == 'checked') el.checked = true;
					if (k == 'selected') el.selected = true;
				}
			});
			return this;
		},
		'ajaxSubmit': function ( params ) {
			
			var $form = $nn(this.first());

			params = $nn.extend({
				url: $form.attr('action'),
				data: $form.serialize(),
				complete: false,
				type: 'POST',
				fail: false
			}, params);

			return $nn.ajax( params );
		},
		'removeAttr': function ( key ) {
			this.$elList.forEach( function ( el ) {
				el.removeAttribute( key );
				if (key == 'checked') el.checked = false;
				if (key == 'selected') el.selected = false;
			});
			return this;
		},
		'change': function ( callback ) {
			if (callback) {
				this.addEventListener( 'change', callback);
				return this;
			}
			this.trigger('change');
			return this;	
		},
		'click': function ( callback ) {
			if (callback) {
				this.addEventListener( 'click', callback);
				return this;
			}
			for (var i in this.$elList) {
				this.$elList[i].click();
			}
			return this;	
		},
		'clickOutside': function ( callback ) {
			this.$elList.forEach( function ( el ) {
				$nn(document).click(function (e) {
					if (e.path.indexOf(el) == -1) {
						callback.apply( this );
					}
				});
			});
			return this;
		},
		'closest': function ( sel ) {
			var $parents = $nn();
			for (var i in this.$elList) {
				var parent = this.$elList[i].parentNode;
				while (parent) {
					if (parent.matches(sel)) {
						$parents.add( parent );
						break;
					}
					parent = parent.parentNode;
				}
			}
			return $parents;
		},
		'clone': function () {
			var $clone = $nn();
			this.$elList.forEach(function ($el) {
				$clone.add( $el.cloneNode( true ) );
			});
			return $clone;
		},
		'css': function( obj ) {
			if (obj == undefined) {
				return window.getComputedStyle(this.first());
			}
			if (typeof obj == 'string') {
				return window.getComputedStyle(this.first())[obj];
			}
			this.delay(function () {
				var units = {left:'px', right:'px', top:'px', bottom:'px', height:'px', width:'px'};
				for (var n in obj) {
					var val = obj[n];
					if (val != '' && units[n]) {
						if ($nn.isNumeric(val)) {
							val += units[n];
						}
					}
					this.$elList.forEach(function ( $el ) {
						$el.style[n] = val;
					});
				}
			});
			return this;
		},
		'data': function ( obj ) {
			var first = this.first();
			if (!first._data) {
				first._data = {};
				for (var i in (first.dataset || {})) {
					first._data[i] = first.dataset[i];
				}
			}
			if (!obj) return first._data;
			if (typeof obj == 'string') {
				return first._data[obj];
			}
			for (var i in obj) {
				first._data[i] = obj[i];
			}
			return first._data;
		},
		'delay': function ( dur, func, args ) {
			if (!args) args = [];

			// delay( function () {}, 100, [1,2,3] )
			if (typeof dur == 'function') {
				var delay = func || this._delay;
				$nn.delay( this, dur, delay, args );
				return this;
			}
			
			// delay( 100 ).addClass(...)
			var clone = $nn( this.$elList );
			clone._delay = this._delay + dur;
			this._delay = 0;
			return clone;
		},
		'each': function ( callback ) {
			this.$elList.forEach(function ( $el, i ) {
				callback.apply( $el, [$el, i]);
			});
			return this;
		},
		'fadeOut': function ( dur, func ) {
			return this.animate({opacity:0}, dur, func).hide().css({opacity:''});
		},
		'fadeIn': function ( dur, func ) {
			return this.show().css({opacity:0}).delay(10).animate({opacity:1}, dur, func).css({opacity:''});
		},
		'first': function () {
			return this.$elList[0] || {};
		},
		'find': function ( sel ) {
			var $list = $nn();
			for (var i in this.$elList) {
				var found = this.$elList[i].querySelectorAll( sel );
				for (var n = 0; n < found.length; n++) {
					$list.add( found[n] );
				}
			}
			return $list;
		},
		'filter': function( selector ) {
			var $clone = $nn();
			this.$elList.forEach(function ( $el ) {
				if ($el.matches(selector)) {
					$clone.add( $el );
				}
			});
			return $clone;
		},
		'get': function( n ) {
			return $nn( this.$elList[n] );
		},
		'hasClass': function( className ) {
			var first = this.first();
			if (!first) return false;
			return first.classList.contains(className);
		},
		'height': function ( val ) {
			if (val == undefined) {
				return this.first().clientHeight;
			}
			this.$elList.forEach(function ( $el ) {
				$el.style.height = val;
			});
			return this;
		},
		'hide': function( n ) {
			this.delay(function () {
				this.$elList.forEach(function ( $el ) {
					$el.style.display = 'none';
				});
			});
			return this;
		},
		'hover': function ( enterCallback, leaveCallback ) {
			if (enterCallback) this.mouseenter( enterCallback );
			if (leaveCallback) this.mouseleave( leaveCallback );
			return this;
		},
		'html': function ( html ) {
			if (html === undefined) {
				return this.first().innerHTML;
			}
			for (var i in this.$elList) {
				this.$elList[i].innerHTML = html;
			}
			return this;
		},
		'keyup': function ( callback ) {
			this.addEventListener( 'keyup', callback );
			return this;
		},
		'keydown': function ( callback ) {
			this.addEventListener( 'keydown', callback );
			return this;
		},
		'mouseenter': function ( callback ) {
			if (callback) {
				this.addEventListener( 'mouseenter', callback);
				return this;
			}
			for (var i in this.$elList) {
				this.$elList[i].mouseenter();
			}
			return this;	
		},
		'mouseleave': function ( callback ) {
			if (callback) {
				this.addEventListener( 'mouseleave', callback);
				return this;
			}
			for (var i in this.$elList) {
				this.$elList[i].mouseleave();
			}
			return this;	
		},
		'not': function( selector ) {
			var $clone = $nn();
			for (var i in this.$elList) {
				if (!this.$elList[i].matches(selector)) {
					$clone.add( this.$elList[i] );
				}
			}
			return $clone;
		},
		'offset': function ( includeMargins = false ) {
			var $first = this.first();
			var elOffset = $first.getBoundingClientRect();
			return {
				top: elOffset.top + window.pageYOffset + (!includeMargins ? parseInt(this.css().marginTop) : 0),
				left: elOffset.left + window.pageXOffset + (!includeMargins ? parseInt(this.css().marginLeft) : 0)
			};
		},
		'on': function ( eventName, selector, callback ) {
			this.addEventListener( eventName, callback, selector );
		},
		'outerWidth': function () {
			return this.first().getBoundingClientRect().width;
		},
		'outerHeight': function () {
			return this.first().getBoundingClientRect().height;
		},
		'parent': function () {
			var $parents = $nn();
			for (var i in this.$elList) {
				$parents.add( this.$elList[i].parentNode );
			}
			return $parents;
		},
		'prepend': function( $el ) {
			return this.append( $el, true );
		},
		'position': function( obj ) {
			for (var i in this.$elList) {
				$nn(this.$elList[i]).css( obj );
			}
			return this;
		},
		'ready': function (fn) {
			if (this.first().readyState === 'complete' || this.first().readyState === 'interactive') {
				setTimeout(fn, 1);
			} else {
				this.addEventListener('DOMContentLoaded', fn);
			}
			return this;
		},
		'remove': function () {
			this.delay(function () {
				for (var i in this.$elList) {
					var $ref = this.$elList[i];
					$ref.parentNode.removeChild( $ref );
				}
				this.$elList = [];
			});
			return this;
		},
		'removeClass': function ( className ) {
			this.delay(function () {
				this.$elList.forEach( function ( $el ) {
					$el.classList.remove( className );
				});
			});
			return this;
		},
		'serialize': function ( includeHidden, prefix ) {
			var data = {};
			var includeHidden = includeHidden === undefined || includeHidden === true;

			$nn(this.first()).find('input, select, textarea, button').each( function () {

				var $field = $nn(this);
				var key = $field.attr('name');
				var v = $field.val();
				
				if (!includeHidden && $field.attr('type') == 'hidden') {
					return;
				}

				if ($field.attr('type') == 'radio' || $field.attr('type') == 'checkbox') {
					var $selected = $nn('[name="'+$field.attr('name')+'"]').filter(':checked');
					if ($selected.length) {
						v = $selected.val();
					} else {
						v = '';
					}
				}
				
				if (key) {
					data[key] = v;
				}
			});
			if (prefix) {
				var tmp = {};
				for (var i in data) {
					var k = i.split(prefix+'[').join('').split(']').join('');
					if (k.indexOf('_') == -1) {
						tmp[k] = data[i];
					}
				}
				return tmp;
			}
			return data;
		},
		'show': function( n ) {
			this.delay(function () {
				this.$elList.forEach(function ( $el ) {
					$el.style.display = null;
				});
			});
			return this;
		},
		'slideDown': function ( dur ) {
			if (!dur) dur = 250;
			this.$elList.forEach(function ( el ) {
				var $el = $nn(el);
				var h = $el.show().css({height:''}).height();
				$el.css({height:0, overflow:'hidden'}).animate({height:h}).css({height:'', overflow:''});
			});
			return this.delay(dur);
		},
		'slideUp': function ( dur ) {
			if (!dur) dur = 250;
			this.$elList.forEach(function ( el ) {
				var $el = $nn(el);
				var h = $el.show().height();
				$el.css({height:h, overflow:'hidden'}).animate({height:0}).hide();
			});
			return this.delay(dur);
		},
		'submit': function ( callback ) {
			if (callback == undefined) {
				this.trigger('submit').first().submit();
				return this;
			}
			this.addEventListener( 'submit', callback );
			return this;
		},
		'text': function ( text ) {
			if (text == undefined) {
				return this.first().textContent || '';
			}
			for (var i in this.$elList) {
				this.$elList[i].textContent = text;
			}
			return this;
		},
		'tagName': function( val ) {
			if (val == undefined) {
				return this.first().tagName.toLowerCase();
			}
			this.$elList.forEach(function ( $el ) {
				$el.tagName = val;
			});
		},
		'toggleClass': function ( className, show ) {
			this.$elList.forEach(function ( $el ) {
				var $el = $nn($el);
				var hasClass = $el.hasClass( className );
				var addClass = show === undefined ? !hasClass : show;
				$el[addClass ? 'addClass' : 'removeClass']( className );
			});
			return this;
		},
		'trigger': function( eventName, params ) {
			var ev = new CustomEvent( eventName, {detail:params} );
			for (var i in this.$elList) {
				this.$elList[i].dispatchEvent( ev );
			}
			return this;
		},
		'val': function ( val ) {
			var first = this.first();
			var $first = $nn(first);

			if (val == undefined) {
				if ($first.tagName() == 'option') {
					return first.value == '' ? '' : (first.value || $first.text());
				}
				if ($first.tagName() == 'select' && $first.attr('multiple')) {
					var arr = [];
					$first.find('option').each(function ( $option ) {
						if ($option.selected) arr.push( $nn($option).val() );
					});
					return arr;
				}

				return first.value || '';
			}

			this.$elList.forEach( function ( el ) {
				var $el = $nn(el);
				if (val == '' && $el.tagName() == 'select' && !$el.attr('multiple')) {
					val = $el.find('[value=""]').length ? '' : $nn($el.find('option').first()).val();
				}
				if ($el.tagName() == 'select' && $el.attr('multiple')) {
					$el.find('option').removeAttr('selected');
					if (!Array.isArray(val)) {
						val = [val];
					}
					val.forEach(function (v) {
						$el.find('[value="' + v + '"]').attr({selected:'selected'});
					});
				} else {
					el.value = val;
				}
				$el.change();
			});

			return this;
		},
		'width': function ( val ) {
			if (val == undefined) {
				return this.first().clientWidth;
			}
			this.$elList.forEach(function ( $el ) {
				$el.style.width = val;
			});
			return this;
		}
		
	});

	/**
	 * Add static methods
	 * 
	 */
	$nn.extend({
		'each': function ( arr, callback ) {
			for (var i in arr) {
				callback.apply( arr[i], [i, arr[i]] );
			}
			return this;
		},
		'ajax': function ( params ) {
			
			params = $nn.extend({
				complete: false,
				type: 'POST',
				fail: false,
				data: {}
			}, params);
			
			var url = params.url;
			var urlParams = $nn.unserialize( params.url );
			var sendBody = ['POST', 'PATCH', 'PUT'].indexOf(params.type) > -1;

			// if method GET, add data to GET-request
			if (params.type == 'GET') {
				urlParams.params = $nn.extend( urlParams.params, params.data );
			}

			// add cache-busting
			urlParams.params._ = new Date().getTime();

			// remove cHash
			delete(urlParams.params.cHash);
			delete(params.data.cHash);
			url = $nn.serialize( urlParams );

			var xhr = new XMLHttpRequest();
			var onDone, onFail, _interface;
	
			xhr.onreadystatechange = function () {
				if (xhr.readyState == XMLHttpRequest.DONE) {
					if (xhr.status == 200) {
						if (onDone) onDone(xhr.responseText);
						if (params.complete) params.complete(xhr.responseText);
					} else {
						if (onFail) onFail(xhr.statusText);
						if (params.fail) params.fail(xhr.responseText);
					}
				}
			};

			xhr.open(params.type, url, true);
			xhr.withCredentials = true;
			xhr.setRequestHeader("Cache-Control", "no-cache, no-store, max-age=0");    
			xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			xhr.setRequestHeader("Expires", "Tue, 01 Jan 1980 1:00:00 GMT");
			xhr.setRequestHeader("Pragma", "no-cache");
	
			setTimeout(function () {
				if (sendBody) {
					//console.log($nn.serialize( params.data ));
					xhr.send( $nn.serialize( params.data ) );
				} else {
					xhr.send();
				}
			}, 10);
	
			return _interface = {
				done: function ( callback ) {
					onDone = callback;
					return _interface;
				},
				fail: function ( callback ) {
					onFail = callback;
					return _interface;
				}
			};
		},
		'delay': function ( _this, callback, time, args ) {
			if (!args) args = [];
			if (!time || time < 0) return callback.apply( _this, args );
			setTimeout(function () {
				callback.apply( _this, args );
			}, time);
		},
		'unserialize': function( url ) {
			if (!url) url = '';

			var baseUrl = url.substring( 0, url.search(/\?/) );
			var match,
				pl     = /\+/g,
				search = /([^&=]+)=?([^&]*)/g,
				decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
				query  = url.substring( url.search(/\?/) + 1 );
	
			var urlParams = {};
			while (match = search.exec(query)) {
				urlParams[decode(match[1])] = decode(match[2]);
			}
			return {
				baseUrl: baseUrl || window.location.href,
				params: urlParams,
			}
		},
		'serialize': function( obj ) {
			var baseUrl = '';
			
			if (obj.baseUrl !== undefined || obj.params) {
				baseUrl = obj.baseUrl;
				obj = obj.params || {};
			}
	
			if (baseUrl) {
				baseUrl += '?';
			}
			var arr = [];
			for (var key in obj) {
				var val = obj[key];
				if (!Array.isArray(val)) val = [val];
				val.forEach(function (v) {
					arr.push( key + "=" + encodeURIComponent(v) );
				});
			}
			return baseUrl + arr.join('&');
		},
		'get': function ( url ) {
			var xhr = new XMLHttpRequest();
			var onDone, onFail, _interface;
	
			xhr.onreadystatechange = function () {
				if (xhr.readyState == XMLHttpRequest.DONE) {
					if (xhr.status == 200) {
						if (onDone) onDone(xhr.responseText);
					} else {
						if (onFail) onFail(xhr.statusText);
					}
				}
			};

			// Cache busting
			if (url.indexOf('?') == -1) url += '?';
			url += '&_=' + new Date().getTime();

			xhr.open('GET', url, true);
			xhr.withCredentials = true;
			xhr.setRequestHeader("Cache-Control", "no-cache, no-store, max-age=0");    
			xhr.setRequestHeader("Expires", "Tue, 01 Jan 1980 1:00:00 GMT");
			xhr.setRequestHeader("Pragma", "no-cache");
	
			setTimeout(function () {
				xhr.send();
			}, 10);
	
			return _interface = {
				done: function ( callback ) {
					onDone = callback;
					return _interface;
				},
				fail: function ( callback ) {
					onFail = callback;
					return _interface;
				}
			};
		},
		'getJSON': function ( url ) {
			var onDone, onFail, _interface;
	
			$nn.get( url ).done( function ( result ) {
				if (onDone) onDone( JSON.parse(result) || {} );
			}).fail( function () {
				if (onFail) onFail( JSON.parse(result) || {} );
			});
	
			return _interface = {
				done: function ( callback ) {
					onDone = callback;
					return _interface;
				},
				fail: function ( callback ) {
					onFail = callback;
					return _interface;
				}
			};
		},
		'isNumeric': function ( n ) {
			return !isNaN(parseFloat(n)) && isFinite(n);
		}
	});

	/**
	 * Register in global namespace
	 * 
	 */
	if (!window.$nn) {
		window.$nn = $nn;
	}

})();

/**
 * IE11 CustomEvent polyfill
 * 
 */
(function () {
	if ( typeof window.CustomEvent === "function" ) return false; //If not IE
	function CustomEvent ( event, params ) {
		params = params || { bubbles: false, cancelable: false, detail: undefined };
		var evt = document.createEvent( 'CustomEvent' );
		evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
		return evt;
	}
	CustomEvent.prototype = window.Event.prototype;  
	window.CustomEvent = CustomEvent;
})();

/**
 * IE11 classlist polyfill
 * @source http://purl.eligrey.com/github/classList.js/blob/master/classList.js
 */
"document"in self&&("classList"in document.createElement("_")&&(!document.createElementNS||"classList"in document.createElementNS("http://www.w3.org/2000/svg","g"))||!function(t){"use strict";if("Element"in t){var e="classList",n="prototype",i=t.Element[n],s=Object,r=String[n].trim||function(){return this.replace(/^\s+|\s+$/g,"")},o=Array[n].indexOf||function(t){for(var e=0,n=this.length;n>e;e++)if(e in this&&this[e]===t)return e;return-1},c=function(t,e){this.name=t,this.code=DOMException[t],this.message=e},a=function(t,e){if(""===e)throw new c("SYNTAX_ERR","The token must not be empty.");if(/\s/.test(e))throw new c("INVALID_CHARACTER_ERR","The token must not contain space characters.");return o.call(t,e)},l=function(t){for(var e=r.call(t.getAttribute("class")||""),n=e?e.split(/\s+/):[],i=0,s=n.length;s>i;i++)this.push(n[i]);this._updateClassName=function(){t.setAttribute("class",this.toString())}},u=l[n]=[],h=function(){return new l(this)};if(c[n]=Error[n],u.item=function(t){return this[t]||null},u.contains=function(t){return~a(this,t+"")},u.add=function(){var t,e=arguments,n=0,i=e.length,s=!1;do t=e[n]+"",~a(this,t)||(this.push(t),s=!0);while(++n<i);s&&this._updateClassName()},u.remove=function(){var t,e,n=arguments,i=0,s=n.length,r=!1;do for(t=n[i]+"",e=a(this,t);~e;)this.splice(e,1),r=!0,e=a(this,t);while(++i<s);r&&this._updateClassName()},u.toggle=function(t,e){var n=this.contains(t),i=n?e!==!0&&"remove":e!==!1&&"add";return i&&this[i](t),e===!0||e===!1?e:!n},u.replace=function(t,e){var n=a(t+"");~n&&(this.splice(n,1,e),this._updateClassName())},u.toString=function(){return this.join(" ")},s.defineProperty){var f={get:h,enumerable:!0,configurable:!0};try{s.defineProperty(i,e,f)}catch(p){void 0!==p.number&&-2146823252!==p.number||(f.enumerable=!1,s.defineProperty(i,e,f))}}else s[n].__defineGetter__&&i.__defineGetter__(e,h)}}(self),function(){"use strict";var t=document.createElement("_");if(t.classList.add("c1","c2"),!t.classList.contains("c2")){var e=function(t){var e=DOMTokenList.prototype[t];DOMTokenList.prototype[t]=function(t){var n,i=arguments.length;for(n=0;i>n;n++)t=arguments[n],e.call(this,t)}};e("add"),e("remove")}if(t.classList.toggle("c3",!1),t.classList.contains("c3")){var n=DOMTokenList.prototype.toggle;DOMTokenList.prototype.toggle=function(t,e){return 1 in arguments&&!this.contains(t)==!e?e:n.call(this,t)}}"replace"in document.createElement("_").classList||(DOMTokenList.prototype.replace=function(t,e){var n=this.toString().split(" "),i=n.indexOf(t+"");~i&&(n=n.slice(i),this.remove.apply(this,n),this.add(e),this.add.apply(this,n.slice(1)))}),t=null}());
;