
(function($nn) {
	$nn.fn.extend({

		/**
		 * Tools for collapsing elements
		 * 
		 */
		'collapse': function ( opt ) {

			return this.each(function () {

				var $el = $nn(this);
				var $target = $nn($el.attr('data-nn-target'));

				$el.click(function () {
					var show = !$target.hasClass('show');
					if (show) $target.addClass('show');
					var $delayedTarget = $target[show ? 'slideDown' : 'slideUp']();
					if (!show) $delayedTarget.removeClass('show');
					return false;
				});

				return this;
			});
		}
	});

	/**
	 * 
	 */
	$nn(function () {
		$nn('[data-nn-toggle="collapse"]').collapse();
	});
	
})(window.$nn);
