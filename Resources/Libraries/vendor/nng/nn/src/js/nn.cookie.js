
(function ( $nn ) {
	
	$nn.extend({
		
		/**
		 * Get / set cookies 
		 * 
		 * $nn.cookie.get('test');
		 * $nn.cookie.set('test', 'nice!');
		 * 
		 * @return object
		 */
		'cookie': {
			
			get: function ( key ) 
			{
				var name = key + "=";
				var ca = document.cookie.split(';');
				for(var i = 0; i < ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0) == ' ') {
						c = c.substring(1);
					}
					if (c.indexOf(name) == 0) {
						var str = c.substring(name.length, c.length);
						return decodeURIComponent( str );
					}
				}
				return '';
			},

			set: function ( key, val, options ) 
			{
				if (!options) options = {};
				var path = options.path || '/';
				var expires = options.expires || 30;

				var d = new Date();
				d.setTime(d.getTime() + (options.expires * 24 * 60 * 60 * 1000));
				d = d.toUTCString();

				val = encodeURIComponent( val );
				document.cookie = key + "=" + val + ";" + expires + ";path=" + path;
			}
		}
	});

})(window.$nn);
