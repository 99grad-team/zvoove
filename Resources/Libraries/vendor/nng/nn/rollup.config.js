
import { babel } from '@rollup/plugin-babel'
import { terser } from 'rollup-plugin-terser'
import scss from 'rollup-plugin-scss'
import copy from 'rollup-plugin-copy-watch'
import livereload from 'rollup-plugin-livereload'
import serve from 'rollup-plugin-serve'

const fs = require('fs');

export default [
	{
		input: 'src/js/main.js',
		output: {
			file: 'dist/nn.min.js',
			format: 'iife',
		},
		plugins: [
			babel({ 
				babelHelpers: 'bundled',
				plugins: ['@babel/plugin-proposal-async-generator-functions']
			}),
			terser(),
			scss({
				output: 'dist/nn.min.css',
				watch: 'scss',
				outputStyle: 'compressed',
			})
		]
	}
];