
# nn

This is a extremely reduced version of:

- the boostrap 5 framework
- a JS frontend framework, very, very much inspired by jQuery

We are only keeping the bare-minimum functions and classes in the packages.

Main purpose of this package is to be able to create and deploy TYPO3 and WordPress
extensions with no dependencies or conflicts to/with other CSS and JS frameworks.

This makes the published extensions as "plug & play" as possible. They can be 
delivered with a basic styling and JavaScript functions – but are not bloated with
unneccesary styling or JavaScript.

## nn.min.css

Use (many) of the boostrap 5 classes, prefixed with `nn-`.
Example: `mt-3` will be `nn-mt-3` or `form-control` is `nn-form-control`

## nn.min.js

This is a jQuery drop-in-replacement. Use `$nn()` instead of `$()`. 
It supports the following jQuery functions:

### extending functions
- $nn.extend
- $nn.fn.extend

### basic functions
- .add()
- .addClass()
- .addEventListener()
- .animate()
- .append()
- .attr()
- .ajaxSubmit()
- .removeAttr()
- .change()
- .click()
- .clickOutside()
- .clone()
- .data()
- .delay()
- .each()
- .fadeOut()
- .fadeIn()
- .first()
- .find()
- .filter()
- .get()
- .hasClass()
- .height()
- .hide()
- .hover()
- .html()
- .keyup()
- .keydown()
- .mouseenter()
- .mouseleave()
- .not()
- .offset()
- .on()
- .outerWidth()
- .outerHeight()
- .parent()
- .prepend()
- .position()
- .ready()
- .remove()
- .removeClass()
- .serialize()
- .show()
- .slideDown()
- .slideUp()
- .submit()
- .text()
- .tagName()
- .toggleClass()
- .trigger()
- .val()
- .width()
- .width()

### root functions
- $nn.each()
- $nn.ajax()
- $nn.delay()
- $nn.unserialize()
- $nn.serialize()
- $nn.get()
- $nn.getJSON()
- $nn.isNumeric()


## Compiling

`npm install`
`npm run build`