<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'zvoove',
    'description' => 'Connect TYPO3 to the job and recruiting management zvoove. Display a list of your vacancies directly on your site – without iframes. Send applications to your zvoove inbox from a TYPO3 form. Customize the template with fluid and css.',
    'category' => 'frontend',
    'author' => 'David Bascom',
    'author_email' => 'info@99grad.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.2.0',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-11.9.99',
            'nnhelpers' => '1.7.0-1.9.99',
            'form' => '',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
